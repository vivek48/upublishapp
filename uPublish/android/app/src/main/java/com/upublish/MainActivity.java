package in.reprobooks.shop;
import android.os.Bundle;
import com.facebook.react.ReactActivity;
import org.devio.rn.splashscreen.SplashScreen;
import com.rnfs.RNFSPackage;
import com.google.firebase.analytics.FirebaseAnalytics;

public class MainActivity extends ReactActivity {
  private FirebaseAnalytics mFirebaseAnalytics;

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */

   // Obtain the FirebaseAnalytics instance.
  @Override
  protected String getMainComponentName() {
    return "uPublish";
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    SplashScreen.show(this); // here
    
mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    super.onCreate(savedInstanceState);
    
  }
}
