export const Images = {
    login_bg: require('../assets/images/login_bg.jpg'),
    world: require('../assets/images/world.png'),
    search: require('../assets/images/search.png'),
    setting: require('../assets/images/setting.png'),
    langugae: require('../assets/images/langugae.jpg'),
    logo: require('../assets/images/logo1.png'),

    library: require('../assets/images/books.png'),
    Homenew: require('../assets/images/new.png'),
    stories: require('../assets/images/stories.png'),

    facebook: require('../assets/images/facebook.png'),
    google: require('../assets/images/google.png'),
    loading: require('../assets/images/loading.gif'),
    order: require('../assets/images/order.png'),

    facebook1: require('../assets/images/facebook1.png'),

    repro_logo1: require('../assets/images/repro_logo1.png'),
    repro_logo: require('../assets/images/repro_logo.png'),
    news: require('../assets/images/news.png'),
    shoppingcart:require('../assets/images/shoppingcart.png'),

    removeIcon: require('../assets/images/remove.png'),
    checkedIcon: require('../assets/images/checked.png'),
}