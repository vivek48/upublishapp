export const Fonts = {
  Roboto: {
    Roboto_Black: 'Roboto-Black',
    Roboto_BlackItalic: 'Roboto-BlackItalic',
    Roboto_Bold: 'Roboto-Bold',
    Roboto_BoldItalic: 'Roboto-BoldItalic',
    Roboto_Italic: 'Roboto-Italic',
    Roboto_Light: 'Roboto-Light',
    Roboto_LightItalic: 'Roboto-LightItalic',
    Roboto_Medium: 'Roboto-Medium',
    Roboto_MediumItalic: 'Roboto-MediumItalic',
    Roboto_Regular: 'Roboto-Regular',
    Roboto_Thin: 'Roboto-Thin',
    Roboto_ThinItalic: 'Roboto-ThinItalic',
  },
  Awesome: {
    AntDesign: 'AntDesign',
    Entypo: 'Entypo',
    EvilIcons: 'EvilIcons',
    Feather: 'Feather',
    FontAwesome: 'FontAwesome',
    FontAwesome5_Brands: 'FontAwesome5_Brands',
    FontAwesome5_Regular: 'FontAwesome5_Regular',
    FontAwesome5_Solid: 'FontAwesome5_Solid',
    Foundation: 'Foundation',
    Ionicons: 'Ionicons',
    MaterialCommunityIcons: 'MaterialCommunityIcons',
    MaterialIcons: 'MaterialIcons',
    Octicons: 'Octicons',
    SimpleLineIcons: 'SimpleLineIcons',
    Zocial: 'Zocial'


  }
}