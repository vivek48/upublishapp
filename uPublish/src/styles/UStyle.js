import { responsiveFontSize, responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions'
import { View, StyleSheet } from 'react-native'
import { Colors, Fonts } from "./Index";
export const UStyle = {
    styles: StyleSheet.create({
        commanStyle: {
            margin: responsiveWidth(1.5),
            // backgroundColor: '#ffff',
            // flex: 1
        },
        // Language Screen Style via Vivek Tiwari

        imageBackGound: {
            width: responsiveWidth(100),
            height: responsiveHeight(100)

        },
        outerView: {
            marginTop: responsiveWidth(8),
            marginHorizontal: responsiveWidth(4),
            flexDirection: 'row',
            alignItems: 'center'
        },

        languageText: {
            fontFamily: Fonts.Roboto.Roboto_Light,
            fontSize: responsiveFontSize(2.1)
        },
        lanImage: {
            marginLeft: responsiveWidth(3),
            marginTop: responsiveWidth(1),
            width: responsiveWidth(5),
            height: responsiveWidth(5)
        },
        imageBook: {
            height: responsiveWidth(40),
            marginTop: responsiveWidth(7),
            justifyContent: 'center',
            alignItems: 'center',
        },
        // Language Component Style by Vivek Tiwari


        outerViewComp: {
            width: responsiveWidth(33),
            justifyContent: 'center', alignContent: 'center',
            alignItems: 'center',
            marginTop: responsiveWidth(8)
        },
        textComp: {
            borderRadius: responsiveWidth(3),
            paddingHorizontal: responsiveWidth(5),
            borderColor: 'grey',
            paddingVertical: responsiveWidth(2),
            fontFamily: Fonts.Roboto.Roboto_Regular,
            borderWidth: responsiveWidth(0.2),
            fontSize: responsiveFontSize(2)
        }
    })

}