export const HardCodeData = {
    LanguageList: [
        {
            language: 'English',
            code: 1,
            languageCode: 'en'
        },
        {
            language: 'हिंदी',
            code: 2,
            languageCode: 'hi'
        },
        // {
        //     // Panjabi
        //     language: 'ਪੰਜਾਬੀ',
        //     code: 3,
        //     languageCode: 'ਪੰਜਾਬੀ'
        // },
        // {
        //     language: 'Marathi',
        //     code: 4,
        // },
        // {
        //     language: 'Panjabi',
        //     code: 5,
        // },

    ],

    booksList: [
        {
            key: 1,
            value: 'Popular free ebooks',
            freeEbook: [
                {
                    key: 1,
                    bookName: 'UPSC IAS SYLLABUS CSE.....',
                    rating: 4.4,
                    type: 'free',
                },
                {
                    key: 2,
                    bookName: 'The A to Z of Karma: The Law.....',
                    rating: 5.0,
                    type: 'free',
                },

                {
                    key: 3,
                    bookName: 'Glorious Bharat Part 1.....',
                    rating: 4.6,
                    type: 'free',
                },
                {
                    key: 4,
                    bookName: 'How to Think Bigger.....',
                    rating: 4.6,
                    type: 'free',
                },
                {
                    key: 5,
                    bookName: 'Improving Your Memory',
                    rating: 2.6,
                    type: 'free',
                }
            ]
        },



        {
            key: 2,
            value: 'Competitive exam  books',
            freeEbook: [
                {
                    key: 1,
                    bookName: 'UPSC IAS SYLLABUS CSE.....',
                    rating: 4.4,
                    type: 'free',
                },
                {
                    key: 2,
                    bookName: 'The A to Z of Karma: The Law.....',
                    rating: 5.0,
                    type: 'free',
                },

                {
                    key: 3,
                    bookName: 'Glorious Bharat Part 1.....',
                    rating: 4.6,
                    type: 'free',
                },
                {
                    key: 4,
                    bookName: 'How to Think Bigger.....',
                    rating: 4.6,
                    type: 'free',
                },
                {
                    key: 5,
                    bookName: 'Improving Your Memory',
                    rating: 2.6,
                    type: 'free',
                }
            ]

        },


        {
            key: 3,
            value: 'Recently Reduce   Prices',
            freeEbook: [
                {
                    key: 1,
                    bookName: 'UPSC IAS SYLLABUS CSE.....',
                    rating: 4.4,
                    type: 'buy',
                    price: '230Rs'
                },
                {
                    key: 2,
                    bookName: 'The A to Z of Karma: The Law.....',
                    rating: 5.0,
                    type: 'buy',
                    price: '200Rs'
                },

                {
                    key: 3,
                    bookName: 'Glorious Bharat Part 1.....',
                    rating: 4.6,
                    type: 'buy',
                    price: '210Rs'
                },
                {
                    key: 4,
                    bookName: 'How to Think Bigger.....',
                    rating: 4.6,
                    type: 'buy',
                    price: '180Rs'
                },
                {
                    key: 5,
                    bookName: 'Improving Your Memory',
                    rating: 2.6,
                    type: 'buy',
                    price: '30Rs'
                }
            ]
        },

    ]
}