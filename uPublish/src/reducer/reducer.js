const initialState = {
    loginuser: false,
    checkuserlogin: '',
    dataInfo: '',
}
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_LOGIN_USER':
            return {
                ...state,
                loginuser: action.data
            }
        case 'SET_CHECKLOGIN_USER':
            return {
                ...state,
                checkuserlogin: action.data
            }
        case 'SET_DATA_INFO_USER':
            return {
                ...state,
                dataInfo: action.data
            }

    }
    return state;
}
export default reducer;