import { responsiveFontSize, responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions'
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native'
import React, { useState, useContext ,useEffect} from 'react'
import { Colors, Images, UStyle, Fonts } from "../styles/Index";
import { HardCodeData } from './../hardcodedata/LanguageList'
import { LocalizationContext } from '../services/localization/LocalizationContext'
import { ScreenClass } from '../component/comman/ScreenClass';
import { ScreenName } from '../component/comman/ScreenName'
import { FirebaseConfig } from '../component/comman/FirebaseConfig';
import { ClickedButtonName } from '../component/comman/ClickedButtonName'
import { ApiCall } from '../component/comman/ApiCall'
const Language = (props) => {
    const [language, setLanguage] = useState([]); //HardCodeData.LanguageList
    const { appLanguage, initializeAppLanguage, setAppLanguage, translations } = useContext(LocalizationContext);
    const handleSetLanguage = async language => {
        setAppLanguage(language.languageCode);
    };

    useEffect(() => {
        getAllLanguage();
    }, [])

    const getAllLanguage = () => {
        var requestData = {};
        ApiCall.Api_Call('/language/getAlllanguage', '', 'POST', false).then((result) => {
            console.log(result);
            if (result.status == 1) {
                if (result.result.length > 0) {
                    setLanguage(result.result);
                }
            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {
            console.log(error);
        });
    }
    return (
        <View >
            <View >
                {/* style={{ flexDirection: 'row', flexWrap: 'wrap' }} */}
                {language.map((item, index) => (
                    <TouchableOpacity key={index} activeOpacity={Colors.activeOpecity} onPress={() => {
                        FirebaseConfig.realtimeClickedButton(item.language);
                        handleSetLanguage(item,index);
                    }}>
                        <View style={[styles.outerViewComp, { backgroundColor: item.languageCode == appLanguage ? Colors.appColor : '#fff', }]}>
                            <Text style={[styles.textComp, {

                                color: item.languageCode == appLanguage ? '#fefbfe' : 'black'

                            }]}>{item.language}</Text>
                        </View>
                    </TouchableOpacity>
                ))
                }
            </View>
        </View>
    )
}

const styles = StyleSheet.create({

    outerViewComp: {
        marginHorizontal: responsiveWidth(5),
        // width: responsiveWidth(60),
        justifyContent: 'center', alignContent: 'center',
        alignItems: 'center',
        marginTop: responsiveWidth(12),
        borderColor: 'grey',
        borderWidth: responsiveWidth(0.1),
    },
    textComp: {
        borderRadius: responsiveWidth(3),
        paddingHorizontal: responsiveWidth(5),
        paddingVertical: responsiveWidth(2),
        fontFamily: Fonts.Roboto.Roboto_Regular,
        fontSize: responsiveFontSize(2)
    }
})
export default Language;