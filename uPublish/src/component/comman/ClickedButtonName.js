
export const ClickedButtonName = {

    Hindi: "Hindi",
    English: "English",
    LoginFB: "Continue_with_Facebook",
    E_Mail_LogIN: "Log_IN",
    LoginGoggle: "Continue_with_Google",
    EMailLogin_Button: 'Continue_with_Email',
    buttonTitle: 'Login',
    SignUpButtonTitle: 'Sign_Up',
    SignUpSaveButtonTitle: 'Create_Sign_Up',

    Books: "Books",
    Stories: "Stories",
    Library: "Library",
    Orders: "Orders",
    Cart: "Cart",
    Settings: "Setting",
    SendEmail: "Send_Email",
    OrderInvoiceButton: "Order_Invoice",
    DownloadButton: "Download_Invoice",
    LogOutButton: "Log_Out",
    Contact_Submit: "Contact_Submit",

    Buy_Now_Button: "Buy_Now",
    Add_To_Cart: "Add_To_Cart",
    Book_Item_Share: "Book_Item_Share",
    Pay_Button: "Pay",
    Save_Address: "Save_Address",


}