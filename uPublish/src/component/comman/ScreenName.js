export const ScreenName = {
    Language: "Select_Language",
    LOGINGOOGLE: "Social_Log_IN",
    Books: "Books",
    BookDetail: "Book_Details",
    Address: "Purchase_Book",
    Settings: "Setting",
    Orders: "Orders",
    OrdersDetails: "Order_Detail",
    SignUP: "SignUP",
    EmailLogin: "Email_LogIn",
    Stories: "Stories",
    StoriesView: "Stories_View",
    Library: "Library",
    E_BookReader: "E_BookReader",
    InvoicePDFViewer:"Invoice_PDF_Viewer",

}