
import Constants from "../comman/Constant";
var DataReturn = null;
export const SellrBoxAPICall = {
    Api_Call: async (url, data, method, credetial) => {
        var headers;
        if (credetial) {
            headers = {
                Accept: 'application/json',
                // Accept: '*/*',
                'Content-Type': 'application/json',
                // Connection: 'keep-alive',
                'credentials': 'same-origin',
                // Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySUQiOjIxNCwiaWF0IjoxNjEzMjI1MzM1fQ.rMP2V7Rs_vk8vA4OgQu43CJ8XdJa5DLlzyuSfeB5ASk'
                // Connection: 'keep-alive'
                // "x-access-token": JSON.parse(Constants.LogIn_User).token,
            }
        } else {
            headers = { Accept: 'application/json', 'Content-Type': 'application/json', },
                headers.key = "",
                headers.value = "",
                headers.type = "text",
                headers.disabled = true
        }
        // console.log("Headers", " Json Parse Body", data != '' ? JSON.stringify(data) : '',);
        await fetch(Constants.SELLERBOXAPI_URL + url, {
            method: method,
            headers: headers,
            body: data != '' ? JSON.stringify(data) : '',
        }).then(async (response) => await response.json())
            .then((responseJson) => {
                DataReturn = responseJson;
                return responseJson;
            })
            .catch((error) => {
                DataReturn = error;
                alert(error);
                return error;
            });
        return DataReturn;
    }


}