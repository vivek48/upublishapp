
import Constants from "../comman/Constant";
var DataReturn = null;
export const ApiCall = {
    Api_Call: async (url, data, method, credetial) => {
        // console.log(JSON.parse(Constants.LogIn_User).token, "----------------");
        var headers;
        if (credetial) {
            headers = {
                Accept: 'application/json', 'Content-Type': 'application/json',
                "x-access-token": JSON.parse(Constants.LogIn_User).token,
            }
        } else {
            headers = { Accept: 'application/json', 'Content-Type': 'application/json', },
                headers.key = "",
                headers.value = "",
                headers.type = "text",
                headers.disabled = true
        }
        // console.log("Headers",  " Json Parse Body", data != '' ? JSON.stringify(data) : '',);
        await fetch(Constants.API_URL + url, {
            method: method,
            headers: headers,
            body: data != '' ? JSON.stringify(data) : '',
        }).then(async (response) => await response.json())
            .then((responseJson) => {
                DataReturn = responseJson;
                return responseJson;
            })
            .catch((error) => {
                DataReturn = error;
                alert(error);
                return error;
            });
        return DataReturn;
    }
}


// export  ApiCall;