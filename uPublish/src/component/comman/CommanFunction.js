import React, { Component, useContext } from 'react';
import { PermissionsAndroid } from 'react-native';
import Constant from './Constant';
import { Colors, Fonts, Images } from '../../styles/Index'
import AsyncStorage from '@react-native-community/async-storage';

export const CommanFunction = {

    changeDateFormat: (date) => {
        // console.log(date);
        var d2 = CommanFunction.tConvert(new Date(date).toLocaleTimeString());
        var d3 = d2.split(':');
        var d1 = d3[0] + ":" + d3[1] + " " + d3[2].substr(2, 4)
        // console.log(d1);
        var splitDate = date.split('-');
        var date = splitDate[2].split('T');
        switch (splitDate[1]) {
            case '01':
                return date[0] + '-Jan' + " " + d1;  //+ splitDate[0]
                break;
            case '02':
                return date[0] + '-Feb' + " " + d1; //+ splitDate[0]
                break;
            case '03':
                return date[0] + '-Mar' + " " + d1; //+ splitDate[0]
                break;
            case '04':
                return date[0] + '-Apr' + " " + d1; //+ splitDate[0]
                break;
            case '05':
                return date[0] + '-May' + " " + d1; //+ splitDate[0]
                break;
            case '06':
                return date[0] + '-Jun' + " " + d1; //+ splitDate[0] 
                break;
            case '07':
                return date[0] + '-Jul' + " " + d1; //+ splitDate[0] 
                break;
            case '08':
                return date[0] + '-Aug' + " " + d1; //+ splitDate[0]
                break;
            case '09':
                return date[0] + '-Sep' + " " + d1; //+ splitDate[0] 
                break;
            case '10':
                return date[0] + '-Oct' + " " + d1; //+ splitDate[0]
                break;
            case '11':
                return date[0] + '-Nov' + " " + d1; //+ splitDate[0]
                break;
            case '12':
                return date[0] + '-Dec' + " " + d1; //+ splitDate[0]
                break;

        }
    },
    tConvert: (time) => {
        // Check correct time format and split into components
        time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
            time = time.slice(1);  // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join(''); // return adjusted time or original string
    },
    discountRateBook: (listPrice, sellingPrice) => {
        const discount = listPrice - sellingPrice;
        const discountRate = discount * 100 / listPrice;
        // console.log(discountRate);
        return discountRate;
    },
    discountBook: (listPrice, sellingPrice) => {
        const discount = listPrice - sellingPrice;
        return discount;
    },
    buyingBookPrice: (numerOfBook, sellingPrice) => {
        const bookPrice = numerOfBook * sellingPrice;
        return bookPrice;
    },
    setBuyBook: (epubBookURL) => {
        // console.log("SET BUY BOOK-----", epubBookURL);
        // var splitEpub = epubBookURL.split("/");
        // var epubKey = splitEpub[splitEpub.length - 2];
        // var jsonEpub = { epubKey: epubBookURL }
        // console.log(jsonEpub);
        Constant.BuyEPubBookLIST.push(epubBookURL);
        // console.log("SET BUY BOOK", Constant.BuyEPubBookLIST);
        AsyncStorage.setItem(Constant.BuyEPubBook, JSON.stringify(Constant.BuyEPubBookLIST)).then(() => { }).catch((err) => { console.log("eeeeeeee", err); });

    },

    checkImageAvailability: (imageUrl, isbn) => {


        if (imageUrl != null) {
            var splitImage = imageUrl.split("/");
            if (splitImage[0] == "https:") {
                // console.log(splitImage[0]);
                if (isbn !== '' && isbn !== null && isbn !== undefined) {
                    // console.log( Constant.ServerImaagePath + isbn + '_s.jpg');
                    return { uri: Constant.ServerImaagePath + isbn + '_s.jpg' };
                } else {
                    console.log("Else Image URL",imageUrl);
                    return { uri: imageUrl };
                }
            } else {
                return Images.library;
            }
        } else {
            return Images.library;
        }
    },

    paymentOptions: (bookPrice, currency, bookImageUrl, bookTitle, orderID, phoneNumber) => {
        var parseData = JSON.parse(Constant.LogIn_User);

        var options = {
            description: bookTitle,
            image: bookImageUrl,
            currency: currency,
            order_id: orderID,
            // uPublish Key
          //  key: 'rzp_test_OA5EZX2i8xV3zM',// 
            // Repro Prod Key
              key: 'rzp_live_wJC8nCXWqlj6N6',// 'rzp_test_OA5EZX2i8xV3zM',// 
            // key: 'rzp_test_PVee4MGDqPBBGK',// praveen dhegra sir API Key 'rzp_test_I8RVPCOpus2SoX',//   Vivek Payment Key 'rzp_test_WLhSNdXFjVomRf', // Your api key
            amount: bookPrice,
            name: 'Payment',
            prefill: {
                email: parseData.userEmail,
                contact: phoneNumber,//'',
                name: parseData.userName,
            },
            theme: {
                color: Colors.appColor,
                //  '#F37254'
            }
        }
        return options;
    },

    spliArray: (booklist, type) => {
        var booksList = [];
        var fifthBook = [];
        var jsonData = {};
        if (type !== "topsellingbook") {
            for (var i = 1; i <= booklist.length; i++) {
                jsonData = booklist[i - 1];
                fifthBook.push(jsonData);
                // console.log("Check", i % 5 == 0, "i Value", i, i == booklist.length);
                if (i % 5 == 0) {
                    booksList.push(
                        {
                            key: jsonData._id + i,
                            value: 'Popular in Repro Books',
                            // uPublish
                            freeEbook: fifthBook,
                            type: type
                        }
                    );
                    fifthBook = [];
                } else if (i == booklist.length) {
                    // console.log("Check data", fifthBook);
                    booksList.push(
                        {
                            key: jsonData._id + i,
                            value: 'Popular in Repro Books',
                            // uPublish
                            freeEbook: fifthBook,
                            type: type
                        }
                    );
                    fifthBook = [];
                }

            }
        } else {
            booksList.push(
                {
                    key: 1,
                    value: 'Popular in Repro Books ',
                    // uPublish
                    freeEbook: booklist,
                    type: type
                }
            );
        }
        return booksList
    },
    applyCouponAmmount: (ammount, discountValue, discountType) => {
        console.log(ammount, discountValue, discountType);
        var discountAmmount = parseFloat(ammount);
        if (discountType == 'ABSOLUTE') {
            discountAmmount = discountAmmount - parseFloat(discountValue);

        } else {
            var percentageAmmount = discountAmmount * parseFloat(discountValue) / 100;
            discountAmmount = discountAmmount - percentageAmmount;

        }
        console.log(discountAmmount);

        return discountAmmount
    },

    orderDateFormatter: (dateString) => {
        var date = new Date(dateString);
        var dateTime = date.toLocaleTimeString()
        var dateArray = date.toDateString().split(" ");
        var dateArrayConvert = dateArray[2] + "-" + dateArray[1] + "-" + dateArray[3] + " " + CommanFunction.tConvert(dateTime);
        return dateArrayConvert
    },
    storiesSplitArray: (storieslist) => {
        var storiesList = [];
        var threeBook = [];
        var jsonData = {};
        for (var i = 1; i <= storieslist.length; i++) {
            jsonData = storieslist[i - 1];
            threeBook.push(jsonData);
            if (i % 3 == 0) {
                storiesList.push(
                    {
                        key: jsonData.contentId + i,
                        freeEbook: threeBook,
                        // type: type
                    }
                );
                threeBook = [];
            } else if (i == storieslist.length) {
                storiesList.push(
                    {
                        key: jsonData.contentId + i,
                        freeEbook: threeBook,
                        // type: type
                    }
                );
                threeBook = [];
            }
        }
        return storiesList
    },
    storeFiveReviewsOnlyForBookDetails: (reviwsLList) => {
        var fiveReview = [];
        if (reviwsLList.length > 3) {
            for (var i = 0; i < 3; i++) {
                fiveReview.push(reviwsLList[i]);
            }
        } else {
            fiveReview = reviwsLList;
        }
        return fiveReview

    },

    formatAMPM: (date) => {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    },
    chatHistoryChangeData: (item) => {
        let historyArrayList = [];

        for (let i = 0; i < item.length; i++) {

            historyArrayList.push({
                id: item[i]._id,
                to: item[i].toUser,
                from: '',
                text: item[i].message,
                fromID: item[i].fromUser,
                type: item[i].status == 'DELIVERED' ? 'Receive' : 'Deliver',
                date: new Date(item[i].createdOn),
            })
        }
        //  console.log(historyArrayList);
        return historyArrayList
    },
    currencyChangeValue: (previousCurrencyValue, currentCurrencyValue) => {
        return (previousCurrencyValue * currentCurrencyValue).toFixed(2);
    },

    ValidateEmail: (mail) => {
        var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (mail.match(mailformat)) {
            return true;
        }
        else {
            return false;
        }
    },
    CheckPassword: (password) => {
        var paswd=  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
        // var paswd = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,15}$/;
        if (password.match(paswd)) {
            return true;
        }
        else {
            return false;
        }
    },
    CheckPhoneNumber: (phoneNumber) => {
        var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
        if (filter.test(phoneNumber)) {
            if (phoneNumber.length == 10) {
                return true
            } else {
                return false
            }
        }
        else {
            return false
        }
    },
    CheckName: (name) => {
        if (/^[A-Za-z\s]+$/.test(name)) {
            return true;
        } else {
            return false;
        }
    },
    
}