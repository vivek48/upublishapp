
import React from 'react';
import { StyleSheet, View, Modal, ActivityIndicator, ImageBackground, Image } from 'react-native';
import { Images, Fonts, Colors } from '../../styles/Index'
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
const Loader = props => {
    const {
        loading,
        ...attributes
    } = props;

    return (
        <Modal
            transparent={true}
            animationType={'fade'}
            visible={loading}
            onRequestClose={() => { }}>
            <View style={styles.modalBackground}>
                <ImageBackground source={Images.loading} style={{
                    height: responsiveWidth(30),
                    width: responsiveWidth(30),
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-around',
                }}>
                </ImageBackground>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default Loader;