export const ScreenClass = {
    Language: "Language",
    LOGINGOOGLE: "Social_Log_IN",
    Books: "Home",
    BookDetail: "Book_Details",
    Address: "User_Address",
    Settings: "Settings",
    Orders: "Orders",
    OrdersDetails: "Order_Detail",
    SignUP: "SignUP",
    EmailLogin: "Email_LogIn",

    Stories: "Stories",
    StoriesView: "Stories_View",
    Library: "Library",
    E_BookReader: "E_BookReader",
    InvoicePDFViewer:"Invoice_PDF_Viewer",

}