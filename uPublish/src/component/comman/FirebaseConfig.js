import analytics from '@react-native-firebase/analytics';

export const FirebaseConfig = {

    realtimeScreenLogContent: (screen_class, screen_name) => {
        // console.log(screen_class);
        analytics().logScreenView({
            screen_class: screen_class,
            screen_name: screen_name
        })
    },
    realtimeClickedButton: (buttonName) => {
        // console.log(screen_class);
        analytics().logEvent("custom_clicked_event", { "Clicked_Button": buttonName });
    }
}


