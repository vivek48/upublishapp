import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import AwesomeAlert from 'react-native-awesome-alerts';
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions'
import { Images, Fonts, Colors } from '../../styles/Index'
const ValidationPopup = ({ message, showHide }, props) => {
    return (
        <View >
            <AwesomeAlert
                contentContainerStyle={styles.containerStyle}
                show={true}
                showProgress={false}
                message={message}
                closeOnTouchOutside={false}
                closeOnHardwareBackPress={false}
                showConfirmButton={true}
                messageStyle={{ fontFamily: Fonts.Roboto.Roboto_Medium, fontSize: responsiveFontSize(1.8) }}
                confirmText="Ok"
                confirmButtonTextStyle={{
                    fontFamily: Fonts.Roboto.Roboto_Bold,
                    fontSize: responsiveFontSize(2),
                    alignItems: 'center',
                    justifyContent: 'center',
                    // padding: responsiveWidth(0.5),
                }}
                confirmButtonStyle={{
                    backgroundColor: '#ffc107',
                    width: responsiveWidth(69),
                    marginTop: responsiveWidth(8),
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: responsiveWidth(9)  //Colors.appColor,
                }}
                onConfirmPressed={showHide}
            />
        </View>
    );
};


const styles = StyleSheet.create({
    containerStyle: {
        backgroundColor: '#ffff',
        width: responsiveWidth(75),
        // height: responsiveHeight(25),
        justifyContent: 'space-around',
        alignItems: 'center'

    }
});
export default ValidationPopup