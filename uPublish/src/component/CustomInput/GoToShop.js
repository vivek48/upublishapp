import React, { Component, useContext, useEffect, useState } from 'react';
import { View, Text, FlatList, StyleSheet, ScrollView, Image, TouchableOpacity } from 'react-native';
import Header from "../../component/CustomInput/Header";
import { LocalizationContext } from '../../services/localization/LocalizationContext';
import { ApiCall } from "../../component/comman/ApiCall";
import Loader from '../../component/comman/Loader'
import { Colors, Fonts, Images } from '../../styles/Index';
import { responsiveFontSize, responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions'
import { BuyingBookContext } from "../../services/buyingbook/BuyingBookContext";
import { WebView } from 'react-native-webview';
const GoToShop = (props) => {
    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);
    const { setUserBuyingBook, userBuyingBook } = useContext(BuyingBookContext);
    const [isLoading, setIsLoading] = useState(false);
    const [productURL, setProductURL] = useState(props.route.params.ProductURL);
    useEffect(() => {
    }, [])
    const closeExistsAddress = () => {
        props.navigation.navigate("BookDetails")
    }
    return (
        <View>
            <Loader loading={isLoading} />
            <Header headerTitle={translations.PurchaseBook} backScreen={closeExistsAddress} navigation={props.navigation} />

            <View style={{ height: responsiveHeight(100), width: responsiveWidth(100) }}>
                {productURL != '' &&
                    <WebView
                        style={{ flex: 1 }}
                        originWhitelist={['*']}
                        source={{
                            uri: productURL
                        }}
                        style={{ marginTop: 0 }}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                    />
                }
            </View>
        </View >
    )
}

const styles = StyleSheet.create({


})

export default GoToShop;
