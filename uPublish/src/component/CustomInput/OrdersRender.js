import React, { Component, useState, useContext } from 'react';
import { View, Text, Image, StyleSheet, SafeAreaView, ScrollView, TouchableOpacity, FlatList } from 'react-native';
import { responsiveWidth, responsiveFontSize, responsiveHeight } from 'react-native-responsive-dimensions';
import { Images, Fonts, Colors } from '../../styles/Index'
import { ApiCall, } from '../comman/ApiCall'
import { LocalizationContext } from '../../services/localization/LocalizationContext';
import Loader from '../comman/Loader'
import { CommanFunction } from "../comman/CommanFunction";
import { BuyingBookContext } from '../../services/buyingbook/BuyingBookContext';

import { ClickedButtonName } from '../comman/ClickedButtonName';

import { FirebaseConfig } from '../../component/comman/FirebaseConfig';
const OrdersRender = ({ item, index, navigation, }) => {
    const [isLoading, setIsLoading] = useState(false);
    const { setUserBuyingBook, userBuyingBook } = useContext(BuyingBookContext);
    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);


    const navigateOrderDetail = (item) => {
        FirebaseConfig.realtimeClickedButton(item.order_no + "_" + ClickedButtonName.Orders);
        var userData = {};
        var order = {};
        order.orderno = item.order_no;
        userData.order = order;
        setIsLoading(true);
        ApiCall.Api_Call('/order/fetchOrderDetails', userData, 'POST', true).then((result) => {
            setIsLoading(false)
            // console.log(result);
            if (result.status == 1) {
                navigation.navigation.navigate("OrderDetail", {orderItem:result.result})
            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });


    }


    return (
        <>
            <SafeAreaView key={index} style={[styles.safeAreaViewThird,]}>

                <Loader loading={isLoading} />
                <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { navigateOrderDetail(item) }}>
                    <>
                        <View>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={styles.outerView}>
                                    <View>
                                        <Text style={styles.headingView}>{translations.OrderNumber} </Text>
                                    </View>
                                    <View>
                                        <Text style={styles.textFontSize}>:{" " + item.order_no}</Text>
                                    </View>
                                </View>
                                <View style={styles.secondOuterView}>
                                    <View>
                                        <Text style={styles.headingView}>{translations.OrderAmmout} </Text>
                                    </View>
                                    <View>
                                        <Text style={styles.textFontSize}>:{" " + item.order_amount}</Text>
                                    </View>
                                </View>
                            </View>
                            {/* <View style={{ flexDirection: 'row' }}>
                        <View style={styles.outerView}>
                            <View>
                                <Text style={styles.headingView}>{translations.OrderChannel}  </Text>
                            </View>
                            <View>
                                <Text>{" " + item.order_no}</Text>
                            </View>
                        </View>
                        <View style={styles.secondOuterView}>
                            <View>
                                <Text style={styles.headingView}>{translations.OrderPending}</Text>
                            </View>
                            <View>
                                <Text>{" " + item.order_amount}</Text>
                            </View>
                        </View>
                    </View> */}

                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', }}>

                                <View style={[styles.outerView,]}>
                                    <View>
                                        <Text style={styles.headingView}>{translations.OrderDate} </Text>
                                    </View>
                                    <View>
                                        <Text style={styles.textFontSize}>:{" " + CommanFunction.changeDateFormat(item.order_date)}</Text>
                                    </View>
                                </View>

                                {/* <View style={styles.outerView}>
                            <View>
                                <Text style={styles.headingView}>{translations.OrderBusinessUnit} </Text>
                            </View>
                            <View>
                                <Text style={styles.textFontSize}>:{" " + item.businessUnit}</Text>
                            </View>
                        </View> */}
                                <View style={styles.secondOuterView}>
                                    <View>
                                        <Text style={styles.headingView}>{translations.OrderStatus} </Text>
                                    </View>
                                    <View>
                                        <Text style={styles.textFontSize}>:{" " + item.upublishStatus}</Text>
                                    </View>
                                </View>
                            </View>
                            {/* <View style={{ flexDirection: 'row', }}>
                        <View style={[styles.outerView, { width: responsiveWidth(80) }]}>
                            <View>
                                <Text style={styles.headingView}>{translations.OrderDate} </Text>
                            </View>
                            <View>
                                <Text style={styles.textFontSize}>:{" " + CommanFunction.changeDateFormat(item.order_date)}</Text>
                            </View>
                        </View>
                    </View> */}
                            <View style={{ flexDirection: 'row' }}>
                                <View style={[styles.outerView,]}>
                                    <View>
                                        <Text style={styles.headingView}>{translations.OrderShipDate} </Text>
                                    </View>
                                    <View>
                                        <Text style={styles.textFontSize}>:{" " + CommanFunction.changeDateFormat(item.shipby_date)}</Text>
                                    </View>
                                </View>
                                {/* <View style={[styles.outerView,]}>
                                    <View>
                                        <Text style={styles.headingView}>{translations.Quantity} </Text>
                                    </View>
                                    <View>
                                        <Text style={styles.textFontSize}>:{" " + item.items[0].order_qty}</Text>
                                    </View>
                                </View> */}
                            </View>
                        </View>
                    </>
                </TouchableOpacity>
            </SafeAreaView>
        </>
    )
}

const styles = StyleSheet.create({
    flex: 1,
    headingView: {
        fontFamily: Fonts.Roboto.Roboto_Bold,
        fontSize: responsiveFontSize(1.5),
    },
    safeAreaViewThird: {
        width: responsiveWidth(100),
        paddingHorizontal: responsiveWidth(2),
        paddingVertical: responsiveWidth(2),
        backgroundColor: '#fefefe',
        borderBottomColor: 'grey',
        borderBottomWidth:0.3,// responsiveWidth(0.1),
    },
    outerView: {
        width: responsiveWidth(56), flexDirection: 'row',
        alignItems: 'center',
    },
    secondOuterView: {
        width: responsiveWidth(40),
        flexDirection: 'row',
        alignItems: 'center', justifyContent: 'flex-start'
    },
    textFontSize: { fontSize: responsiveFontSize(1.5) }

})

export default OrdersRender;