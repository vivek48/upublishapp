import React, { Component, useContext, useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, TextInput, TouchableOpacity, Animated, Modal } from 'react-native';
import Header from "../CustomInput/Header"
import { LocalizationContext } from '../../services/localization/LocalizationContext';
import { Colors, Fonts, Images } from '../../styles/Index'
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import CheckBox from '@react-native-community/checkbox';
const CustomModal = ({ sellerBoxData, applyOrCloseModal, modalOpen }, props) => {

    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);
    const [value] = useState(new Animated.Value(0))

    useEffect(() => {
        Animated.timing(value, {
            toValue: value,
            duration: 100,
            useNativeDriver: true,
        }).start() // < Don't forget to start!
    }, [])
    // console.log(sellerBoxData);
    return (
        <Modal
            transparent={true}
            animationType={'slide'}
            visible={modalOpen}
            onRequestClose={() => { }}>
            <Animated.View style={[styles.modalBackground,]}>
                <View style={styles.centerModal}>
                    <View>
                        <Text style={styles.sellingHeaderText}>Selling Price other Seller</Text>
                    </View>
                    {sellerBoxData.map((sellerItem, index) => (
                        <View key={index} style={{ width: responsiveWidth(84.3), marginVertical: responsiveWidth(2), borderBottomWidth: responsiveWidth(0.2) }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <View>
                                    <Text style={[styles.sellingHeaderText, { fontSize: responsiveFontSize(1.5) }]}>Seller Name : </Text>
                                </View>
                                <View>
                                    <Text>{sellerItem._source.sellerName} </Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <View>
                                    <Text style={[styles.sellingHeaderText, { fontSize: responsiveFontSize(1.5) }]}>Selling Price : </Text>
                                </View>
                                <View>
                                    <Text>{sellerItem._source.currency} {' ' + sellerItem._source.sp}</Text>
                                </View>
                            </View>
                        </View>
                    ))
                    }
                    <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { applyOrCloseModal('close') }}>
                        <View style={styles.buttonStyle}>
                            <Text style={styles.textStyle}>Ok</Text>
                            {/* {translations.FilterCloseButton} */}
                        </View>
                    </TouchableOpacity>
                </View>
            </Animated.View>
        </Modal>
    )
}
const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        marginTop: responsiveWidth(40)
    },
    centerModal: {
        backgroundColor: 'lightgrey',
        width: responsiveWidth(88),
        borderRadius: responsiveWidth(4),
        borderRadius: responsiveWidth(2),
        backgroundColor: '#ffff',
        shadowColor: '#30C1DD',
        alignItems: 'center',
        paddingBottom: responsiveWidth(4),
        shadowRadius: 3,
        shadowOpacity: 0.6,
        elevation: 8,
        shadowOffset: { width: 0, height: 4 },
    },
    buttonStyle: {
        marginTop: responsiveWidth(5),
        backgroundColor: '#ffc107',
        borderRadius: responsiveWidth(1),
        width: responsiveWidth(20),
        justifyContent: 'center',
        height: responsiveWidth(9),
        width: responsiveWidth(70)
    },
    textStyle: {
        textAlign: 'center',
        fontFamily: Fonts.Roboto.Roboto_Bold, color: '#ffff',
        fontSize: responsiveFontSize(2)
    },
    sellingHeaderText: {
        padding: responsiveWidth(2),
        fontFamily: Fonts.Roboto.Roboto_Bold,
        fontSize: responsiveFontSize(2),
        letterSpacing: responsiveWidth(0.5)
    }


});


export default CustomModal;