import React, { Component, useState, useContext } from 'react';
import { View, Text, Image, StyleSheet, SafeAreaView, ScrollView, TouchableOpacity, FlatList } from 'react-native';
import { responsiveWidth, responsiveFontSize, responsiveHeight } from 'react-native-responsive-dimensions';
import { Images, Fonts, Colors } from '../../styles/Index'
import { ApiCall, } from '../comman/ApiCall'
import Loader from '../comman/Loader'
import { CommanFunction } from "../comman/CommanFunction";
import { BuyingBookContext } from '../../services/buyingbook/BuyingBookContext'
import { LocalizationContext } from '../../services/localization/LocalizationContext';
const ShopBookRender = ({ item, index, navigation, }) => {
    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);
    const [isLoading, setIsLoading] = useState(false);
    const { setUserBuyingBook, userBuyingBook } = useContext(BuyingBookContext);
    const getBuyBook = (bookItem) => {
        setUserBuyingBook({ bookItem })
        navigation.navigation.navigate('Address', { routeName: 'Search' })
    }
    return (
        <>
            <SafeAreaView key={index} style={[styles.safeAreaViewThird,]}>
                <Loader loading={isLoading} />
                <View style={{ flexDirection: 'row',alignItems:'center' }}>
                    <View style={{ paddingVertical: responsiveWidth(1), justifyContent: 'center', paddingTop: responsiveWidth(1) }}>
                        <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={
                            () => { navigation.navigation.navigate('BookDetails', { bookItem: item,routeName: 'Search' }) }}>
                            <View style={styles.bookImageOterView}>
                                <Image resizeMode={'center'} source={CommanFunction.checkImageAvailability(item._source.thumbnail)} style={{
                                    width: responsiveWidth(22),
                                    height: responsiveHeight(19),
                                }}></Image>
                            </View>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flexDirection: 'column', flexWrap: 'wrap', width: responsiveWidth(73.5), paddingLeft: responsiveWidth(4) }}>
                        <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={
                            () => { navigation.navigation.navigate('BookDetails', { bookItem: item, routeName: 'Search' }) }}>
                            <View style={styles.headerViewStyle} >
                                <Text style={styles.authorStyle}>{item._source.skutitle} {"(" + item._source.bindingtype + ")"}</Text>
                            </View>
                            <View style={styles.headerViewStyle} >
                                <Text style={styles.descriptionText}>{+ item._source.pagecount + " pages"}
                                    {/* 
                            {item._source.longdescription.length>54?
                            item._source.longdescription.substr(0,50)+'...........': item._source.longdescription} */}
                                </Text>
                            </View>
                            <View style={[styles.headerViewStyle, { width: responsiveWidth(67), }]} >
                                <Text style={[styles.descriptionText, { flexWrap: 'wrap', fontFamily: Fonts.Roboto.Roboto_Regular }]}>{translations.AuthorBy} :
                                <Text style={{
                                        flexWrap: 'wrap', fontFamily: Fonts.Roboto.Roboto_Bold,
                                        color: '#17a2b8'
                                    }}>{" " + item._source.authorname + " | "}</Text>
                                    {translations.PublishedBy}
                                    <Text style={{
                                        fontFamily: Fonts.Roboto.Roboto_Bold,
                                        color: '#17a2b8',
                                        lineHeight:responsiveWidth(4),
                                    }}>{" " + item._source.publishername}</Text>
                                </Text>
                            </View>
                            <View style={styles.headerViewStyle} >
                                <Text>{item._source.currency}{" " + item._source.sp}</Text>
                            </View>
                            {item._source.sp !== item._source.rrp && <>
                                <View style={styles.headerViewStyle} >
                                    <Text>{" " + item._source.rrp}</Text>
                                </View>
                                <View style={styles.headerViewStyle} >
                                    <Text>{translations.SaveDiscount} {" " + item._source.currency + " " + CommanFunction.discountBook(item._source.rrp, item._source.sp,) + " (" + CommanFunction.discountRateBook(item._source.rrp, item._source.sp,) + "%)"} </Text>
                                </View>
                            </>
                            }
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { getBuyBook(item._source) }} >
                            <View style={styles.buttonStyle}>
                                <Text style={styles.buttonText}>{translations.BuyButton}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>

        </>
    )
}

const styles = StyleSheet.create({
    flex: 1,
    titleStyle: {
        opacity: 0.7,
        color: '#000000', fontSize: responsiveFontSize(2),
        fontFamily: Fonts.Roboto.Roboto_Bold,
    },
    authorStyle: {
        color: '#17a2b8',
        fontSize: responsiveFontSize(1.7),
        fontFamily: Fonts.Roboto.Roboto_Black,
    },

    buttonStyle: {
        marginVertical: responsiveWidth(2)
        , width: responsiveWidth(28), height: responsiveWidth(9),
        borderRadius: responsiveWidth(2), backgroundColor: '#17a2b8', padding: responsiveWidth(1.8),
    },
    buttonText: {
        textAlign: 'center',
        fontFamily: Fonts.Roboto.Roboto_Bold,
        color: 'white', fontSize: responsiveFontSize(1.8)
    },
    descriptionText: {
        color: 'black',
        fontSize: responsiveFontSize(1.5),
        fontFamily: Fonts.Roboto.Roboto_Bold,
    },
    headerViewStyle: {
        paddingBottom: responsiveWidth(0.4),
    },
    safeAreaViewThird: {
        width: responsiveWidth(100),
        paddingHorizontal: responsiveWidth(4),
        paddingVertical: responsiveWidth(2),
        backgroundColor: '#fefefe',
        borderBottomColor: 'grey',
        borderBottomWidth: responsiveWidth(0.1),
    },

    bookImageOterView: {
        justifyContent: 'center',
        alignItems: 'center',
        opacity: 0.8,
        borderRadius: responsiveWidth(2),
        backgroundColor: '#ffff',// '#d9d9d9',
        shadowColor: '#30C1DD',
        shadowRadius: 10,
        shadowOpacity: 0.6,
        elevation: 8,
        // width: responsiveWidth(),
        width: responsiveWidth(25),
        // height: responsiveHeight(12),
        height: responsiveHeight(20),
        shadowOffset: { width: 0, height: 4 },
    },
})

export default ShopBookRender;