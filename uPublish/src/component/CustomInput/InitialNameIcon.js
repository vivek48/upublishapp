import React, { Component, useContext, useEffect, useState } from 'react'
import { View, Text, TextInput, SafeAreaView, TouchableOpacity, Image, StyleSheet, TouchableHighlight } from 'react-native'
import Constants from "../comman/Constant";
import { Colors, Images, Fonts } from '../../styles/Index';
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions'
const InitialNameIcon = ({ IconName }) => {
  //   const [userName, setUserName] = useState('');
  //   useEffect(() => {
  //     setUserName(JSON.parse(Constants.LogIn_User).user.userName)
  // }, [userName])
  return (
    <View
      style={{
        backgroundColor: Colors.appColor,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: responsiveWidth(5), width: responsiveWidth(9), height: responsiveWidth(9)
      }}>
      <Text style={{ color: 'white', fontSize: responsiveFontSize(2.5) }}>{IconName}
        {/* {userName.substring(0, 1)} */}
      </Text>
    </View>
  );
};

export default InitialNameIcon;