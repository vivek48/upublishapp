import React, { Component, useContext, useEffect, useState } from 'react'
import { View, Text, TextInput, SafeAreaView, TouchableOpacity, Image, StyleSheet, TouchableHighlight } from 'react-native'
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions'
import { Colors, Images, Fonts } from '../../styles/Index';
import { LocalizationContext } from '../../services/localization/LocalizationContext';
const ReturnCancellation = (props) => {

    const { appLanguage, initializeAppLanguage, setAppLanguage, translations } = useContext(LocalizationContext);

    return (
        <SafeAreaView>
            <View style={styles.searchOuterView}>
                <Text style={styles.textStyle}>{translations.CancellationText}</Text>
            </View>
        </SafeAreaView>
    )
}
export default ReturnCancellation;
const styles = StyleSheet.create({
    searchOuterView: {
        paddingHorizontal: responsiveWidth(3),
        paddingVertical: responsiveWidth(3),
    },

    textStyle: {
        fontFamily: Fonts.Roboto.Roboto_LightItalic,
        fontSize: responsiveFontSize(1.6),
        color: 'black',
        textAlign: 'center',
        textDecorationLine: 'underline',
        letterSpacing: responsiveWidth(0.1)
    }


})

