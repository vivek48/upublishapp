import React, { Component, useState, useContext, useEffect } from 'react';
import { View, Text, Image, StyleSheet, SafeAreaView, ScrollView, TouchableOpacity, FlatList } from 'react-native';
import { responsiveWidth, responsiveFontSize, responsiveHeight } from 'react-native-responsive-dimensions';
import { Images, Fonts, Colors } from '../../styles/Index'
import { ApiCall, } from '../comman/ApiCall'
import Loader from '../comman/Loader'
import { CommanFunction } from "../comman/CommanFunction";
import { LocalizationContext } from "../../services/localization/LocalizationContext";
import CustomRadioButton from "./CustomRadioButton";
import CustomInput from "./CustomInput";

import { ScreenClass } from '../../component/comman/ScreenClass'
import { ScreenName } from '../../component/comman/ScreenName';
import { FirebaseConfig } from '../../component/comman/FirebaseConfig'
import { ClickedButtonName } from '../../component/comman/ClickedButtonName';
const AddNewAddressRender = ({ selectedTab, navigation, routeName }, props) => {
    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);
    const [isLoading, setIsLoading] = useState(false);
    const [name, setName] = useState('');
    const [nameError, setNameError] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [phoneNumberError, setPhoneNumberError] = useState('');
    const [address1, setAddress1] = useState('');
    const [address1Error, setAddress1Error] = useState('');
    const [address2, setAddress2] = useState('');
    const [address2Error, setAddress2Error] = useState('');
    const [zipCode, setZipCode] = useState('');
    const [zipCodeError, setZipCodeError] = useState('');
    const [city, setCity] = useState('');
    const [cityError, setCityError] = useState('');
    const [state, setState] = useState('');
    const [stateError, setStateError] = useState('');
    const [country, setCountry] = useState('');
    const [countryError, setCountryError] = useState('');
    useEffect(() => {

    }, [])

    const setAddressName = (value) => {
        setNameError(false)
        setName(value);
    }
    const setAddressPhoneNumber = (value) => {
        setPhoneNumberError(false)
        setPhoneNumber(value);
    }
    const setNewAdreessAddress1 = (value) => {
        setAddress1Error(false)
        setAddress1(value);
    }

    const setNewAdreessAddress2 = (value) => {
        setAddress2Error(false)
        setAddress2(value);
    }
    const setNewAdreessZipCode = (value) => {
        setZipCodeError(false)
        setZipCode(value);
        if (value.length >= 6) {
            console.log(value);
            getStateCity(value);
        }
    }

    const setNewAdreessCity = (value) => {
        setCityError(false)
        setCity(value);
    }

    const setNewAdreessState = (value) => {
        setStateError(false)
        setState(value);
    }

    const setNewAdreessCountry = (value) => {
        setCountryError(false)
        setCountry(value);
    }

    const saveNewAddress = () => {
        FirebaseConfig.realtimeClickedButton(ClickedButtonName.Save_Address);
        var userData = {}
        var address = {};
        // if (name.trim() == '' || name.trim() == undefined || name.trim() == null) {
        //     setNameError(true);
        //     return true;
        // } else {
        if (CommanFunction.CheckName(name)) {
            address.name = name.trim();
            setNameError(false);
        } else {
            setNameError(true);
            return true;
        }
        // }

        if (address1.trim() == '' || address1.trim() == undefined || address1.trim() == null) {
            setAddress1Error(true);
            return true;
        } else {
            address.address1 = address1.trim();
            setAddress1Error(false);
        }
        if (address2.trim() == '' || address2.trim() == undefined || address2.trim() == null) {
            setAddress2Error(true);
            return true;
        } else {
            address.address2 = address2.trim();
            setAddress2Error(false);
        }
        // if (phoneNumber.trim() == '' || phoneNumber.trim() == undefined || phoneNumber.trim() == null) {
        //     setPhoneNumberError(true);
        //     return true;
        // } else {
        if (CommanFunction.CheckPhoneNumber(phoneNumber)) {
            address.phoneNumber = phoneNumber.trim();
            setPhoneNumberError(false);
        } else {
            setPhoneNumberError(true);
            return true;
        }
        // }
        if (zipCode.trim() == '' || zipCode.trim() == undefined || zipCode.trim() == null) {
            setZipCodeError(true);
            return true;
        } else {
            address.pincode = zipCode.trim();
            setZipCodeError(false);
        }

        if (city.trim() == '' || city.trim() == undefined || city.trim() == null) {
            setCityError(true);
            return true;
        } else {
            address.city = city.trim();
            setCityError(false);
        }
        if (state.trim() == '' || state.trim() == undefined || state.trim() == null) {
            setStateError(true);
            return true;
        } else {
            address.state = state.trim();
            setStateError(false);
        }
        if (country.trim() == '' || country.trim() == undefined || country.trim() == null) {
            setCountryError(true);
            return true;
        } else {
            address.country = country.trim();
            setCountryError(false);
        }
        userData.address = address;
        // console.log(userData);
        setIsLoading(true)
        ApiCall.Api_Call('/address', userData, 'POST', true).then((result) => {
            setIsLoading(false)
            // console.log(result);
            if (result.status == 1) {
                selectedTab();
                if (result.result != null) {

                }
            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });
    }
    const getStateCity = (pincode) => {
        setIsLoading(true);
        ApiCall.Api_Call('/pincode/' + pincode, '', 'GET', true).then((result) => {
            setIsLoading(false)
            // console.log(result);
            if (result.status == 1) {
                if (result.result != null) {
                    setCity(result.result.cityName);
                    setState(result.result.stateName);
                    setCountry(result.result.countryName);
                } else {
                    setCity('');
                    setState('');
                    setCountry('');
                }
            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });

    }
    const closeNewAddress = () => {
        navigation.navigation.navigate(routeName);
    }
    return (
        <ScrollView removeClippedSubviews={true} scrollEnabled={true} nestedScrollEnabled={true} >
            <View>
                <Loader loading={isLoading} />
                <View style={{ paddingVertical: responsiveWidth(2), marginTop: responsiveWidth(2) }}>
                    <CustomInput inputvalue={name} OnChangeText={setAddressName}
                        errorField={nameError}
                        secureText={false}
                        // textType={'text'}
                        label={translations.NameLabel} placeholder={translations.NamePlaceHolder} />
                </View>
                {nameError != '' && nameError &&
                    <View style={{ paddingBottom: responsiveWidth(1), paddingLeft: responsiveWidth(1) }}>
                        <Text style={styles.errorColor}>{translations.NameTextValidation}</Text>
                    </View>
                }

                <View style={{ paddingVertical: responsiveWidth(2), }}>
                    <CustomInput inputvalue={address1} OnChangeText={setNewAdreessAddress1}
                        errorField={address1Error}
                        secureText={false}
                        label={translations.AddressLine1Label} placeholder={translations.AddressLine1PlaceHolder} />
                </View>
                {address1Error != '' && address1Error &&
                    <View style={{ paddingBottom: responsiveWidth(1), paddingLeft: responsiveWidth(1) }}>
                        <Text style={styles.errorColor}>{translations.AddressLine1Validation}</Text>
                    </View>
                }
                <View style={{ paddingVertical: responsiveWidth(2), }}>
                    <CustomInput inputvalue={address2} OnChangeText={setNewAdreessAddress2}
                        errorField={address2Error}
                        secureText={false}
                        label={translations.AddressLine2Label} placeholder={translations.AddressLine2PlaceHolder} />
                </View>
                {address2Error != '' && address2Error &&
                    <View style={{ paddingBottom: responsiveWidth(1), paddingLeft: responsiveWidth(1) }}>
                        <Text style={styles.errorColor}>{translations.AddressLine2Validation}</Text>
                    </View>
                }
                <View style={{ paddingVertical: responsiveWidth(2), }}>
                    <CustomInput inputvalue={phoneNumber} OnChangeText={setAddressPhoneNumber}
                        errorField={phoneNumberError}
                        secureText={false}
                        textType={'numeric'}
                        label={translations.ContactNumber} placeholder={translations.ContactNumberPlaceHolder} />
                </View>
                {phoneNumberError != '' && phoneNumberError &&
                    <View style={{ paddingBottom: responsiveWidth(1), paddingLeft: responsiveWidth(1) }}>
                        <Text style={styles.errorColor}>{translations.PhoneNumberValidation}</Text>
                    </View>
                }
                <View style={{ paddingVertical: responsiveWidth(2), }}>
                    <CustomInput inputvalue={zipCode} OnChangeText={setNewAdreessZipCode}
                        errorField={zipCodeError}
                        secureText={false}
                        textType={'numeric'}
                        label={translations.ZipCode} placeholder={translations.ZipCodePlaceHolder} />
                </View>
                {zipCodeError != '' && zipCodeError &&
                    <View style={{ paddingBottom: responsiveWidth(1), paddingLeft: responsiveWidth(1) }}>
                        <Text style={styles.errorColor}>{translations.ZipCodeValidation}</Text>
                    </View>
                }
                <View style={{ paddingVertical: responsiveWidth(2), }}>
                    <CustomInput inputvalue={city} OnChangeText={setNewAdreessCity}
                        errorField={cityError}
                        secureText={false}
                        label={translations.City} placeholder={translations.CityPlaceHolder} />
                </View>
                {cityError != '' && cityError &&
                    <View style={{ paddingBottom: responsiveWidth(1), paddingLeft: responsiveWidth(1) }}>
                        <Text style={styles.errorColor}>{translations.CityValidation}</Text>
                    </View>
                }

                <View style={{ paddingVertical: responsiveWidth(2), }}>
                    <CustomInput inputvalue={state} OnChangeText={setNewAdreessState}
                        errorField={stateError}
                        secureText={false}
                        label={translations.State} placeholder={translations.StatePlaceHolder} />
                </View>
                {stateError != '' && stateError &&
                    <View style={{ paddingBottom: responsiveWidth(1), paddingLeft: responsiveWidth(1) }}>
                        <Text style={styles.errorColor}>{translations.StateValidation}</Text>
                    </View>
                }
                {/* <View style={{ paddingVertical: responsiveWidth(2), }}>
                    <CustomInput inputvalue={name} OnChangeText={setAddressName}
                        errorField={countryError}
                        secureText={false}
                        label={"Country"} placeholder={"Enter Country *"} />
                </View>
                {countryError != '' && countryError &&
                    <View style={{ paddingBottom: responsiveWidth(1), paddingLeft: responsiveWidth(1) }}>
                        <Text style={styles.errorColor}>Country field required</Text>
                    </View>
                } */}

                <View style={{
                    flexDirection: 'row', justifyContent: 'flex-end',
                    alignItems: 'center'
                }}>
                    <TouchableOpacity activeOpacity={Colors.activeOpecity}
                        onPress={() => { saveNewAddress() }}
                    >
                        <View style={styles.buttonStyle}>
                            <Text style={styles.buttonText}>{translations.SaveButton}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
    )
}
const styles = StyleSheet.create({
    flex: 1,
    listHeaderViewAddress: {
        justifyContent: 'space-between',
        paddingVertical: responsiveWidth(1),
        flexDirection: 'row'
    },
    listHeaderTextAddress: {
        fontSize: responsiveFontSize(1.7),
    },
    buttonStyle: {
        width: responsiveWidth(88),
        borderRadius: responsiveWidth(2),
        backgroundColor: '#ffc107', alignItems: 'center',
        marginHorizontal: responsiveWidth(5.8),
        paddingVertical: responsiveWidth(0.6),
        marginTop: responsiveWidth(3)
        // marginRight: responsiveWidth(3)
    },
    buttonText: {
        fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(2),
        // color: 'black',// '#ffff',
        color: '#fff',
        paddingVertical: responsiveWidth(2)
    },
    errorColor: {
        color: 'red', fontSize: responsiveFontSize(1.5), paddingHorizontal: responsiveWidth(7)
    }
})

export default AddNewAddressRender;