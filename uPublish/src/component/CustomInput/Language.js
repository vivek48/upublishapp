import React, { Component, useContext, useEffect, useState } from 'react'
import { View, Text, TextInput, SafeAreaView, TouchableOpacity, Image, StyleSheet, TouchableHighlight } from 'react-native'
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions'
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Colors, Images, Fonts } from '../../styles/Index';
import { LocalizationContext } from '../../services/localization/LocalizationContext';
import Entypo from 'react-native-vector-icons/Entypo'
import { HardCodeData } from '../../hardcodedata/LanguageList'
import { ApiCall } from '../comman/ApiCall'
import CustomRadioButton from './CustomRadioButton';
import { ClickedButtonName } from '../comman/ClickedButtonName';
import Loader from "../comman/Loader";

import { FirebaseConfig } from '../../component/comman/FirebaseConfig'
import Constants from "../comman/Constant";

const Language = (props) => {
    const { appLanguage, initializeAppLanguage, setAppLanguage, translations } = useContext(LocalizationContext);
    const [languageList, setLanguageList] = useState([]);
    const [user, setUser] = useState(JSON.parse(Constants.LogIn_User));
    const [isLoading, setIsLoading] = useState(false);
    useEffect(() => {
        //  setLanguageList(HardCodeData.LanguageList);
        if (languageList.length == 0) {
            getAllLanguage();
        }
    }, [])
    const changeLanguage = (language) => {
        setAppLanguage(language)
    }

    const getAllLanguage = () => {
        var requestData = {};
        setIsLoading(true);
        ApiCall.Api_Call('/language/getAlllanguage', '', 'POST', true).then((result) => {
            setIsLoading(false)
            console.log(result);
            if (result.status == 1) {
                if (result.result.length > 0) {
                    setLanguageList(result.result);
                }
            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });
    }


    const saveUserLanguage = (language, index) => {
        var requestData = {};
        requestData.language = language.languageCode;//.torString();  //language
        requestData.userId = user.user.userId;
        // setIsLoading(true);
        console.log(requestData);
        ApiCall.Api_Call('/language/saveUserlanguage', requestData, 'POST', true).then((result) => {
            //  setIsLoading(false)
            console.log(result);
            if (result.status == 1) {
                changeLanguage(language.languageCode) //language.languageCode
                //  getSelectedLanguage(user)
            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {
            //  setIsLoading(false)
            console.log(error);
        });
    }
    return (
        <SafeAreaView>
            <Loader loading={isLoading} />
            {languageList.map((item, index) => (
                <View key={index} style={[styles.listHeader, { borderBottomWidth: languageList.length - 1 == index ? responsiveWidth(0) : responsiveWidth(0.1) }]}>
                    <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => {
                        FirebaseConfig.realtimeClickedButton(item.language);
                        //  changeLanguage(item.languageCode)
                        saveUserLanguage(item, index)
                    }}>
                        <View style={[styles.listHeaderView,]}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <CustomRadioButton key={index}
                                    disableRadio={false}
                                    color={Colors.appColor}
                                    selected={appLanguage == item.languageCode ? true : false}
                                    onClick={() => { saveUserLanguage(item, index) }} />
                            </View>
                            <View style={{ width: responsiveWidth(90) }}>
                                <Text style={styles.listHeaderTextSites}>{item.language}</Text>
                            </View>

                        </View>
                    </TouchableOpacity>
                </View>
            ))}
        </SafeAreaView>
    )


}


export default Language;
const styles = StyleSheet.create({
    listHeader: {
        alignContent: 'center', justifyContent: 'center',
        paddingHorizontal: responsiveWidth(1),
    },
    listHeaderView: {
        alignItems: 'center',
        paddingVertical: responsiveWidth(1),
        flexDirection: 'row'
    },
    listHeaderTextSites: {
        opacity: 0.7,
        fontSize: responsiveFontSize(1.4),
        fontFamily: Fonts.Roboto.Roboto_Medium,
    },

})

