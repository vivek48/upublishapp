import React, { Component, useContext, useEffect, useState } from 'react'
import { View, Text, TextInput, SafeAreaView, TouchableOpacity, Image, StyleSheet, TouchableHighlight } from 'react-native'
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions'
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Colors, Images, Fonts } from '../../styles/Index';
import Entypo from 'react-native-vector-icons/Entypo'
import { ApiCall } from '../comman/ApiCall'
import { CartItemsContextProvider, CartItemsContext } from '../../services/cartitems/CartItemsContext'

import { BuyingBookContextProvider, BuyingBookContext } from '../../services/buyingbook/BuyingBookContext'
import { ClickedButtonName } from '../comman/ClickedButtonName';
import InitialNameIcon from "./InitialNameIcon";
import { LocalizationContext } from '../../services/localization/LocalizationContext';
import { FirebaseConfig } from '../comman/FirebaseConfig';
import useForceUpdate from 'use-force-update';
import Constants from "../comman/Constant";
const SearchRender = ({ searchTextValue, searchBook, props, cartInBookCount,placeHolder }) => {
    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);
    const [userProfile, setUserProfile] = useState('');
    const { userCartgBook, setUserCartItems } = useContext(CartItemsContext);
    const { setUserBuyingBook, userBuyingBook } = useContext(BuyingBookContext)
    const [cartApiCalling, setCartApiCalling] = useState(true);
    // const [bookInCart, setBookInCart] = useState(0);
    const forceUpdate = useForceUpdate();
    useEffect(() => {
        setUserProfile(JSON.parse(Constants.LogIn_User).user.GooglePhotoURL);
        if (cartApiCalling) {
            setCartApiCalling(false)
            getCartData();

        }
        forceUpdate()
    }, [userProfile])

    const getCartData = () => {
        ApiCall.Api_Call('/cart/fetch', '', 'POST', true).then((result) => {
            // console.log(result);
            if (result.status == 1) {

                setUserCartItems(result.result.cart.items)
                forceUpdate();

            } else if (result.status == 0) {
                setUserCartItems([])

            } else {
                setUserCartItems([])
            }
        }).catch((error) => {
            setUserCartItems([])
            console.log(error);
        });
    }
    const goToCartScreen = () => {
        ApiCall.Api_Call('/cart/detailed', '', 'POST', true).then((result) => {
            //  console.log(result);
            if (result.status == 1) {
                setUserBuyingBook(result.result.cart.items)
                props.navigation.navigate('Address', { routeName: "Books" })
            } else if (result.status == 0) {
                setUserBuyingBook([])

            } else {
                setUserBuyingBook([])
            }
        }).catch((error) => {
            setUserBuyingBook([])
            console.log(error);
        });
    }
    return (
        <SafeAreaView>
            <View style={styles.searchOuterView}>
                <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => {
                    FirebaseConfig.realtimeClickedButton(ClickedButtonName.Orders);
                    props.navigation.navigate(translations.Orders)
                }}>
                    <View>
                        <MaterialIcon size={responsiveWidth(5)} name={'menu'}></MaterialIcon>
                    </View>
                </TouchableOpacity>
                <View>
                    <TextInput
                        name="searchText"
                        value={searchTextValue}
                        placeholder={placeHolder}
                        style={{
                            flex: 1,
                            //  backgroundColor: 'red',
                            width: responsiveWidth(50)
                        }}
                        onChangeText={(searchText) => { searchBook(searchText, 'APINOTCALL') }}
                        // onEndEditing={(searchText) => searchBook(searchText, "APICALL")}
                        onSubmitEditing={() => { searchBook(searchTextValue, "APICALL"); }}

                    ></TextInput>
                </View>
                {searchTextValue.length > 0 && searchTextValue.trim().length > 0 ?
                    <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { searchBook('', "APICALL"); }}>
                        <View style={{ marginRight: responsiveWidth(2) }}>
                            <Entypo size={responsiveWidth(6)} name={'cross'}></Entypo>
                        </View>
                    </TouchableOpacity>
                    : <View />
                }

                <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => {
                    FirebaseConfig.realtimeClickedButton(ClickedButtonName.Cart);
                    if (userCartgBook.length > 0) { goToCartScreen() }
                }}>
                    <View style={{ marginRight: responsiveWidth(2) }}>
                        <View>
                            <Image source={Images.shoppingcart} tintColor={'grey'} style={{ width: responsiveWidth(6), height: responsiveWidth(6) }} ></Image>
                        </View>
                        <View style={{ position: 'absolute', top: responsiveWidth(-3), bottom: responsiveWidth(34), left: responsiveWidth(3) }}>
                            <Text style={{ color: 'grey', fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(1.6) }}>{cartInBookCount > 0 ? cartInBookCount : ''}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
                {userProfile !== '' && userProfile !== undefined && userProfile !== null ?
                    <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => {
                        FirebaseConfig.realtimeClickedButton(ClickedButtonName.Settings);
                        props.navigation.navigate('Setting')
                    }}>
                        <View>
                            <Image source={{ uri: userProfile }} style={{ borderRadius: responsiveWidth(4), width: responsiveWidth(8), height: responsiveWidth(8) }} ></Image>
                        </View>
                    </TouchableOpacity>
                    :
                    <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => {
                        FirebaseConfig.realtimeClickedButton(ClickedButtonName.Settings);
                        props.navigation.navigate('Setting')
                    }}>
                        <View>
                            <InitialNameIcon IconName={(JSON.parse(Constants.LogIn_User).user.userName).substring(0, 1)}>
                            </InitialNameIcon>
                        </View>
                    </TouchableOpacity>
                }
            </View>
        </SafeAreaView>
    )
}


export default SearchRender;
const styles = StyleSheet.create({
    searchOuterView: {
        marginHorizontal: responsiveWidth(4), marginTop: responsiveWidth(1),
        height: responsiveHeight(7),
        opacity: 0.8,
        borderRadius: responsiveWidth(2),
        backgroundColor: '#ffff',// '#d9d9d9',
        shadowColor: '#30C1DD',
        shadowRadius: 10,
        shadowOpacity: 0.6,
        elevation: 8,
        shadowOffset: { width: 0, height: 4 },
        justifyContent: 'space-between', paddingHorizontal: responsiveWidth(4),
        flexDirection: 'row',
        // alignContent: 'center',
        alignItems: 'center'
    }

})

