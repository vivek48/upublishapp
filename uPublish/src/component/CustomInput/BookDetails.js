import React, { Component, useState, useContext, useEffect } from 'react';
import { View, Text, Image, StyleSheet, SafeAreaView, ScrollView, TouchableOpacity, FlatList, ToastAndroid } from 'react-native';
import { responsiveWidth, responsiveFontSize, responsiveHeight } from 'react-native-responsive-dimensions';
import { Images, Fonts, Colors } from '../../styles/Index'
import { ApiCall, } from '../comman/ApiCall'
import { SellrBoxAPICall, } from '../comman/SellrBoxAPICall'
import Loader from '../comman/Loader'
import { CommanFunction } from "../comman/CommanFunction";
import { LocalizationContext } from "../../services/localization/LocalizationContext";
import { UserAddressContext } from "../../services/address/UserAddressContext";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import RazorpayCheckout from 'react-native-razorpay';
import Share from 'react-native-share'
import ImgToBase64 from 'react-native-image-base64';
import { BuyingBookContext } from '../../services/buyingbook/BuyingBookContext'

import { CartItemsContext } from '../../services/cartitems/CartItemsContext'
import Icon from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons'
import Constants from "../comman/Constant";
import useForceUpdate from 'use-force-update';
import { ScreenClass } from '../../component/comman/ScreenClass'
import { ScreenName } from '../../component/comman/ScreenName';
import { FirebaseConfig } from '../../component/comman/FirebaseConfig'
import { ClickedButtonName } from '../../component/comman/ClickedButtonName';
import { HardCodeData } from '../../hardcodedata/LanguageList'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import ReviewComponent from './ReviewComponent';

import { CurrencyContext } from '../../services/currency/CurrencyContext'
import getSymbolFromCurrency from 'currency-symbol-map'
const BookDetails = (props) => {
    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);
    const { setUserAddress, userAddress } = useContext(UserAddressContext);
    const [isLoading, setIsLoading] = useState(false);

    const { setUserBuyingBook, userBuyingBook } = useContext(BuyingBookContext);
    const { setUserCartItems, userCartgBook } = useContext(CartItemsContext);
    const { setUserCurreny, setUserCurrenyValue, userCurrency, userCurrencyValue } = useContext(CurrencyContext);
    const [book, setBook] = useState(props.route.params.bookItem)
    const [sellBoxAPiCall, setSellBoxAPiCall] = useState(false);
    const [sellerBoxData, setSellerBoxData] = useState([]);

    const [reviewsData, setReviewsData] = useState([]);
    const [avgRating, setAvgRating] = useState(0);
    const [ratingArr, setRatingArr] = useState([]);
    const [ratingCount, setRatingCount] = useState(0);
    const [reviewsDataLength, setReviewsDataLength] = useState(0);
    const [refreshData, setRefreshData] = useState('');
    const [selectedBookType, setSelectedBookType] = useState("PrintBook")
    const forceUpdate = useForceUpdate();
    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', () => {
            setBook(props.route.params.bookItem);
            fetchBookAllReview(book._source.isbn);
            setSelectedBookType(book._source.printeligible ? "PrintBook" : "eBook");
            FirebaseConfig.realtimeScreenLogContent(ScreenClass.BookDetail, ScreenName.BookDetail);
        });
        return unsubscribe
    }, [book])



    const shareBooks = async (ISBN) => {
        FirebaseConfig.realtimeClickedButton(ISBN + "_" + ClickedButtonName.Book_Item_Share);
        setIsLoading(true)
        var imageBase64Arr = [];
        await ImgToBase64.getBase64String(book._source.thumbnail)
            .then(base64String => {
                var image64 = `data:image/jpeg;base64,` + base64String;
                imageBase64Arr.push(image64);

            })
            .catch(err => console.log(err));

        var shareOptions;
        if (imageBase64Arr.length > 0) {

            shareOptions = {
                title: 'Repro Books',
                message: 'Repro Books - Books on Demand. Anytime. Anywhere \n\n'
                    + 'Buy ' + '*' + book._source.skutitle.trim() + '*' + '  @ ' + ' '
                    + '*' + book._source.currency + '*' + ' ' + '*' + book._source.sp + '*' + '\n\n\n'
                    + 'Shopping link ' + 'https://shop.reprobooks.in/book/shop/' + ISBN,                // `${<html><h1>hello</h1></html>}`,
                subject: 'Repro Books',
                urls: imageBase64Arr,

            };
        } else {
            shareOptions = {
                title: 'Repro Books',
                message: 'Repro Books - Books on Demand. Anytime. Anywhere \n\n'

                    + 'Buy ' + book._source.skutitle.trim() + '  @ ' + '*Price*' + ' '
                    + book._source.currency + ' ' + book._source.sp + '\n\n\n'
                    + 'Shopping link ' + 'https://shop.reprobooks.in/book/shop/' + ISBN,
                subject: 'Repro Books',
            };
        }

        try {
            // setIsLoading(false)
            const data = Share.open(shareOptions).then(res => { console.log("resss", res); setIsLoading(false) }).catch(err => {
                console.log("err", err)
                setIsLoading(false)
            });
        } catch (err) {
            setIsLoading(false)
            // Toast.show(err, Toast.LONG,)
            console.log(err)
        }
    }


    const getDetailsByISBN = (ISBN) => {
        var userData = {};
        console.log("ISBN--", ISBN);
        userData.seller = [],
            userData.channel = [],
            userData.sellBox = [],
            userData.keyword = ISBN,
            userData.From = '',
            userData.To = '',
            // userData.startFrom = ''
            setIsLoading(true);
        SellrBoxAPICall.Api_Call('/seller-box/advancedSearch', userData, 'POST', true).then((result) => {
            setIsLoading(false)
            // console.log("Seller Box API Call ----", result.result.esdata);
            if (result.status == 1) {
                if (result.result.esdata.length > 0) {
                    // setOpenModalValue(true);
                    // props.navigation.navigate('SellBoxItem', { sellerBoxItem: result.result.esdata })
                    setSellerBoxData(result.result.esdata);
                    setSellBoxAPiCall(true)
                }

            } else if (result.status == 0) {

                alert(result.message)
            } else {
            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });
    }
    const gotoShopButton = (ProductURL) => {
        // console.log(ProductURL);
        if (ProductURL !== '') {
            props.navigation.navigate('GoToShop', { ProductURL: ProductURL })
        } else {
            alert("Product Url Empty :" + ProductURL);
        }
    }





    const DownloadNewsPostAPi = (checkBoxIDs) => {
        // console.log("New List", checkBoxIDs);
        var UpIDS = [];
        for (var i = 0; i < checkBoxIDs.length; i++) {
            UpIDS.push(checkBoxIDs[i]._source.upID)
        }
        // setIsLoading(true);
        var userData = {};
        // { "SelectedRecords": ["60177d5fea06b75d47ed72d8", "60177d5eea06b75d47ed72d6", "6005a6b54e03420d5234e5f6", "6005beb94e03420d5235139b", "6006dcd74e03420d52370960", "6006d04e4e03420d5236f85e"], "Type": "SIMPLE" }
        // // userData.SelectedRecords =    ["602526d11b47ee2f689fff8b"];
        userData.SelectedRecords = UpIDS;
        // // userData.seller = [];
        // // userData.channel = [];
        // // userData.sellBox = [];
        // // userData.Media = ['Print'];
        userData.Type = "ADVANCED"; //"SIMPLE";
        console.log(" var userData ", userData);
        SellrBoxAPICall.Api_Call('/dataSearch/export/PDF', userData, 'POST', true).then((result) => {
            setIsLoading(false)
            console.log("Seller Box API Call ----", result);
            if (result.status == 1) {
                convertPdfWurlBase64(result.result.filePath);

            } else if (result.status == 0) {

            } else {

            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });
    }
    const convertPdfWurlBase64 = async (url) => {
        await CommonFunctions.getBase64(url).then((result) => {
            ShareProducts(result);
        }).catch((err) => {

        })
    }
    const ShareProducts = async (pdfUrl) => {
        const whatMessage = "Sell Box Analysis " + '\n\n'
            + '*Get Inside the Box*🛒 \n\n'
            + 'Increase your online sales by getting inside ' + '*Sell Box*' + '\n\n'
            + '' + 'Visit@ ' + 'https://sellbox.in' + '\n\n';
        const shareOptions = {
            title: 'SellBox',
            message: whatMessage,// :  type == 'WhatsApp' ?  otherSharing,
            subject: 'SellBox Products',
            urls: ['data:application/pdf;base64,' + pdfUrl],
            // social: type == 'EMAIL' ? Share.Social.EMAIL : Share.Social.WHATSAPP,
        };
        try {
            const data = Share.open(shareOptions).then(res => console.log(res)).catch(err => console.log(err));
        } catch (err) {
            console.log(err);
        }
    }
    const addCartData = (ItemID, paperbackQty, eBookquantity, type) => {

         console.log(paperbackQty, eBookquantity, ItemID);
        var userData = {};
        userData.bookID = ItemID
        setIsLoading(true);
        //  userData.quantity = quantity
        userData.paperbackQty = paperbackQty;
        userData.eBookQty = eBookquantity;
        ApiCall.Api_Call('/cart/add', userData, 'POST', true).then((result) => {
            // console.log(result);
             console.log(result);
            setIsLoading(false)
            if (result.status == 1) {
                var data = book;
                data.eBookQty = eBookquantity;
                data.paperbackQty = paperbackQty;
                setBook(data);
                forceUpdate();
                setUserCartItems(result.result.items);
                if (type == 'Buy') {
                    FirebaseConfig.realtimeClickedButton(ItemID + "_" + ClickedButtonName.Buy_Now_Button);
                    goToCartScreen();
                } else {
                    FirebaseConfig.realtimeClickedButton(ItemID + "_" + ClickedButtonName.Add_To_Cart);
                }
            } else if (result.status == 0) {
                setIsLoading(false)
            } else {
                setIsLoading(false)
            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });
    }
    const goToCartScreen = () => {

        FirebaseConfig.realtimeClickedButton(ClickedButtonName.Cart);
        setIsLoading(true)
        ApiCall.Api_Call('/cart/detailed', '', 'POST', true).then((result) => {
            if (result.status == 1) {
                setIsLoading(false)

                setUserBuyingBook(result.result.cart.items);
                props.navigation.pop();
                props.navigation.navigate('Address', { routeName: "Books" })


            } else if (result.status == 0) {
                setUserBuyingBook([])

                setIsLoading(false)

            } else {

                setUserBuyingBook([])
                setIsLoading(false)
            }
        }).catch((error) => {

            setUserBuyingBook([])
            setIsLoading(false)
            console.log(error);
        });
    }

    const fetchBookAllReview = (isbn) => {
        var userData = {};
        userData.isbn = isbn;
        userData.startFrom = 0;
        setIsLoading(true);
        ApiCall.Api_Call('/review/fetchBookAllReview', userData, 'POST', true).then((result) => {
            console.log(result);
            console.log(result.result.totalCount);
            setRefreshData(3636);
            setBook(props.route.params.bookItem)
            if (result.status == 1) {
                setIsLoading(false)
                setRatingCount(result.result.totalCount);
                setReviewsDataLength(result.result.Records.length)
                setRatingArr(result.result.ratingArr.reverse());
                setAvgRating(result.result.avg_rating);
                setReviewsData(CommanFunction.storeFiveReviewsOnlyForBookDetails(result.result.Records));
                console.log("reviewsData", reviewsData);

            } else if (result.status == 0) {
                setReviewsData([]);
                setIsLoading(false)
            } else {
                setReviewsData([]);
                setIsLoading(false)
            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });
    }


    const checkItemPurchase = (isbn) => {
        var userData = {};
        userData.isbn = isbn
        setIsLoading(true);
        ApiCall.Api_Call('/review/isItemPurchased', userData, 'POST', true).then((result) => {
            console.log(result);
            if (result.status == 1) {
                // setIsLoading(false)
                isUserCanWriteAReview(isbn);
                // props.navigation.navigate("ReviewProduct", { bookItem: book })
            } else if (result.status == 0) {
                ToastAndroid.show("Purchase a book and write a review", ToastAndroid.LONG);
                setIsLoading(false)
            } else {
                setIsLoading(false)
            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });
    }


    const isUserCanWriteAReview = (isbn) => {
        var userData = {};
        userData.isbn = isbn;
        userData.startFrom = 0,

            setIsLoading(true);
        ApiCall.Api_Call('/review/isUserCanWriteAReview', userData, 'POST', true).then((result) => {
            console.log(result);
            if (result.status == 1) {
                setIsLoading(false)
                if (result.result.isUserCanWriteAReview) {
                    props.navigation.navigate("ReviewProduct", { bookItem: book })
                } else {
                    ToastAndroid.show("book review's already available ", ToastAndroid.LONG);
                }
            } else if (result.status == 0) {

                setIsLoading(false)
            } else {
                setIsLoading(false)
            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });
    }


    //    / console.log(book.eBookQty, "book");

    return (
        // 

        <View style={{ flex: 1 }}>

            <Loader loading={isLoading} />
            <ScrollView>

                <View style={[styles.bookheaderView,]}>
                    <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { Constants.LinkingURL = false; props.navigation.navigate('Books') }}>
                        <View>
                            <Text style={{ fontFamily: Fonts.Roboto.Roboto_Medium, color: 'black', fontSize: responsiveFontSize(2) }}>{translations.BackHeaderButton}</Text>

                            {/* <MaterialIcons style={{ opacity: Colors.activeOpecity }} size={responsiveWidth(6)} name={'arrow-back'}></MaterialIcons> */}
                        </View>
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                        <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { if (userCartgBook.length > 0) { goToCartScreen() } }}>
                            <View style={{ marginRight: responsiveWidth(2), }}>
                                <View>
                                    <Image source={Images.shoppingcart} tintColor={'grey'} style={{ width: responsiveWidth(6), height: responsiveWidth(6) }} ></Image>
                                </View>
                                <View style={{ position: 'absolute', top: responsiveWidth(-2.5), left: responsiveWidth(3) }}>
                                    <Text style={{ color: 'grey', fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(1.6) }}>{userCartgBook.length > 0 ? userCartgBook.length : ''}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { props.navigation.navigate('Setting') }}>
                            <View style={{ paddingLeft: responsiveWidth(6) }}>
                                <Image source={Images.setting} style={{ width: responsiveWidth(8), height: responsiveWidth(8) }}></Image>
                                {/* <Entypo style={{ opacity: Colors.activeOpecity }} size={responsiveWidth(6)} name={'dots-three-vertical'}></Entypo> */}
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.bookImageView}>
                    <View>
                         <Image source={CommanFunction.checkImageAvailability(book._source.thumbnail, book._source.isbn)}
                            style={styles.bookImage}></Image>
                    </View>
                    <View style={styles.bookTextView}>
                        <View>
                            <Text style={styles.bookText}>{book._source.skutitle}</Text>
                        </View>
                        <View>
                            <Text style={[styles.bookAuthor, {}]}>Author Name : <Text style={styles.headerTextStyle}> {book._source.authorname} </Text></Text>
                        </View>
                        <View>
                            <Text style={styles.bookAuthor}>Publisher Name : <Text style={styles.headerTextStyle}> {book._source.publishername}
                            </Text>

                            </Text>
                        </View>
                        <View>
                            <Text style={styles.bookAuthor}>Language : <Text style={styles.headerTextStyle}>{book._source.publishedlanguage}</Text> </Text>
                        </View>
                    </View>

                </View>
                <View style={styles.reviewHeader}>

                    <View style={styles.eBookPrint} >
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <View style={{ paddingRight: responsiveWidth(1) }}>
                                <Text style={[{ fontSize: responsiveFontSize(1.4) }]}>{avgRating}</Text>
                            </View>
                            <View>
                                <MaterialIcons size={responsiveWidth(4)} name={'star-border'} color={'yellow'}></MaterialIcons>
                            </View>
                        </View>
                        <View style={{ paddingRight: responsiveWidth(4) }}>
                            <Text style={{ fontSize: responsiveFontSize(1) }}>{ratingCount} reviews</Text>
                        </View>
                    </View>
                    <View style={styles.eBookPrint} >
                        <Text style={[{ fontSize: responsiveFontSize(1.4) }]}>{book._source.pagecount}</Text>
                        <Text style={{ fontSize: responsiveFontSize(1) }}>Pages</Text>
                    </View>
                    {/* {book._source.ebookeligible && book._source.ebookeligible !== undefined &&
                        <TouchableOpacity onPress={() => { setSelectedBookType("eBook") }}>
                           
                            <View style={[styles.eBookPrint, { backgroundColor: selectedBookType == "eBook" ? Colors.appColor : '#f4edf5' }]}>
                                <Text style={[{
                                    fontSize: responsiveFontSize(1.0), textAlign: 'justify', color: selectedBookType ==
                                        "eBook" ? "#ffff" : 'black'
                                }]}>eBook</Text>
                                <Text style={{
                                    fontSize: responsiveFontSize(1.4), textAlign: 'justify', color: selectedBookType ==
                                        "eBook" ? "#ffff" : 'black'
                                }}>

                                    {book._source.currency !== userCurrency && userCurrency !== '' && userCurrency && userCurrency !== null ?
                                        userCurrency : book._source.currency}{book._source.currency !== userCurrency && userCurrency !== '' && userCurrency &&
                                            userCurrency !== null ?
                                            " " + getSymbolFromCurrency(userCurrency) + " " + CommanFunction.currencyChangeValue(book._source.ebooksp, userCurrencyValue) :   " " + getSymbolFromCurrency(userCurrency) + " " + book._source.ebooksp}
                                  

                                </Text>
                            </View>
                        </TouchableOpacity>
                    } */}
                    {book._source.printeligible && book._source.printeligible !== undefined &&
                        <TouchableOpacity onPress={() => { setSelectedBookType("PrintBook") }}>
                            <View style={[styles.eBookPrint, {
                                backgroundColor: selectedBookType == "PrintBook" ? Colors.appColor : '#f4edf5'
                            }]} >
                                <Text style={[{
                                    fontSize: responsiveFontSize(1.0), textAlign: 'justify', color:
                                        selectedBookType == "PrintBook" ? "#ffff" : 'black'
                                }]}>Print book</Text>
                                <Text style={{
                                    fontSize: responsiveFontSize(1.4), textAlign: 'justify',
                                    color: selectedBookType == "PrintBook" ? "#ffff" : 'black'
                                }}>
                                    {book._source.currency !== userCurrency && userCurrency !== '' && userCurrency && userCurrency !== null ?
                                        userCurrency : book._source.currency}{book._source.currency !== userCurrency && userCurrency !== '' && userCurrency &&
                                            userCurrency !== null ?
                                            " " + getSymbolFromCurrency(userCurrency) + " " + CommanFunction.currencyChangeValue(book._source.sp, userCurrencyValue) :  " " + getSymbolFromCurrency(userCurrency) + " " + book._source.sp}
                                    {/* Rs. {" " + book._source.sp} */}
                                </Text>
                            </View>
                        </TouchableOpacity>
                    }

                    {/* backgroundColor: '#6a2c70' */}

                </View>
                <View style={styles.buttonView}>
                    {/* getBuyBook(book._source) */}

                    {selectedBookType == "PrintBook" &&
                        <View>

                            {book.paperbackQty == undefined || book.paperbackQty == 0 ?
                                <TouchableOpacity activeOpacity={Colors.activeOpecity}
                                    onPress={() => {
                                        addCartData(book._id, book.paperbackQty + 1, book.eBookQty, "AddToCart")
                                    }}>
                                    <View style={[styles.buttonStyle, { backgroundColor: '#ffc107', }]}>
                                        <Text style={{
                                            color: '#ffff', fontSize: responsiveFontSize(1.4),
                                            fontFamily: Fonts.Roboto.Roboto_Medium
                                        }} >Add to Cart</Text>
                                    </View>
                                </TouchableOpacity>
                                :
                                <View style={{ flexDirection: 'row' }}>
                                    <TouchableOpacity activeOpacity={Colors.activeOpecity}
                                        onPress={() => { addCartData(book._id, book.paperbackQty - 1, book.eBookQty, "Decrease") }}>
                                        <View style={[{ justifyContent: 'center', alignItems: 'center', width: responsiveWidth(12.5), backgroundColor: '#ffc107', }]}>
                                            <Text style={{
                                                color: '#ffff', fontSize: responsiveFontSize(3),
                                                fontFamily: Fonts.Roboto.Roboto_Medium
                                            }} >-</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <View style={[{ justifyContent: 'center', alignItems: 'center', width: responsiveWidth(15), backgroundColor: 'lightgrey', }]}>
                                        <Text style={{
                                            color: 'grey', fontSize: responsiveFontSize(1.8),
                                            fontFamily: Fonts.Roboto.Roboto_Bold
                                        }} >{book.paperbackQty}</Text>
                                    </View>
                                    <TouchableOpacity activeOpacity={Colors.activeOpecity}
                                        onPress={() => { addCartData(book._id, book.paperbackQty + 1, book.eBookQty, "Increase") }}>
                                        <View style={[{ justifyContent: 'center', alignItems: 'center', width: responsiveWidth(12.5), backgroundColor: '#ffc107', }]}>
                                            <Text style={{
                                                color: '#ffff', fontSize: responsiveFontSize(3),
                                                fontFamily: Fonts.Roboto.Roboto_Medium,

                                            }} >+</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            }
                        </View>
                    }





                    {selectedBookType == "eBook" &&
                        <View>
                            {book.eBookQty == undefined || book.eBookQty == 0 ?
                                <TouchableOpacity activeOpacity={Colors.activeOpecity}
                                    onPress={() => {
                                        addCartData(book._id, book.paperbackQty, book.eBookQty + 1, "AddToCart")
                                    }}>
                                    <View style={[styles.buttonStyle, { backgroundColor: '#ffc107', }]}>
                                        <Text style={{
                                            color: '#ffff', fontSize: responsiveFontSize(1.4),
                                            fontFamily: Fonts.Roboto.Roboto_Medium
                                        }} >Add to Cart</Text>
                                    </View>
                                </TouchableOpacity>
                                :
                                <View style={{ flexDirection: 'row' }}>
                                    <TouchableOpacity activeOpacity={Colors.activeOpecity}
                                        onPress={() => { addCartData(book._id, book.paperbackQty, book.eBookQty - 1, "Decrease") }}>
                                        <View style={[{ justifyContent: 'center', alignItems: 'center', width: responsiveWidth(12.5), backgroundColor: '#ffc107', }]}>
                                            <Text style={{
                                                color: '#ffff', fontSize: responsiveFontSize(3),
                                                fontFamily: Fonts.Roboto.Roboto_Medium
                                            }} >-</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <View style={[{ justifyContent: 'center', alignItems: 'center', width: responsiveWidth(15), backgroundColor: 'lightgrey', }]}>
                                        <Text style={{
                                            color: 'grey', fontSize: responsiveFontSize(1.8),
                                            fontFamily: Fonts.Roboto.Roboto_Bold
                                        }} >{book.eBookQty}</Text>
                                    </View>
                                    <TouchableOpacity activeOpacity={Colors.activeOpecity}
                                        onPress={() => { addCartData(book._id, book.paperbackQty, book.eBookQty + 1, "Increase") }}>
                                        <View style={[{ justifyContent: 'center', alignItems: 'center', width: responsiveWidth(12.5), backgroundColor: '#ffc107', }]}>
                                            <Text style={{
                                                color: '#ffff', fontSize: responsiveFontSize(3),
                                                fontFamily: Fonts.Roboto.Roboto_Medium,

                                            }} >+</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            }
                        </View>
                    }
                    <TouchableOpacity activeOpacity={Colors.activeOpecity}
                        onPress={() => {
                            if (selectedBookType == "PrintBook") {

                                addCartData(book._id, book.paperbackQty + 1, book.eBookQty, "Buy")
                            } else {
                                addCartData(book._id, book.paperbackQty, book.eBookQty + 1, "Buy")
                            }
                        }}>
                        {selectedBookType == "PrintBook" &&
                            <View style={[styles.buttonStyle, { backgroundColor: '#ffc107', }]}>

                                <Text style={{
                                    color: '#ffff', fontSize: responsiveFontSize(1.4),
                                    fontFamily: Fonts.Roboto.Roboto_Medium
                                }} >


                                    Buy Now {book._source.currency !== userCurrency && userCurrency !== '' && userCurrency && userCurrency !== null ?
                                        userCurrency : book._source.currency}{book._source.currency !== userCurrency && userCurrency !== '' && userCurrency &&
                                            userCurrency !== null ?
                                            " " + getSymbolFromCurrency(userCurrency) + " " + CommanFunction.currencyChangeValue(book._source.sp, userCurrencyValue) :   " " + getSymbolFromCurrency(userCurrency) + " " + book._source.sp}

                                    {/* Buy Now   Rs. {book._source.sp} */}


                                </Text>
                            </View>
                        }

                        {selectedBookType == "eBook" &&
                            <View style={[styles.buttonStyle, { backgroundColor: '#ffc107', }]}>
                                {selectedBookType == "eBook" &&

                                    <Text style={{
                                        color: '#ffff', fontSize: responsiveFontSize(1.4),
                                        fontFamily: Fonts.Roboto.Roboto_Medium
                                    }} >
                                        Buy Now {book._source.currency !== userCurrency && userCurrency !== '' && userCurrency && userCurrency !== null ?
                                            userCurrency : book._source.currency}{book._source.currency !== userCurrency && userCurrency !== '' && userCurrency &&
                                                userCurrency !== null ?
                                                " " + getSymbolFromCurrency(userCurrency) + " " + CommanFunction.currencyChangeValue(book._source.ebooksp, userCurrencyValue) :   " " + getSymbolFromCurrency(userCurrency) + " " + book._source.ebooksp}

                                        {/* Buy Now   Rs. {book._source.ebooksp !== undefined ? book._source.ebooksp : "0"} */}


                                    </Text>
                                }

                            </View>
                        }
                    </TouchableOpacity>
                </View>




















                <View style={[styles.buttonView, { marginTop: responsiveWidth(1), }]}>
                    <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => {
                        if (sellBoxAPiCall) {
                            setSellerBoxData([]);
                            setSellBoxAPiCall(false)
                        } else {
                            // getDetailsByISBN('9789390486953')
                            getDetailsByISBN(book._source.isbn)
                        }
                    }}>
                        {/* width: responsiveWidth(12), */}
                        <View style={[styles.buttonStyle, { backgroundColor: Colors.appColor }]}>
                            <Ionicons size={18} color={'#ffff'} name={'git-branch'}></Ionicons>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => {
                        shareBooks(book._source.isbn)
                    }}>
                        {/* width: responsiveWidth(20),  */}
                        <View style={[styles.buttonStyle, { backgroundColor: Colors.appColor, flexDirection: 'row', }]}>
                            <View>
                                <Icon name='sharealt' size={17} color={'#ffff'}></Icon>
                            </View>
                            <View style={{ paddingLeft: responsiveWidth(2) }}>
                                <Text style={{
                                    color: '#ffff',
                                    fontFamily: Fonts.Roboto.Roboto_Medium,
                                    fontSize: responsiveFontSize(1.4)
                                }} >Share</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    {/* <TouchableOpacity activeOpacity={Colors.activeOpecity}
                        onPress={() => { getBuyBook(book._source) }}>
                        <View style={[styles.buttonStyle, { backgroundColor: '#ffc107', }]}>
                            <Text style={{
                                color: '#ffff', fontSize: responsiveFontSize(1.4),
                                fontFamily: Fonts.Roboto.Roboto_Medium
                            }} >Buy Now   {book._source.currency}{" " + book._source.sp}</Text>
                        </View>
                    </TouchableOpacity> */}
                </View>






























                {book._source.sp !== book._source.rrp &&
                    < View style={{
                        justifyContent: 'flex-end',
                        alignItems: 'flex-end',
                        paddingHorizontal: responsiveWidth(10),
                        backgroundColor: '#fff',
                        paddingBottom: responsiveWidth(2),
                    }}>
                        <Text style={{
                            fontSize: responsiveFontSize(1.2),
                            fontFamily: Fonts.Awesome.FontAwesome5_Regular, textDecorationLine: 'line-through',
                            textDecorationStyle: 'solid'
                        }}>Was :  {book._source.currency}{" " + book._source.rrp}</Text>
                    </View>
                }
                {sellerBoxData.length > 0 &&
                    <View style={{
                        paddingHorizontal: responsiveWidth(4), marginTop: responsiveWidth(2), backgroundColor: '#fff',
                        height: sellerBoxData.length > 6 ? responsiveWidth(80) : sellerBoxData.length > 3 ? responsiveWidth(60) : responsiveWidth(30)
                    }}>
                        <View style={{ flexDirection: 'row', paddingTop: responsiveWidth(2), alignItems: 'center', justifyContent: 'space-between' }}>
                            <View>
                                <Text style={{ fontSize: responsiveFontSize(1.3), textShadowColor: 'black', textShadowRadius: responsiveWidth(0.5), letterSpacing: responsiveWidth(0.2), lineHeight: responsiveWidth(6) }}>Compare Price from other</Text>
                            </View>
                            <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { DownloadNewsPostAPi(sellerBoxData) }}>
                                <View>
                                    {/* <Icon name='sharealt' size={20} color={'grey'}></Icon> */}
                                </View>
                            </TouchableOpacity>
                        </View>
                        <ScrollView nestedScrollEnabled={true}>
                            <View style={{ marginTop: responsiveWidth(4), flexWrap: 'wrap' }}>
                                {sellerBoxData.map((item, index) => (
                                    // console.log(item),
                                    <View key={index} style={{ flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center', }}>
                                        <View style={{ width: responsiveWidth(42), }}>
                                            <Text style={styles.sellerNameStyle}>{item._source.sellerName}</Text>
                                        </View>
                                        <View style={{ width: responsiveWidth(8), alignItems: 'center', }}>
                                            <Text style={styles.dateStyle}>{CommanFunction.changeDateFormat(item._source.createdOn).split(" ")[0]}</Text>
                                        </View>
                                        <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { gotoShopButton(item._source.productURL) }}>
                                            <View style={{ width: responsiveWidth(42), alignItems: 'flex-end', paddingRight: responsiveWidth(3) }}>
                                                <Text style={[styles.priceStyle,]}>{'     ' + item._source.currency} {' ' + item._source.sp}</Text>
                                            </View>
                                        </TouchableOpacity>
                                        {/* <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { gotoShopButton(item._source.productURL) }}>
                                        <View style={styles.shopButton}>
                                            <Text style={styles.shopButtonText}> Go to Shop</Text>
                                        </View>
                                    </TouchableOpacity> */}
                                    </View>
                                ))}
                            </View>
                        </ScrollView>
                    </View>
                }

                <View style={{
                    paddingHorizontal: responsiveWidth(6), alignItems: 'flex-start',
                    paddingVertical: responsiveWidth(2),
                    backgroundColor: '#fff', marginTop: responsiveWidth(2),
                }}>
                    <View>
                        <Text style={[styles.bookText, { fontSize: responsiveFontSize(1.8) }]}>About this Book</Text>
                    </View>
                    <View style={{ paddingVertical: responsiveWidth(3) }}>
                        <Text style={[styles.reviewText, { fontSize: responsiveFontSize(1.7), lineHeight: 25, }]}>
                            {book._source.longdescription}

                        </Text>
                    </View>
                </View>
                {/* <View style={{
                    flexDirection: 'row', paddingHorizontal: responsiveWidth(4),
                    height: responsiveWidth(15),
                    paddingTop: responsiveWidth(1), backgroundColor: '#ffff', marginTop: responsiveWidth(2),
                    justifyContent: 'space-between', alignItems: 'center'
                }}>
                    <View>
                        <Text style={[styles.bookText, { fontSize: responsiveFontSize(1.7) }]}>Customer reviews </Text>
                    </View>
                    <TouchableOpacity onPress={() => { checkItemPurchase(book._source.isbn) }}>
                        <View style={{
                            backgroundColor: '#ffc107',
                            borderRadius: responsiveWidth(0.7)
                        }}>
                            <Text style={[styles.bookText, {
                                color: "#ffff",
                                paddingVertical: responsiveWidth(2.3),
                                paddingHorizontal: responsiveWidth(3),
                                fontSize: responsiveFontSize(1.5),
                            }]}>Write a product review </Text>
                        </View>
                    </TouchableOpacity>
                </View> */}


                {/* {reviewsDataLength !== 0 &&
                    <SafeAreaView style={{ flex: 1 }}>
                        <FlatList
                            data={reviewsData}
                            nestedScrollEnabled
                            onEndReachedThreshold={200}
                            renderItem={({ item, index, }) => (
                                <ReviewComponent index={index} ratingCount={ratingCount} avaragRating={avgRating} ratingArr={ratingArr} item={item} book={book} props={props} isbn={book._source.isbn} />
                            )}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </SafeAreaView>
                }
                {reviewsDataLength > 3 &&
                    <TouchableOpacity onPress={() => { props.navigation.navigate("AllReviewsScreen", { isbn: book._source.isbn, book: book }) }}>
                        <View style={{
                            flexDirection: 'row', borderBottomWidth: responsiveWidth(0.1), alignItems: 'center', justifyContent: 'space-between',
                            paddingHorizontal: responsiveWidth(2), backgroundColor: '#ffff'
                        }}>
                            <View>
                                <Text style={{ textAlign: 'justify', fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(1.5), padding: responsiveWidth(3) }}>All {reviewsDataLength} reviews</Text>
                            </View>
                            <View>
                                <MaterialCommunityIcons name={"chevron-right"} size={responsiveWidth(7)}></MaterialCommunityIcons>
                            </View>
                        </View>
                    </TouchableOpacity>
                } */}

            </ScrollView >
        </View>


    )


}
const styles = StyleSheet.create({
    flex: 1,
    ratingProgressbar: { color: 'black', fontFamily: Fonts.Roboto.Roboto_Black, fontSize: responsiveFontSize(1.4), paddingRight: responsiveWidth(2) },
    ratingnumber: { paddingVertical: responsiveWidth(1), paddingLeft: responsiveWidth(2), fontFamily: Fonts.Roboto.Roboto_Regular, textAlign: 'left', fontSize: responsiveFontSize(1.5) },
    reviewOuterView: {
        flexDirection: 'row', paddingVertical: responsiveWidth(2),
        justifyContent: 'space-between'
    },
    reviewInnerView: {
        // width: responsiveWidth(46),
        justifyContent: 'center',

    },
    bookheaderView: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: responsiveWidth(5),
        paddingRight: responsiveWidth(3),
        justifyContent: 'space-between',
        width: responsiveWidth(100),
        height: responsiveHeight(6.5),
        backgroundColor: '#fff',
    },
    bookImageView: {
        paddingHorizontal: responsiveWidth(6),
        paddingVertical: responsiveWidth(3),
        width: responsiveWidth(100),
        flexDirection: 'row',
        backgroundColor: '#fff',
    },
    bookImage: {
        width: responsiveWidth(30),
        height: responsiveWidth(40),
        borderRadius: responsiveWidth(2)
    },
    bookTextView: {
        width: responsiveWidth(60),
        flexWrap: 'wrap',
        marginLeft: responsiveWidth(3),
        paddingHorizontal: responsiveWidth(5),
    },
    bookText: {
        opacity: 0.8,
        fontFamily: Fonts.Roboto.Roboto_Bold,
        fontSize: responsiveFontSize(2.6)
    },
    bookAuthor: {
        fontFamily: Fonts.Roboto.Roboto_Medium,
        fontSize: responsiveFontSize(1.6),
        color: 'black',
        lineHeight: 22
    },
    reviewHeader: {
        paddingHorizontal: responsiveWidth(4),
        paddingVertical: responsiveWidth(3),
        // justifyContent: 'space-around',
        flexDirection: 'row', alignItems: 'center',
        backgroundColor: '#fff',
    },
    reviewList: {
        width: responsiveWidth(20),
        alignItems: 'center',
        borderRightWidth: responsiveWidth(0.1)
    },
    reviewText: {
        fontFamily: Fonts.Awesome.FontAwesome5_Regular, fontSize: responsiveFontSize(1.2),
        paddingVertical: responsiveWidth(1),
        textAlign: 'justify'

    },
    buttonView: {
        backgroundColor: '#fff',
        paddingHorizontal: responsiveWidth(4),
        flexDirection: 'row',
        justifyContent: 'space-between', alignItems: 'center',
        paddingTop: responsiveWidth(2),
        paddingBottom: responsiveWidth(2)
    },
    buttonStyle: {
        //width: responsiveWidth(50),
        width: responsiveWidth(45),
        justifyContent: 'center', height: responsiveHeight(5),
        alignItems: 'center',
        borderRadius: responsiveWidth(1)
    },

    headerTextStyle: {
        lineHeight: 22,
        fontFamily: Fonts.Roboto.Roboto_Regular,
        fontSize: responsiveFontSize(1.5)
    },
    sellerNameStyle: {
        paddingTop: responsiveWidth(1.5), textShadowRadius: responsiveWidth(0.5),
        fontSize: responsiveFontSize(1), letterSpacing: responsiveWidth(0.2)
    },
    dateStyle: {
        paddingTop: responsiveWidth(1.5), textShadowRadius: responsiveWidth(1),
        fontSize: responsiveFontSize(1), letterSpacing: responsiveWidth(0.2)
    },
    priceStyle: {
        paddingTop: responsiveWidth(1),
        textShadowRadius: responsiveWidth(5), textDecorationLine: 'underline',
        fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(2.3), letterSpacing: responsiveWidth(0.2)
    },
    shopButton: {
        marginLeft: responsiveWidth(5),
        backgroundColor: Colors.appColor,
        alignItems: 'center',
        borderRadius: responsiveWidth(1),
        padding: responsiveWidth(1.5)
    },
    shopButtonText: { color: '#fff', fontFamily: Fonts.Roboto.Roboto_Bold, letterSpacing: responsiveWidth(0.1), fontSize: responsiveFontSize(1.3) },
    reviewImages: {
        borderWidth: responsiveWidth(0.1), borderColor: 'red', width: responsiveWidth(20),
        height: responsiveWidth(23)
    },
    eBookPrint: {
        width: responsiveWidth(22),
        // height: responsiveHeight(5),
        borderWidth: responsiveWidth(0.1), borderRadius: responsiveWidth(1),
        justifyContent: 'center', alignItems: 'center',
        paddingVertical: responsiveWidth(1),
        backgroundColor: '#f4edf5',
        marginRight: responsiveWidth(1.3)
    }

})

export default BookDetails;