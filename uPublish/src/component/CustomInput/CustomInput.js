import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import { Colors, Fonts, Images } from "../../styles/Index";
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions'


const CustomInput = ({ label, inputvalue, OnChangeText, errorField, placeholder, secureText, textType, ...inputProps }) => {
    //console.log(textType);
    return (
        <View style={{ paddingHorizontal: responsiveWidth(6), }}>
            <FloatingLabelInput
                label={label}
                value={inputvalue}
                staticLabel
                hintTextColor={'#aaa'}
                hint={placeholder}
                isPassword={secureText}
                keyboardType={textType}
                containerStyles={{
                    borderWidth: responsiveWidth(0.3),
                    paddingHorizontal: responsiveWidth(1),
                    backgroundColor: '#fff',
                    borderColor: errorField ? 'red' : Colors.appColor,   //'blue',
                    borderRadius: responsiveWidth(3),
                    color: 'red',
                }}
                customLabelStyles={{
                    // colorFocused: 'red',
                    fontSizeFocused: responsiveFontSize(1.6),
                }}
                labelStyles={styles.label}
                inputStyles={styles.input}
                onChangeText={(value) => { OnChangeText(value) }}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    label: {
        backgroundColor: '#fff',
        paddingHorizontal: responsiveWidth(2),
    },
    input: {
        color: Colors.appColor, //'blue',
        paddingHorizontal: responsiveWidth(2),
    }
})




export default CustomInput;