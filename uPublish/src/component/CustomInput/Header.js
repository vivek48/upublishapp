import React, { useContext, useState } from 'react';
import { View, Text, TouchableOpacity, Image, SafeAreaView, StyleSheet } from 'react-native';
import { Fonts, Images, Colors } from '../../styles/Index';
import { useNavigation } from '@react-navigation/native';
import { responsiveFontSize, responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions'
import { ClickedButtonName } from '../comman/ClickedButtonName';

import { FirebaseConfig } from '../../component/comman/FirebaseConfig'
import { LocalizationContext } from "../../services/localization/LocalizationContext";
const Header = ({ headerTitle, navigation, backScreen }) => {
    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);
    const [isLoading, setIsLoading] = useState(false);
    return (
        <SafeAreaView style={styles.safeAreaView}>
            <View style={{ height: responsiveHeight(4), alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row' }}>
                {backScreen !== undefined && backScreen != null && backScreen != '' ?
                    < TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={backScreen}>
                        <View>
                            <Text style={{ fontFamily: Fonts.Roboto.Roboto_Medium, color: '#fff', fontSize: responsiveFontSize(2) }}>{translations.BackHeaderButton}</Text>
                        </View>
                    </TouchableOpacity> :
                    <View></View>
                }
                <View style={{ alignSelf: 'center', justifyContent: 'center', alignContent: 'flex-end' }}>
                    <Text style={{
                        color: '#ffff', fontFamily: Fonts.Roboto.Roboto_Medium, fontSize: responsiveFontSize(2)
                    }}>{headerTitle}</Text>
                </View>
                <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => {
                    FirebaseConfig.realtimeClickedButton(ClickedButtonName.Settings);
                    navigation.navigate('Setting')
                }}
                    style={{ alignSelf: 'center', }}>
                    <View>
                        <Image source={Images.setting} tintColor={'#FFFF'} style={{ width: responsiveWidth(8), height: responsiveWidth(8) }}></Image>
                    </View>
                </TouchableOpacity>
            </View>
        </SafeAreaView >
    )
}

const styles = StyleSheet.create({
    flex: 1,
    safeAreaView: {
        // width: responsiveWidth(100),
        paddingVertical: responsiveWidth(2),
        paddingHorizontal: responsiveWidth(4),
        // paddingBottom: responsiveWidth(2),
        backgroundColor: Colors.appColor   //'#27baba'
    },
})

export default Header