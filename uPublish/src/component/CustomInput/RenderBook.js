import React, { Component, useContext, useState } from 'react';
import { View, Text, Image, StyleSheet, SafeAreaView, ScrollView, TouchableOpacity, FlatList } from 'react-native';
import { responsiveWidth, responsiveFontSize, responsiveHeight } from 'react-native-responsive-dimensions';
import { Images, Fonts, Colors } from '../../styles/Index'
import { ApiCall, } from '../comman/ApiCall'
import Loader from '../comman/Loader'
import { CommanFunction, } from "../comman/CommanFunction";
import { BuyEpubBookContext } from "../../services/buyepubbook/BuyEpubBookContext";
import Constants from '../comman/Constant';
import { ClickedButtonName } from '../comman/ClickedButtonName';

import { FirebaseConfig } from '../../component/comman/FirebaseConfig'
const RenderBook = ({ item, index, navigation, }) => {
    const { buyEpubBookList, setBuyEpubBookList } = useContext(BuyEpubBookContext);
    const [isLoading, setIsLoading] = useState(false);
    const [ePubFile, setEPubFile] = useState('')
    const getEPubBooks = (bookContentID) => {
        // bookContentID = '5ff5934625c50c02fda648a3';//'moby-dick' //
        var url = Constants.EPUBURL + bookContentID + "/";
        FirebaseConfig.realtimeClickedButton(bookContentID + "_" + ClickedButtonName.Library)
        console.log("hello", Constants.BuyEPubBookLIST.includes(url), url)
        if (Constants.BuyEPubBookLIST.includes(url)) {
            // alert("API not call", Constants.BuyEPubBookLIST.includes(url))
            navigation.navigation.navigate('EPubReader',
                { ePubFile: url, navigationSreen: navigation.route.name, epubUrlKey: null });
        } else {
            getBookUrlFromServer(bookContentID);
        }
    }
    const getBookUrlFromServer = (bookContentID) => {
        var userData = {};
        userData.contentId = bookContentID; //
        setIsLoading(true);
        // alert("API call",)
        ApiCall.Api_Call('/getepubbookurl', userData, 'POST', true).then((result) => {
            // getmyepubbook  getmyepubbookurl
            setIsLoading(false)
            console.log(result);
            if (result.status == 1) {
                navigation.navigation.navigate('EPubReader', {
                    ePubFile: result.result.url,
                    navigationSreen: navigation.route.name, epubUrlKey: result.result.key
                });
            } else if (result.status == 0) {
                alert(result.message);

            } else {
            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });
    }
    // console.log(item);
    return (
        <>
            <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { getEPubBooks(item._id) }} >
                <SafeAreaView key={index} style={[styles.safeAreaViewThird,]}>
                    <Loader loading={isLoading} />
                    <View style={styles.bookImageView}>
                        <View style={styles.bookImageOterView}>
                            <Image source={CommanFunction.checkImageAvailability(item.thumbnail)}
                                style={styles.bookImage}></Image>
                        </View>
                        <View style={styles.bookTextView}>
                            <View>
                                <Text style={styles.bookText}>{item.contentMetaDataRef.booktitle}</Text>
                            </View>
                            <View>
                                <Text style={styles.bookAuthor}>{item.contentMetaDataRef.Author}</Text>
                            </View>
                            <View>
                                <Text style={styles.bookAuthor}>{item.contentMetaDataRef.publisherName}</Text>
                            </View>
                        </View>

                    </View>


                    {/* <View style={{ alignSelf: 'center', alignContent: 'center' }} >
                        <Text style={styles.titleStyle}>{item.title} ({item.pages1 == 0 ? item.pages + ' pages' : '0 Pages'})</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>

                        <View style={{ paddingVertical: responsiveWidth(1), paddingTop: responsiveWidth(1) }}>
                            <Image resizeMode={'center'} source={Images.logo} style={{
                                borderColor: 'grey',
                                borderWidth: responsiveWidth(0.1), width: responsiveWidth(22),
                                height: responsiveWidth(30)
                            }}></Image>
                        </View>
                        <View style={{ flexWrap: 'wrap', width: responsiveWidth(73.5), paddingLeft: responsiveWidth(6) }}>
                            <View style={{ paddingTop: responsiveWidth(1) }}>
                                <Text style={styles.authorStyle}>Author Name -
                                  <Text style={styles.authorStyleText}>{item.contentMetaDataRef.Author}</Text>
                                </Text>

                            </View>
                            <View>
                                <Text style={styles.authorStyle}>Cover Type -
                                  <Text style={styles.authorStyleText}>{' ' + item.contentMetaDataRef.coverType}</Text>
                                </Text>

                            </View>
                            <View>
                                <Text style={styles.authorStyle}>Book Size -
                                  <Text style={styles.authorStyleText}>{' ' + item.contentMetaDataRef.booksize}</Text>
                                </Text>
                            </View>
                            <View style={styles.headerViewStyle}>
                                <Text style={styles.authorStyle}>Distribution -
                                  <Text style={styles.authorStyleText}>{' ' + item.contentMetaDataRef.distribution}</Text>
                                </Text>
                            </View>
                            <View style={styles.headerViewStyle}>
                                <Text style={[styles.authorStyle, { color: 'black', fontFamily: Fonts.Roboto.Roboto_Black, fontSize: responsiveFontSize(1.6) }]}>Selling Price -
                                  <Text style={styles.authorStyleText}>{' ' + item.contentMetaDataRef.salePrice !== 0 ? ' ' + item.contentMetaDataRef.salePrice : '0.0'}</Text>
                                </Text>
                            </View>
                            <View style={styles.headerViewStyle}>
                                <Text style={styles.authorStyle}>Binding Type -
                                  <Text style={styles.authorStyleText}>{' ' + item.contentMetaDataRef.coverType}</Text>
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', }}>
                        <View>
                            <Text style={{
                                color: 'black', flexWrap: 'wrap', fontSize: responsiveFontSize(1.7), fontFamily: Fonts.Roboto.Roboto_Bold,
                            }}>{CommanFunction.changeDateFormat(item.createdOn)}</Text>
                        </View>
                    </View> */}
                </SafeAreaView>
            </TouchableOpacity>

        </>
    )
}

const styles = StyleSheet.create({
    flex: 1,
    // titleStyle: {
    //     // opacity: 0.7,
    //     color: '#17a2b8', fontSize: responsiveFontSize(2),
    //     fontFamily: Fonts.Roboto.Roboto_Bold,
    // },
    // authorStyle: {
    //     color: '#808080',
    //     fontSize: responsiveFontSize(1.6),
    //     fontFamily: Fonts.Roboto.Roboto_Regular,
    // },
    // authorStyleText: {
    //     color: '#808080',
    //     fontSize: responsiveFontSize(1.5),
    //     fontFamily: Fonts.Roboto.Roboto_Regular,
    // },
    // headerViewStyle: {
    //     paddingBottom: responsiveWidth(1)
    // },
    safeAreaViewThird: {
        width: responsiveWidth(100),
        paddingHorizontal: responsiveWidth(2),
        // paddingVertical: responsiveWidth(2),
        borderBottomColor: 'grey',
        backgroundColor: '#ffff',
        borderBottomWidth: responsiveWidth(0.1),
    },
    bookImageView: {
        paddingHorizontal: responsiveWidth(3),
        paddingVertical: responsiveWidth(3),
        flexDirection: 'row',
        // backgroundColor: 'red',
    },
    bookImageOterView: {
        justifyContent: 'center',
        alignItems: 'center',
        opacity: 0.8,
        borderRadius: responsiveWidth(2),
        backgroundColor: '#ffff',// '#d9d9d9',
        shadowColor: '#30C1DD',
        shadowRadius: 10,
        shadowOpacity: 0.6,
        elevation: 8,
        // width: responsiveWidth(16),
        width: responsiveWidth(22),
        // height: responsiveHeight(12),
        height: responsiveHeight(15),
        shadowOffset: { width: 0, height: 4 },
    },
    bookImage: {

        // width: responsiveWidth(13),
        width: responsiveWidth(20),
        // height: responsiveHeight(10),
        height: responsiveHeight(14),
        // borderRadius: responsiveWidth(2)
    },
    bookTextView: {
        // borderRadius: responsiveWidth(2),
        // backgroundColor: '#ffff',// '#d9d9d9',
        // shadowColor: '#30C1DD',
        // shadowRadius: 10,
        // shadowOpacity: 0.6,
        // elevation: 8,
        width: responsiveWidth(67),
        flexWrap: 'wrap',
        //  backgroundColor:'red',
        marginLeft: responsiveWidth(3),
        paddingHorizontal: responsiveWidth(3),
    },
    bookText: {
        opacity: 0.8,
        fontFamily: Fonts.Roboto.Roboto_Bold,
        fontSize: responsiveFontSize(1.7)
    },
})

export default RenderBook;