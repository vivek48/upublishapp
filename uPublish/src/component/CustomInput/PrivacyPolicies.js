import React, { Component, useContext, useEffect, useState } from 'react'
import { View, Text, TextInput, SafeAreaView, TouchableOpacity, Image, StyleSheet, TouchableHighlight } from 'react-native'
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions'
import { Colors, Images, Fonts } from '../../styles/Index';
import { LocalizationContext } from '../../services/localization/LocalizationContext';
// import PrivacyModal from './PrivacyModal'
const PrivacyPolicies = ({ props, privacyPageOpen }) => {
    const { appLanguage, initializeAppLanguage, setAppLanguage, translations } = useContext(LocalizationContext);
    const [modalOpen, setModalOpen] = useState(false);
    const [privacyUri, setPrivacyUri] = useState('https://shop.reprobooks.in/privacy-policy');

    // const closeModal = (value) => {
    //     setModalOpen(value)
    // }
    return (
        <SafeAreaView>
            <TouchableOpacity onPress={() => { privacyPageOpen(true) }}>
                <View style={styles.searchOuterView}>
                    <Text style={styles.textStyle}>{privacyUri}</Text>
                </View>
            </TouchableOpacity>
            {/* {modalOpen &&
                <PrivacyModal modalOpen={modalOpen} props={props} closeModal={closeModal} webViewURI={privacyUri} />
            } */}
        </SafeAreaView>
    )
}
export default PrivacyPolicies;
const styles = StyleSheet.create({
    searchOuterView: {
        paddingHorizontal: responsiveWidth(3),
        paddingVertical: responsiveWidth(3),
    },

    textStyle: {
        fontFamily: Fonts.Roboto.Roboto_LightItalic,
        fontSize: responsiveFontSize(1.6),
        color: 'black',
        textAlign: 'center',
        textDecorationLine: 'underline',
        letterSpacing: responsiveWidth(0.1)
    }


})