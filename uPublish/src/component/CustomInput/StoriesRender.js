import React, { Component, useState } from 'react';
import { View, Text, Image, StyleSheet, SafeAreaView, ScrollView, TouchableOpacity, FlatList } from 'react-native';
import { responsiveWidth, responsiveFontSize, responsiveHeight } from 'react-native-responsive-dimensions';
import { Images, Fonts, Colors } from '../../styles/Index'
import { ApiCall, } from '../comman/ApiCall'
import Loader from '../comman/Loader'
import { CommanFunction } from "../comman/CommanFunction";
const StoriesRender = ({ item, index, navigation, }) => {
    const getSourceURL = (sourceURL) => {
        console.log(sourceURL);
        navigation.navigation.navigate('StoriesView',
            { sourceURL: sourceURL, navigationSreen: navigation.route.name });
    }

    return (
        <SafeAreaView>
            <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { getSourceURL(item._source.sourceURL) }}>
                <View style={{ flexDirection: 'row', paddingBottom: responsiveWidth(1), borderWidth: responsiveWidth(0.1) }}>
                    <View style={styles.bookImageOterView}>
                        <Image resizeMode={'center'} source={Images.library} style={{
                            borderColor: 'grey',
                            borderWidth: responsiveWidth(0.1),
                            width: responsiveWidth(20),
                            height: responsiveWidth(26)
                        }}></Image>
                    </View>
                    <View style={{ flexDirection: 'column', paddingTop: responsiveWidth(1), flexWrap: 'wrap', width: responsiveWidth(73.5), paddingLeft: responsiveWidth(4) }}>
                        <View style={styles.headerViewStyle} >
                            <Text style={styles.authorStyle}>{item._source.chapterName} </Text>
                        </View>
                        <View style={styles.headerViewStyle} >
                            <Text style={[styles.descriptionText, { flexWrap: 'wrap', fontFamily: Fonts.Roboto.Roboto_Regular }]}>Author Name :
                    <Text style={{ flexWrap: 'wrap', fontFamily: Fonts.Roboto.Roboto_Bold, color: '#17a2b8' }}>{" " + item._source.authorName}</Text>
                            </Text>
                        </View>
                        <View style={styles.headerViewStyle} >
                            <Text style={[styles.descriptionText, { flexWrap: 'wrap', fontFamily: Fonts.Roboto.Roboto_Regular }]}>
                                Language :
                       <Text style={{ fontFamily: Fonts.Roboto.Roboto_Bold, color: '#17a2b8' }}>{" " + item._source.language}</Text>
                            </Text>
                        </View>
                        <View style={styles.headerViewStyle} >
                            <Text style={[styles.descriptionText, { flexWrap: 'wrap', fontFamily: Fonts.Roboto.Roboto_Regular }]}>
                                Pages :
                       <Text style={{ fontFamily: Fonts.Roboto.Roboto_Bold, color: 'black' }}>{" " + item._source.pages}</Text>
                            </Text>
                        </View>
                        <View style={styles.headerViewStyle} >
                            <Text style={[styles.descriptionText, { flexWrap: 'wrap', fontFamily: Fonts.Roboto.Roboto_Regular }]}>
                                Selling Price :
                       <Text style={{ fontFamily: Fonts.Roboto.Roboto_Bold, color: 'black' }}>{" " + item._source.sp}</Text>
                            </Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        </SafeAreaView>

    )
}

const styles = StyleSheet.create({
    flex: 1,
    titleStyle: {
        opacity: 0.7,
        color: '#000000', fontSize: responsiveFontSize(2),
        fontFamily: Fonts.Roboto.Roboto_Bold,
    },
    authorStyle: {
        color: '#17a2b8',
        fontSize: responsiveFontSize(1.7),
        fontFamily: Fonts.Roboto.Roboto_Black,
    },

    buttonStyle: {
        marginVertical: responsiveWidth(2)
        , width: responsiveWidth(28), height: responsiveWidth(9),
        borderRadius: responsiveWidth(2), backgroundColor: '#17a2b8', padding: responsiveWidth(1.8),
    },
    buttonText: {
        textAlign: 'center',
        fontFamily: Fonts.Roboto.Roboto_Bold,
        color: 'white', fontSize: responsiveFontSize(1.8)
    },
    descriptionText: {
        color: 'black',
        fontSize: responsiveFontSize(1.5),
        fontFamily: Fonts.Roboto.Roboto_Bold,
    },
    headerViewStyle: {
        paddingBottom: responsiveWidth(0.4)
    },
    safeAreaViewThird: {
        width: responsiveWidth(100),
        paddingHorizontal: responsiveWidth(4),
        paddingVertical: responsiveWidth(2),
        backgroundColor: '#fefefe',
        borderBottomColor: 'grey',
        borderBottomWidth: responsiveWidth(0.1),
    },
    bookImageOterView: {
        justifyContent: 'center',
        alignItems: 'center',
        opacity: 0.8,
        borderRadius: responsiveWidth(2),
        backgroundColor: '#ffff',// '#d9d9d9',
        shadowColor: '#30C1DD',
        shadowRadius: 10,
        shadowOpacity: 0.6,
        elevation: 8,
        // width: responsiveWidth(),
        width: responsiveWidth(23),
        // height: responsiveHeight(12),
        height: responsiveHeight(18),
        shadowOffset: { width: 0, height: 4 },
    },
})


export default StoriesRender;