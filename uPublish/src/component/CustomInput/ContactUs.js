import React, { Component, useContext, useEffect, useState } from 'react'
import { View, Text, TextInput, SafeAreaView, TouchableOpacity, Image, StyleSheet, TouchableHighlight, ToastAndroid } from 'react-native'
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions'
import { Colors, Images, Fonts } from '../../styles/Index';
import Loader from "../comman/Loader";
import { ApiCall } from '../comman/ApiCall';
import { ScreenClass } from '../comman/ScreenClass'
import { ScreenName } from '../comman/ScreenName';
import { FirebaseConfig } from '../comman/FirebaseConfig';
import { ClickedButtonName } from '../comman/ClickedButtonName';
import { CommanFunction } from '../comman/CommanFunction';

import { LocalizationContext } from '../../services/localization/LocalizationContext'
const ContactUs = ({ props, closeButton }) => {
    const { appLanguage, initializeAppLanguage, setAppLanguage, translations } = useContext(LocalizationContext);

    const [contactNumber, setContactNumber] = useState('');
    const [message, setMessage] = useState('');
    const [contactNumberError, setContactNumberError] = useState('');
    const [messageError, setMessageError] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    const sendContact = () => {
        FirebaseConfig.realtimeClickedButton(ClickedButtonName.Contact_Submit);
        var requestData = {};
        if (contactNumber.trim() == '' || contactNumber == undefined || contactNumber == null || contactNumberError) {
            setContactNumberError(true)
            return true;
        } else {
            if (CommanFunction.CheckPhoneNumber(contactNumber)) {
                requestData.contactnumber = contactNumber.trim();
                setContactNumberError(false)
            } else {
                setContactNumberError(true)
                return true;
            }

        }
        if (message.trim() == '' || message == undefined || message == null) {
            setMessageError(true)
            return true;
        } else {
            requestData.message = message.trim();
            setMessageError(false)
        }
        setIsLoading(true);
        ApiCall.Api_Call('/saveContactUs', requestData, 'POST', true).then((result) => {
            setIsLoading(false)
            console.log(result);
            if (result.status == 1) {
                ToastAndroid.show("Successfully submitted request", ToastAndroid.LONG);

                setContactNumber('');
                setMessage('');
                closeButton();


            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });
    }
    const contactNumberSet = (value) => {
        setContactNumber(value);
        setContactNumberError(false);
    }
    const messageSet = (value) => {
        setMessage(value);
        setMessageError(false);
    }

    return (
        <SafeAreaView>
            <View style={styles.searchOuterView}>
                <Loader loading={isLoading} />
                <View >
                    <Text style={[styles.labelView, { fontFamily: Fonts.Roboto.Roboto_Black, textAlign: 'center' }]}>{translations.ContactWriteUs}</Text>
                </View>
                <View>
                    <Text style={{ fontFamily: Fonts.Roboto.Roboto_Regular, fontSize: responsiveFontSize(1.5), paddingVertical: responsiveWidth(1.2) }}>{translations.ContactWriteUsDes}</Text>
                </View>
                <View style={styles.commonViewPadding}>
                    <Text style={styles.labelView}>
                        {translations.ContactNumber} *</Text>
                </View>
                <View style={styles.commonViewPadding}>
                    <TextInput style={[styles.textInputStyles,]}
                        name='contactNumber'
                        editable={true}
                        value={contactNumber}
                        keyboardType={'numeric'}
                        onChangeText={(contactNumber) => { contactNumberSet(contactNumber.trim()) }}></TextInput>
                </View>
                {contactNumberError ?
                    <View style={{ paddingTop: responsiveWidth(1), }}>
                        <Text style={styles.errorColor}>{translations.PhoneNumberValidation}</Text>
                    </View> : null
                }
                <View style={styles.commonViewPadding}>
                    <Text style={styles.labelView}>
                        {translations.MessageContact} *</Text>
                </View>
                <View style={styles.commonViewPadding}>
                    <TextInput style={[styles.textInputStyles, { padding: responsiveWidth(1), letterSpacing: responsiveWidth(0.2), height: responsiveHeight(12), }]}
                        name='message'
                        editable={true}
                        multiline={true}
                        numberOfLines={10}
                        value={message}
                        onChangeText={(message) => { messageSet(message.trim()) }}></TextInput>
                </View>
                {messageError ?
                    <View style={{ paddingTop: responsiveWidth(1), }}>
                        <Text style={styles.errorColor}>{translations.MessageValidation}</Text>
                    </View> : null
                }

                <View style={{
                    justifyContent: 'space-around', flexDirection: 'row',
                    marginTop: responsiveWidth(4),
                }}>
                    <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { sendContact() }}>
                        <View style={styles.buttonStyles}>
                            <Text style={styles.buttonText}>{translations.SubmitButton}</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { setMessage(''); setContactNumber('') }}>
                        <View style={styles.buttonStyles}>
                            <Text style={styles.buttonText}>{translations.ResetButton}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{ marginTop: responsiveWidth(3) }}>
                    <View>
                        <Text style={[styles.labelView, { fontFamily: Fonts.Roboto.Roboto_Black, textAlign: 'center' }]}>{translations.CorpoRateName}</Text>
                    </View>
                    <View>
                        <Text style={styles.bottomText}>{translations.Text1}</Text>
                    </View>
                    <View>
                        <Text style={styles.bottomText}>{translations.Text2}</Text>
                    </View>
                    <View>
                        <Text style={styles.bottomText}>{translations.Text3}</Text>
                    </View>
                    <View>
                        <Text style={styles.bottomText}>{translations.Text4}</Text>
                    </View>
                    <View>
                        <Text style={styles.bottomText}>{translations.Text5}</Text>
                    </View>
                    <View>
                        <Text style={styles.bottomText}>{translations.Text6}</Text>
                    </View>
                    <View>
                        <Text style={styles.bottomText}>{translations.Text7}</Text>
                    </View>
                    <View>
                        <Text style={styles.bottomText}>{translations.b2bBusiness}</Text>
                    </View>

                    <View>
                        <Text style={styles.bottomText}>{translations.Text8}</Text>
                    </View>
                </View>
            </View>
        </SafeAreaView>
    )
}


export default ContactUs;
const styles = StyleSheet.create({
    searchOuterView: {
        paddingHorizontal: responsiveWidth(3),
        marginVertical: responsiveWidth(5),

    },
    commonViewPadding: {
        paddingTop: responsiveWidth(1.5),
    },
    textInputStyles: {
        backgroundColor: '#fff',
        borderColor: 'grey',
        borderWidth: responsiveWidth(0.2),
        height: responsiveHeight(5),
        paddingHorizontal: responsiveWidth(2),
        borderRadius: responsiveWidth(1)
    },
    labelView: {
        fontSize: responsiveFontSize(1.4),
        color: '#031215',
        // color: '#0e2c6e', 
        fontFamily: Fonts.Roboto.Roboto_Medium,
    },
    errorColor: {
        color: 'red', fontSize: responsiveFontSize(1.5)
    },

    buttonStyles: {
        justifyContent: 'center',
        alignItems: 'center',
        borderTopWidth: responsiveWidth(0.1),
        borderBottomWidth: responsiveWidth(0.1),
        backgroundColor: Colors.appColor,
        borderColor: 'lightgrey',
        borderRadius: responsiveWidth(0.7),
        width: responsiveWidth(43)
    },

    buttonText: {
        textAlign: 'center',
        padding: responsiveWidth(2),
        color: '#ffff',
        fontFamily: Fonts.Roboto.Roboto_Black,
        letterSpacing: responsiveWidth(0.2),
        fontSize: responsiveFontSize(1.5),
    },
    bottomText: {
        textAlign: 'center', fontFamily: Fonts.Roboto.Roboto_Regular,
        fontSize: responsiveFontSize(1.4),
        paddingTop: responsiveWidth(0.5),
        color: '#031215',
        // color: '#0e2c6e', 
    }

})

