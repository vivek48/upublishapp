import React, { Component, useState, useContext, useEffect } from 'react';
import { View, Text, Image, StyleSheet, SafeAreaView, ScrollView, TouchableOpacity, FlatList } from 'react-native';
import { responsiveWidth, responsiveFontSize, responsiveHeight } from 'react-native-responsive-dimensions';
import { Images, Fonts, Colors } from '../../styles/Index'
import { ApiCall, } from '../comman/ApiCall'
import Loader from '../comman/Loader'
import { CommanFunction } from "../comman/CommanFunction";
import { LocalizationContext } from "../../services/localization/LocalizationContext";
import { UserAddressContext } from "../../services/address/UserAddressContext";
import CustomRadioButton from "./CustomRadioButton";
const ExistingAddressRender = ({ currentAddress, selectedAddress }) => {
    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);
    const { setUserAddress, userAddress } = useContext(UserAddressContext);
    const [isLoading, setIsLoading] = useState(false);
    // const [addressList, setAddressList] = useState([]);
    useEffect(() => {
        getAllAddressUser();
    }, [])

    const getAllAddressUser = () => {
        setIsLoading(true);
        ApiCall.Api_Call('/address', '', 'GET', true).then((result) => {
            setIsLoading(false)
            // console.log(result);
            if (result.status == 1) {
                // setAddressList(result.result)
                setUserAddress('API', result.result);
            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });
    }

    // console.log("userAddress----", userAddress);
    return (
        <ScrollView>
            <View>
                <Loader loading={isLoading} />
                {userAddress.map((item, index) => (
                    <View key={index} style={[styles.listHeaderSites, { borderBottomWidth: userAddress.length - 1 == index ? responsiveWidth(0) : responsiveWidth(0.1) }]}>
                        <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { selectedAddress(item) }}>
                            <View style={styles.listHeaderViewAddress}>
                                <View>
                                    <CustomRadioButton key={index}
                                        disableRadio={false}
                                        color={"#27baba"}
                                        selected={currentAddress == item}
                                        onClick={() => { selectedAddress(item) }} />
                                </View>
                                <View style={{
                                    width: responsiveWidth(85), marginRight: responsiveWidth(3),
                                    paddingVertical: responsiveWidth(1.5)
                                }}>
                                    <Text style={[styles.listHeaderTextAddress, {
                                        opacity: 0.8,
                                        fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(2)
                                    }]}>{item.name} </Text>
                                    <Text style={styles.listHeaderTextAddress}>{item.address1}</Text>
                                    <Text style={styles.listHeaderTextAddress}>{item.address2}</Text>
                                    <Text style={styles.listHeaderTextAddress}>{item.city}{", " + item.state}</Text>
                                    <Text style={styles.listHeaderTextAddress}>{item.phoneNumber}</Text>
                                </View>

                            </View>
                        </TouchableOpacity>
                    </View>
                ))}
            </View>
        </ScrollView>
    )
}
const styles = StyleSheet.create({
    flex: 1,
    listHeaderViewAddress: {
        justifyContent: 'space-between',
        // alignItems: 'center',
        //  alignContent: 'center',
        paddingVertical: responsiveWidth(1),
        flexDirection: 'row'
    },
    listHeaderTextAddress: {
        // opacity: 0.7,
        fontSize: responsiveFontSize(1.7),
        // fontFamily: Fonts.Roboto.Roboto_Medium,
    },

})

export default ExistingAddressRender;