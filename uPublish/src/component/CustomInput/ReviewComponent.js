import React, { Component, useState, useContext, useEffect } from 'react';
import { View, Text, Image, StyleSheet, SafeAreaView, ScrollView, TouchableOpacity, FlatList, ToastAndroid } from 'react-native';
import { responsiveWidth, responsiveFontSize, responsiveHeight } from 'react-native-responsive-dimensions';
import { Images, Fonts, Colors } from '../../styles/Index'
import { ApiCall, } from '../comman/ApiCall'
import { SellrBoxAPICall, } from '../comman/SellrBoxAPICall'
import Loader from '../comman/Loader'
import { CommanFunction } from "../comman/CommanFunction";
import { LocalizationContext } from "../../services/localization/LocalizationContext";
import { UserAddressContext } from "../../services/address/UserAddressContext";
import AntDesign from 'react-native-vector-icons/AntDesign';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { ProgressBar } from '@react-native-community/progress-bar-android';
import Constants from '../comman/Constant';
const ReviewComponent = ({ item, index, isbn, props, avaragRating, ratingArr, book, ratingCount }) => {
    const [userID, setUserID] = useState(JSON.parse(Constants.LogIn_User).userId)
    const [userLike, setUserLike] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    const addReviewHelpfull = (item, index) => {
        var userData = {};
        userData.isbn = isbn,
            // userData.like = item._source.likes.length + 1,
            userData.reviewId = item._id,

            setIsLoading(true);
        ApiCall.Api_Call('/review/likeReview', userData, 'POST', true).then((result) => {
            console.log(result);
            setIsLoading(false);
            if (result.status == 1) {
                item._source.likes.push(userID);
                setUserLike(item._id);
                ToastAndroid.show(result.message, ToastAndroid.LONG);
            } else if (result.status == 0) {
                ToastAndroid.show(result.message, ToastAndroid.LONG);
            } else {

            }
        }).catch((error) => {
            console.log(error);
        });
    }

    const reportReview = (item, index) => {
        var userData = {};
        userData.isbn = isbn,
            userData.reviewId = item._id,
            setIsLoading(true);
        ApiCall.Api_Call('/review/reportReview', userData, 'POST', true).then((result) => {
            console.log(result);
            setIsLoading(false);
            if (result.status == 1) {

                ToastAndroid.show(result.message, ToastAndroid.LONG);
            } else if (result.status == 0) {
                ToastAndroid.show(result.message, ToastAndroid.LONG);
            } else {

            }
        }).catch((error) => {
            console.log(error);
        });
    }
    return (
        <View style={{ backgroundColor: '#ffff', }}>
            {/* HardCodeData.reviewListData */}
            <Loader loading={isLoading} />
            {index == 0 &&
                <View style={{
                    paddingHorizontal: responsiveWidth(4),
                    backgroundColor: '#fff',
                }}>

                    <View style={styles.reviewOuterView}>
                        <View style={[styles.reviewInnerView, { alignItems: 'center', }]}>
                            <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                                <View style={{}}>
                                    <Rating
                                        onStartRating={(ratingNUmber) => { console.log(ratingNUmber); }}
                                        imageSize={responsiveWidth(6)}
                                        startingValue={avaragRating}
                                        onSwipeRating={(ratingNUmber) => { console.log(ratingNUmber); }}
                                        ratingCount={5}
                                        readonly={true}
                                        ratingBackgroundColor={'grey'}
                                        minValue={0}
                                        style={{ paddingVertical: responsiveWidth(2), paddingLeft: responsiveWidth(1) }}
                                    />
                                </View>
                                <View>
                                    <Text style={{ paddingVertical: responsiveWidth(1), fontSize: responsiveFontSize(1.8), fontFamily: Fonts.Roboto.Roboto_Bold }}>
                                        {avaragRating} out of 5
                                    </Text>
                                </View>
                                <View>
                                    <Text style={{ fontSize: responsiveFontSize(1.8), fontFamily: Fonts.Roboto.Roboto_Regular }}>
                                        {ratingCount}  global ratings
                                    </Text>
                                </View>
                            </View>

                        </View>
                        <View style={{ borderLeftWidth: responsiveWidth(0.1), }}>

                        </View>
                        <View style={[styles.reviewInnerView, {}]}>

                            {ratingArr.map((item, index) => (

                               // console.log(item.ratingPercent),
                                <View key={index} style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <View>
                                        <Text style={styles.ratingProgressbar}>{item.key} star </Text>
                                    </View>
                                    {/* <View>
                                        <Image source={Images.star} resizeMode={'center'} tintColor={'#ffc107'} style={{ width: responsiveWidth(3), height: responsiveWidth(3) }}></Image>
                                    </View> */}
                                    <View style={{ paddingLeft: responsiveWidth(2) }}>
                                        <ProgressBar
                                            styleAttr="Horizontal"
                                            indeterminate={false}
                                            progress={item.ratingPercent / 100}
                                            color={"#ffc107"}
                                        />
                                    </View>
                                    <View>
                                        <Text style={styles.ratingnumber}>{parseInt(item.ratingPercent).toFixed(0)} %</Text>
                                    </View>
                                </View>
                            ))
                            }
                        </View>
                    </View>
                </View>
            }
            <View key={index} style={{
                borderBottomWidth: responsiveWidth(0.1),
                borderTopWidth: index == 0 ? responsiveWidth(0.1) : responsiveWidth(0),
                paddingHorizontal: responsiveWidth(6),
                marginBottom: responsiveWidth(1),
                paddingTop: index == 0 ? responsiveWidth(1) : responsiveWidth(0)
            }}>

                <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                    <View style={{
                        marginBottom: responsiveWidth(2),
                        // borderColor: 'grey',
                        // borderWidth: responsiveWidth(0.1),
                        // borderRadius: responsiveWidth(4),
                        // width: responsiveWidth(8),
                        // height: responsiveWidth(8),
                        // backgroundColor: 'lightgrey',
                        alignItems: 'center', justifyContent: 'space-around'
                    }}>
                        <View>
                            <Image source={{ uri: item._source.userPhoto }} resizeMode={"center"} style={{
                                borderWidth: responsiveWidth(0.1),
                                borderRadius: responsiveWidth(4.5),
                                width: responsiveWidth(7.5),
                                height: responsiveWidth(7.5),
                            }}></Image>
                        </View>
                    </View>
                    <View style={{ width: responsiveWidth(70), paddingLeft: responsiveWidth(3), height: responsiveWidth(8), }}>
                        <Text style={{
                            textAlign: 'justify', fontSize: responsiveFontSize(1.5),
                            fontFamily: Fonts.Roboto.Roboto_Regular
                        }}>{item._source.modifiedBy.userName} </Text>
                    </View>
                </View>

                <View style={{ flexDirection: 'row', }}>
                    <View >
                        {item._source.rating !== "" &&
                            <Rating
                                // onStartRating={0}
                                startingValue={Number(item._source.rating)}
                                imageSize={responsiveWidth(3)}
                                ratingCount={5}
                                ratingBackgroundColor={'grey'}
                                readonly={true}


                                style={{ paddingVertical: responsiveWidth(1), }}
                            />
                        }
                    </View>
                    <View style={{ width: responsiveWidth(70), paddingLeft: responsiveWidth(3), justifyContent: 'center' }}>
                        <Text style={{
                            textAlign: 'justify', fontSize: responsiveFontSize(1.5), color: '#c45500',
                            fontFamily: Fonts.Roboto.Roboto_Regular
                        }}>verified Purchase</Text>
                    </View>
                </View>

                {item._source.title !== "" &&
                    <View style={{ flexDirection: 'row', }}>
                        <View style={{ justifyContent: 'center' }}>
                            <Text style={{
                                textAlign: 'justify', fontSize: responsiveFontSize(1.5),
                                fontFamily: Fonts.Roboto.Roboto_Bold
                            }}>{item._source.title}</Text>
                        </View>
                    </View>
                }
                <View>
                    <Text style={{ textAlign: 'justify', color: 'grey', fontSize: responsiveFontSize(1.4) }}>Reviewed in India on
                     {"  " + CommanFunction.changeDateFormat(item._source.createdOn)}
                        {/* CommanFunction.changeDateFormat( */}
                    </Text>
                </View>
                <View>
                    <Text style={{
                        textAlign: 'justify',
                        fontSize: responsiveFontSize(1.5)
                    }}>{item._source.description}</Text>
                </View>
                {/* for rating images shown here */}
                <ScrollView horizontal={true}>
                    {item._source.imageurls.images !== undefined && item._source.imageurls.images.length > 0 && item._source.imageurls.images !== undefined && <View style={styles.reviewOuterView}>
                        {item._source.imageurls.images.map((item, index) => (
                            <View key={index} style={{ marginRight: responsiveWidth(2), paddingVertical: responsiveWidth(1.5), }}>
                                <Image source={{ uri: item.linkUrl }} resizeMode={'center'} style={styles.userReviewImages}></Image>
                            </View>
                        ))}
                    </View>
                    }
                </ScrollView>
                <View>
                    <Text style={{
                        textAlign: 'justify', paddingBottom: responsiveWidth(2),
                        fontFamily: Fonts.Roboto.Roboto_Light,
                        fontSize: responsiveFontSize(1.4)
                    }}>{item._source.likes.length} people found this helpful</Text>
                </View>
                <View style={{ flexDirection: 'row', paddingBottom: responsiveWidth(2) }}>
                    <View style={{
                        alignItems: 'center', flexDirection: 'row'
                        , justifyContent: 'space-between'
                    }}>
                        <TouchableOpacity onPress={() => { addReviewHelpfull(item, index) }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                                <View>
                                    <AntDesign name={'like1'} color={item._source.likes.includes(userID) || userLike == item._id ? 'green' : 'grey'} size={responsiveWidth(5)}></AntDesign>
                                </View>
                                <View style={{ paddingLeft: responsiveWidth(1) }}>
                                    <Text>{item._source.likes.length}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { reportReview(item, index) }}>
                            <View style={{ paddingLeft: responsiveWidth(10), flexDirection: 'row', alignItems: 'center', }}>
                                <View>
                                    <Text style={{
                                        textAlign: 'justify',
                                        backgroundColor: "lightgrey", paddingVertical: responsiveWidth(1.6),
                                        color: 'black', paddingHorizontal: responsiveWidth(2),
                                        fontSize: responsiveFontSize(1.4), borderRadius: responsiveWidth(1)
                                    }} >Report abuse</Text>
                                </View>
                            </View>
                        </TouchableOpacity>

                    </View>
                </View>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    flex: 1,
    safeAreaView: {
        // width: responsiveWidth(100),
        paddingVertical: responsiveWidth(2),
        paddingHorizontal: responsiveWidth(4),
        // paddingBottom: responsiveWidth(2),
        backgroundColor: Colors.appColor   //'#27baba'
    },
    userReviewImages: {

        borderWidth: responsiveWidth(0.1), borderColor: 'grey', width: responsiveWidth(12),
        height: responsiveWidth(15)
    },
    ratingProgressbar: { color: '#007185', fontFamily: Fonts.Roboto.Roboto_Black, fontSize: responsiveFontSize(1.6), paddingRight: responsiveWidth(2) },
    ratingnumber: { paddingVertical: responsiveWidth(1), paddingLeft: responsiveWidth(2), fontFamily: Fonts.Roboto.Roboto_Regular, textAlign: 'left', fontSize: responsiveFontSize(1.5) },
    reviewOuterView: {
        flexDirection: 'row', paddingVertical: responsiveWidth(2),
        justifyContent: 'space-between'
    },
    reviewInnerView: {
        // width: responsiveWidth(46),
        justifyContent: 'center',

    },
    bookText: {
        opacity: 0.8,
        fontFamily: Fonts.Roboto.Roboto_Bold,
        fontSize: responsiveFontSize(2.6)
    },
})

export default ReviewComponent