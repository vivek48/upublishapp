import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { responsiveWidth } from 'react-native-responsive-dimensions';
import { Colors, Fonts, Images } from "../../styles/Index";
export default class CustomRadioButton extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.disableRadio ? null : this.props.onClick}
                activeOpacity={Colors.activeOpecity} style={styles.radioButton}>

                <View style={[styles.radioButtonHolder, {
                    height: responsiveWidth(5),
                    width: responsiveWidth(5), borderColor: this.props.color
                }]}>
                    {
                        (this.props.selected)
                            ?
                            (<View style={[styles.radioIcon, {
                                height: responsiveWidth(5) / 2,
                                width: responsiveWidth(5) / 2, backgroundColor: this.props.color
                            }]}></View>)
                            :
                            null
                    }
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    radioButton: {
        flexDirection: 'row',
        margin: responsiveWidth(2),
        alignItems: 'center',
        justifyContent: 'center'
    },
    radioButtonHolder: {
        borderRadius: responsiveWidth(4),
        borderWidth: responsiveWidth(0.5),
        justifyContent: 'center',
        alignItems: 'center'
    },
    radioIcon: {
        borderRadius: responsiveWidth(2),
        justifyContent: 'center',
        alignItems: 'center'
    },
});