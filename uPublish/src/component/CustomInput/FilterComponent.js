import React, { Component, useContext, useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, TextInput, TouchableOpacity, Modal } from 'react-native';
import Header from "../CustomInput/Header"
import { LocalizationContext } from '../../services/localization/LocalizationContext';
import { Colors, Fonts, Images } from '../../styles/Index'
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import CheckBox from '@react-native-community/checkbox';
const FilterComponent = ({ filterList, authorList, publisherList, languageList
    , setAuthor, setPublisher, setLanguage, applyOrCloseModal, modalOpen }, props) => {

    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);
    // console.log(filterList);
    return (
        <Modal
            transparent={true}
            animationType={'none'}
            visible={modalOpen}
            // key={index}
            onRequestClose={() => { }}>
            <View style={styles.modalBackground}>
                <View style={styles.centerModal}>
                    <View style={[

                        , {
                            flexDirection: 'row',
                            height: responsiveHeight(25),
                        }]
                        //     {
                        //     flexDirection: 'row',
                        //     height: responsiveHeight(25),
                        // }

                    }>
                        <View style={styles.modalHeaderView}>
                            <View style={styles.headingStyle}>
                                <Text style={styles.modalHeaderText}>{filterList[0].heading}</Text>
                            </View>
                            {filterList[0].facets.map((item, index) => (
                                <View style={styles.checkBoxView} key={index}>
                                    <TouchableOpacity
                                        onPress={() => { setAuthor(item.key) }}
                                        style={{ flexDirection: 'row', alignContent: 'center', alignItems: 'center' }}>
                                        <CheckBox
                                            disabled={false}
                                            value={authorList.includes(item.key)}
                                            onChange={() => setAuthor(item.key)}
                                        // onValueChange={() => setAuthor(item.key)}
                                        />
                                        <Text style={styles.textCheckBox}> {item.key}{" (" + item.doc_count + ")"}</Text>
                                    </TouchableOpacity>
                                </View>
                            ))}
                        </View>
                        <View style={[styles.modalHeaderView, { marginLeft: responsiveWidth(3), }]}>
                            <View style={styles.headingStyle}>
                                <Text style={styles.modalHeaderText}>{filterList[1].heading}</Text>
                            </View>
                            {filterList[1].facets.map((item, index) => (
                                <View style={styles.checkBoxView} key={index}>
                                    <CheckBox
                                        disabled={false}
                                        value={publisherList.includes(item.key)}
                                        onChange={() => setPublisher(item.key)}
                                    />
                                    <Text style={styles.textCheckBox}>
                                        {item.key} {" (" + item.doc_count + ")"}</Text>
                                </View>
                            ))}
                        </View>
                    </View>
                    <View style={{
                        flexDirection: 'row', height: responsiveHeight(17),
                    }}>
                        <View style={styles.modalHeaderView}>
                            <View style={styles.headingStyle}>
                                <Text style={styles.modalHeaderText}>{filterList[2].heading}</Text>
                            </View>

                            {filterList[2].facets.map((item, index) => (
                                <View style={styles.checkBoxView} key={index}>
                                    <CheckBox
                                        disabled={false}
                                        value={languageList.includes(item.key)}
                                        onChange={() => setLanguage(item.key)}
                                    />
                                    <Text style={styles.textCheckBox}>{item.key}
                                        {" (" + item.doc_count + ")"}</Text>
                                </View>
                            ))}
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', }}>
                        <View style={{ width: responsiveWidth(38) }}>
                        </View>
                        <View style={[styles.modalHeaderView, {
                            flexDirection: 'row',
                            justifyContent: 'space-around',
                        }]}>
                            <TouchableOpacity
                                style={{ marginRight: responsiveWidth(9) }}
                                activeOpacity={Colors.activeOpecity} onPress={() => { applyOrCloseModal('Apply') }}>
                                <View style={styles.buttonStyle}>
                                    <Text style={styles.textStyle}>{translations.FilterApplyButton}</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { applyOrCloseModal('close') }}>
                                <View style={styles.buttonStyle}>
                                    <Text style={styles.textStyle}>{translations.FilterCloseButton}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        </Modal>
    )
}
const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        marginTop: responsiveWidth(15)
    },
    centerModal: {
        backgroundColor: 'lightgrey',
        height: responsiveWidth(95),
        width: responsiveWidth(88),
        borderRadius: responsiveWidth(4),
        borderRadius: responsiveWidth(2),
        backgroundColor: '#ffff',// '#d9d9d9',
        shadowColor: '#30C1DD',
        shadowRadius: 10,
        shadowOpacity: 0.6,
        elevation: 8,
        shadowOffset: { width: 0, height: 4 },
    },
    modalHeaderView: {
        width: responsiveWidth(39),
        paddingVertical: responsiveWidth(3),
        paddingRight: responsiveWidth(4.5)
    },
    headingStyle: {
        paddingLeft: responsiveWidth(10), paddingVertical: responsiveWidth(1)
    },
    modalHeaderText: {
        fontFamily: Fonts.Roboto.Roboto_Bold,
        fontSize: responsiveFontSize(2), color: Colors.appColor
    },
    buttonStyle: {
        backgroundColor: Colors.appColor, borderRadius: responsiveWidth(3),
        width: responsiveWidth(20), justifyContent: 'center', height: responsiveWidth(9)
    },
    textStyle: {
        textAlign: 'center',
        fontFamily: Fonts.Roboto.Roboto_Bold, color: '#ffff',
        fontSize: responsiveFontSize(2)
    },
    textCheckBox:
    {
        fontFamily: Fonts.Roboto.Roboto_Regular,
        fontSize: responsiveFontSize(1.8),

    },
    checkBoxView: {
        flexDirection: 'row',
        alignContent: 'center',
        alignItems: 'center',
    },



    bookImageOterView: {
        // justifyContent: 'center',
        // alignItems: 'center',
        opacity: 0.8,
        borderRadius: responsiveWidth(2),
        backgroundColor: '#ffff',// '#d9d9d9',
        shadowColor: '#30C1DD',
        shadowRadius: 10,
        shadowOpacity: 0.6,
        elevation: 8,
        // width: responsiveWidth(),
        // width: responsiveWidth(23),
        // height: responsiveHeight(12),
        // height: responsiveHeight(18),
        shadowOffset: { width: 0, height: 4 },
    },



});


export default FilterComponent;