import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, SafeAreaView, ScrollView, TouchableOpacity, FlatList } from 'react-native';
import { responsiveWidth, responsiveFontSize, responsiveHeight } from 'react-native-responsive-dimensions';
import { Images, Fonts, Colors } from '../../styles/Index'
import CheckBoxIcon from 'react-native-vector-icons/Fontisto';
function RenderSellBoxComponent({ item, index, navigation, getNews, countSellBoxItem }) {
    return (
        <SafeAreaView key={index} style={[styles.safeAreaViewThird,]}>
            <View>
                <View style={{ paddingHorizontal: responsiveWidth(2), }}>
                    <View>
                        <Text style={{
                            color: '#64B0C8',
                            fontSize: responsiveFontSize(1.6),
                            fontFamily: Fonts.Roboto.Roboto_Bold,
                            letterSpacing: responsiveWidth(0.2)
                        }}>{item._source.title.trim()}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{ flexDirection: 'column', }}>
                            <View style={{ paddingVertical: responsiveWidth(1) }}>
                                <Text style={styles.selleText}>SellBox Price  -
                                         <Text style={styles.sellerTextValue}>{" " + item._source.currency}{item._source.sp}</Text>
                                </Text>
                            </View>
                            < View style={{ paddingBottom: responsiveWidth(1) }}>
                                <Text style={styles.selleText}>Seller -
                                               <Text style={styles.sellerTextValue}>{' ' + item._source.sellerName.trim()}</Text>
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>
                {/* <View>
                    <TouchableOpacity activeOpacity={0.5} onPress={() => { getNews(item._source.upID) }}>
                        <View style={{
                            alignItems: 'center',
                            width: countSellBoxItem.includes(item._source.upID) ? responsiveWidth(8) : responsiveWidth(7)
                        }}>
                            <CheckBoxIcon
                                color={countSellBoxItem.includes(item._source.upID) ? '#27baba' : Colors.NewsColor}
                                size={countSellBoxItem.includes(item._source.upID) ? responsiveWidth(5.5)
                                    : responsiveWidth(4.9)}
                                name={countSellBoxItem.includes(item._source.upID) ? "checkbox-active" : "checkbox-passive"}
                            />
                        </View>
                    </TouchableOpacity>
                </View> */}
                <View style={{ flexDirection: 'row', paddingHorizontal: responsiveWidth(2), lignItems: 'center', justifyContent: 'space-between', }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View>
                            <Image source={Images.news} style={{ width: responsiveWidth(4), height: responsiveHeight(2) }}></Image>
                        </View>
                        <View style={{ paddingLeft: responsiveWidth(3) }}>
                            <Text style={styles.bottomText}>
                                {item._source.channelSource}</Text>
                        </View>
                    </View>
                    <View>
                        {item._source.itemRating !== null && item._source.itemRating !== undefined && item._source.itemRating !== '' &&
                            <Text style={styles.bottomText}>
                                Rating :
                                    <Text style={{ color: '#212529', textAlign: 'center' }}> {" " + item._source.itemRating} </Text>
                            </Text>
                        }
                    </View>
                    <View>
                        {item._source.sellBox &&
                            <Text style={{
                                color: 'red',
                                textAlign: 'center', fontSize: responsiveFontSize(2.4),
                                fontFamily: Fonts.Roboto.Roboto_Medium,
                            }}> 🛒 </Text>
                        }
                    </View>
                </View>
            </View>
        </SafeAreaView>
    )
}
const styles = StyleSheet.create({
    flex: 1,
    safeAreaViewThird: {
        paddingHorizontal: responsiveWidth(1),
        paddingVertical: responsiveWidth(1.5),
        backgroundColor: '#fefefe',
        borderBottomColor: 'grey',
        borderBottomWidth: responsiveWidth(0.1),
    },
    ViewDate: {
        width: responsiveWidth(100),
        paddingHorizontal: responsiveWidth(4),
        height: responsiveHeight(5),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#fefefe',
        borderBottomColor: 'grey',
        borderBottomWidth: responsiveWidth(0.1)
    },
    dateStyle: {
        fontSize: responsiveFontSize(1.8),
        fontFamily: Fonts.Roboto.Roboto_Medium
    },
    selleText: {
        color: '#212529', fontSize: responsiveFontSize(1.3),
        fontFamily: Fonts.Roboto.Roboto_Light,
        letterSpacing: responsiveWidth(0.2)
    },
    sellerTextValue: {
        color: '#f6a3bd', fontSize: responsiveFontSize(1.3),
        fontFamily: Fonts.Roboto.Roboto_Bold,
        letterSpacing: responsiveWidth(0.2)
    },
    bottomText: { color: '#b4b4b4', fontFamily: Fonts.Roboto.Roboto_Bold }

})

export default RenderSellBoxComponent;