import React, { Component, useState, useContext, useEffect } from 'react';
import { View, Text, Image, StyleSheet, SafeAreaView, ScrollView, TouchableOpacity, FlatList } from 'react-native';
import { responsiveWidth, responsiveFontSize, responsiveHeight } from 'react-native-responsive-dimensions';
import { Images, Fonts, Colors } from '../../styles/Index'
import { ApiCall, } from '../comman/ApiCall'
import { SellrBoxAPICall, } from '../comman/SellrBoxAPICall'
import Loader from '../comman/Loader'
import { CommanFunction } from "../comman/CommanFunction";
import { LocalizationContext } from "../../services/localization/LocalizationContext";
import { UserAddressContext } from "../../services/address/UserAddressContext";
import Icon from 'react-native-vector-icons/AntDesign';

import RenderSellBoxComponent from '../CustomInput/RenderSellBoxComponent';
import Header from "../CustomInput/Header";
const SellBoxItem = (props) => {
    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);
    const { setUserAddress, userAddress } = useContext(UserAddressContext);
    const [isLoading, setIsLoading] = useState(false);
    const [sellerBoxItem, setSellerBoxItem] = useState(props.route.params.sellerBoxItem);
    useEffect(() => {

    }, [])
    const closeExistsAddress = () => {
        props.navigation.navigate('BookDetails')
    }
    return (
        <View>
            <Loader loading={isLoading} />
            <Header headerTitle={translations.HeaderName} backScreen={closeExistsAddress} navigation={props.navigation} />
            <FlatList
                data={sellerBoxItem}
                // onEndReached={this.handleLoadMoreData}
                onEndReachedThreshold={200}
                renderItem={({ item, index, }) => (
                    <RenderSellBoxComponent index={index} item={item} navigation={props} />
                )}
                keyExtractor={(item, index) => item._id.toString()}
            // ListFooterComponent={this.renderFooter}
            // extraData={this.state.refresh}
            />
        </View>

    )
}
const styles = StyleSheet.create({
    flex: 1,
    bookheaderView: {
        flexDirection: 'row', alignItems: 'center',
        paddingLeft: responsiveWidth(5),
        paddingRight: responsiveWidth(3),
        justifyContent: 'space-between',
        width: responsiveWidth(100), height: responsiveHeight(8),
    },
})

export default SellBoxItem;