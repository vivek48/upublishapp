
import React, { useState } from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions'
import { Fonts, Images, Colors } from '../../styles/Index'
const CustomButton = ({ buttonTitle, onPress, ...inputProps }) => {
    return (
        <View style={{
            paddingHorizontal: responsiveWidth(6), paddingVertical: responsiveWidth(4),
            //  backgroundColor: '#fff' 
        }}>
            <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { onPress() }}>
                <View>
                    <Text style={styles.buttonStyle}>{buttonTitle}</Text>
                </View>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    buttonStyle: {
        backgroundColor: Colors.appColor,
        textAlign: 'center',
        color: '#ffff',
        fontSize: responsiveFontSize(1.8),
        fontFamily: Fonts.Roboto.Roboto_Bold,
        padding: responsiveWidth(3.5), borderRadius: responsiveWidth(3)
    }

})




export default CustomButton;