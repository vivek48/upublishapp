import { View, Text, Image, } from 'react-native';
import React, { useContext, useEffect } from 'react';
import { NavigationContainer, DarkTheme, DefaultTheme, } from '@react-navigation/native';
import { enableScreens } from 'react-native-screens';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import OrdersScreen from "../screens/orders/OrdersScreen";

import SettingScreen from '../screens/setting/SettingScreen'
import AddressScreen from "../screens/address/AddressScreen"
import OrderDetailScreen from '../screens/orders/OrderDetailScreen';
const Stack = createNativeStackNavigator();
const OrdersStack = () => {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false }} children={AddressScreen} initialRouteName={"Orders"}>
            <Stack.Screen name="Orders" component={OrdersScreen} />
            <Stack.Screen name="OrderDetail" component={OrderDetailScreen} />
            <Stack.Screen name="Setting" component={SettingScreen} />
        </Stack.Navigator>
    )
}

export default OrdersStack;
