import { View, Text, Image, } from 'react-native';
import React, { useContext } from 'react';
import { NavigationContainer, DarkTheme, DefaultTheme, } from '@react-navigation/native';
import { enableScreens } from 'react-native-screens';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import LibraryScreen from "../screens/library/LibraryScreen";
import SettingScreen from '../screens/setting/SettingScreen'
import EPubReader from "../screens/ePubReader/EPubReader";
const Stack = createNativeStackNavigator();

const LibrariesStack = () => {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName={'Library'} >
            <Stack.Screen name='EPubReader' component={EPubReader} />
            <Stack.Screen name="Library" component={LibraryScreen} />
            {/* <Stack.Screen name="Setting" component={SettingScreen} /> */}
        </Stack.Navigator>
    )
}

export default LibrariesStack;
