import { View, Text, Image, } from 'react-native';
import React, { useContext, useEffect } from 'react';
import { NavigationContainer, DarkTheme, DefaultTheme, } from '@react-navigation/native';
import { enableScreens } from 'react-native-screens';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import BottomNavigator from "./BottomNavigator";

import BookDetails from "../component/CustomInput/BookDetails";
const Stack = createNativeStackNavigator();
const DeepLinkStack = ({ props, value }) => {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName={"BookDetails"}>
            <Stack.Screen name="BookDetails" component={BookDetails} bookItem={value} />
            <Stack.Screen name="BottomNavigator" component={BottomNavigator} />
        </Stack.Navigator>
    )
}

export default DeepLinkStack;
