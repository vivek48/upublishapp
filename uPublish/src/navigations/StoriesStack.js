import { View, Text, Image, } from 'react-native';
import React, { useContext } from 'react';
import { NavigationContainer, DarkTheme, DefaultTheme, } from '@react-navigation/native';
import { enableScreens } from 'react-native-screens';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import StoriesScreen from "../screens/stories/StoriesScreen";
import SettingScreen from '../screens/setting/SettingScreen'
import StoriesViewScreen from "../screens/storiesview/StoriesViewScreen";
const Stack = createNativeStackNavigator();
const StoriesStack = () => {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
            <Stack.Screen name="Stories" component={StoriesScreen} />
            {/* <Stack.Screen name="Setting" component={SettingScreen} /> */}
            <Stack.Screen name="StoriesView" component={StoriesViewScreen} />
        </Stack.Navigator>
    )
}

export default StoriesStack;
