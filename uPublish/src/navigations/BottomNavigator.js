import { View, Text, Image, } from 'react-native';
import React, { useContext } from 'react';
import { enableScreens } from 'react-native-screens';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import AsyncStorage from '@react-native-community/async-storage';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import NewBookStack from './NewBookStack';
import StoriesStack from "./StoriesStack";
import LibrariesStack from "./LibrariesStack";
import SearchStack from "./SearchStack";
import OrdersStack from "./OrdersStack";
import { LocalizationContext } from '../services/localization/LocalizationContext'
import { Fonts, Colors, Images } from "../styles/Index";
const Tab = createBottomTabNavigator();
const BottomNavigator = () => {
    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);

    return (
        <Tab.Navigator
            initialRouteName={translations.Home}
          
            tabBarOptions={{
                activeTintColor: Colors.appColor,
                inactiveTintColor: Colors.inactiveButtonColor,     //'#17a2b8',
                style: { paddingVertical: responsiveWidth(2), backgroundColor: '#f5f5f5', height: responsiveHeight(7), width: responsiveWidth(100), },

            }}


        >

            {/* <Tab.Screen
                name={translations.Search}
                component={SearchStack}
                options={{
                    tabBarLabel: (translations.Search),
                    tabBarIcon: ({ color, size }) => (

                        <Image source={Images.search} tintColor={color} style={{ width: responsiveWidth(7), height: responsiveWidth(7) }} />
                    ),
                }}
            /> */}




            {/* <Tab.Screen
                name={translations.Stories}
                component={StoriesStack}
                options={{
                    tabBarLabel: (translations.Stories),
                    tabBarIcon: ({ color, size }) => (
                        <Image source={Images.stories} tintColor={color} style={{ width: responsiveWidth(6.5), height: responsiveWidth(6.5) }} />
                    ),
                }}
            /> */}
            {/* <Tab.Screen
                name={translations.Library}
                component={LibrariesStack}
                options={{
                    tabBarLabel: (translations.Library),
                    tabBarIcon: ({ color, size }) => (

                        <Image source={Images.library} tintColor={color} style={{ width: responsiveWidth(8), height: responsiveWidth(7) }} />
                    ),
                }}
            /> */}


            <Tab.Screen
                name={translations.Home}
                component={NewBookStack}

                options={{
                    tabBarLabel: (translations.Home),

                    tabBarIcon: ({ color, size }) => (
                        <Image source={Images.Homenew} tintColor={color} style={{ width: responsiveWidth(7), height: responsiveWidth(7) }} />
                    ),

                }}

            />

            <Tab.Screen
                name={translations.Orders}
                component={OrdersStack}
                options={{
                    tabBarLabel: (translations.Orders),
                    tabBarIcon: ({ color, size }) => (
                        <Image source={Images.order} tintColor={color} style={{ width: responsiveWidth(7), height: responsiveWidth(7) }} />
                    ),
                }}
            />




        </Tab.Navigator>
    )
}

export default BottomNavigator;
