import { View, Text, Image, Linking, Platform } from 'react-native';
import React, { useContext, useEffect, useState } from 'react';
import { NavigationContainer, DarkTheme, DefaultTheme, } from '@react-navigation/native';
import { enableScreens } from 'react-native-screens';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import LanguageScreen from '../screens/language/LanguageScreen';
import LogInScreen from '../screens/login/LogInScreen'
import BottomNavigator from "./BottomNavigator";
import { LocalizationContext } from '../services/localization/LocalizationContext'
import { UserContext } from "../services/loginuser/UserContext";
import SignUpScreen from '../screens/signup/SignUpScreen';
import CheckDummy from '../screens/login/CheckDummy';
// import ValidationPopup from "../component/CustomInput/ValidationPopup";
const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();
import LoginScreenChanges from '../screens/login/LoginScreenChanges'
import DeepLinkStack from "./DeepLinkStack";

import VerifyOTPScreen from '../screens/signup/VerifyOTPScreen';


enableScreens();
const LanguageStack = () => {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false, }}  >
            <Stack.Screen name="Language" component={LanguageScreen} />
        </Stack.Navigator>
    )
}
const LoginStack = () => {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false, }} initialRouteName={'LoginScreenChanges'}>
            <Stack.Screen name="LogIn" component={LogInScreen} />
            <Stack.Screen name="SignUp" component={SignUpScreen} />
            <Stack.Screen name="LoginScreenChanges" component={LoginScreenChanges} />
            <Stack.Screen name="VerifyOTPScreen" component={VerifyOTPScreen} />
            {/* <Stack.Screen name="ValidationPopup" component={ValidationPopup} /> */}
        </Stack.Navigator>
    )
}

const Routes = (props) => {
    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);
    const { setUserLogin, userLogin } = useContext(UserContext);
    return (
        < NavigationContainer theme={DefaultTheme}>
            {appLanguage != null ? (
                userLogin != null && userLogin != '' ? <BottomNavigator /> :

                    <LoginStack />
            )
                : <LanguageStack />
            }

        </ NavigationContainer>
    )
}


export default Routes;