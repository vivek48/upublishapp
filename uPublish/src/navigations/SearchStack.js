import { View, Text, Image, } from 'react-native';
import React, { useContext } from 'react';
import { NavigationContainer, DarkTheme, DefaultTheme, } from '@react-navigation/native';
import { enableScreens } from 'react-native-screens';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SearchScreen from "../screens/search/SearchScreen";
import SettingScreen from '../screens/setting/SettingScreen'
import AddressScreen from "../screens/address/AddressScreen";
const Stack = createNativeStackNavigator();

const SearchStack = () => {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName={"Search"}>
            <Stack.Screen name="Search" component={SearchScreen} />
            <Stack.Screen name="Setting" component={SettingScreen} />
            <Stack.Screen name="Address" component={AddressScreen} />
        </Stack.Navigator>
    )
}

export default SearchStack;
