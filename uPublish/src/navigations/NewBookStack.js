import { View, Text, Image, Platform, Linking } from 'react-native';
import React, { useContext, useEffect, useState } from 'react';
import { NavigationContainer, DarkTheme, DefaultTheme, } from '@react-navigation/native';
import { enableScreens } from 'react-native-screens';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import HomeScreen from "../screens/home/HomeScreen";
import SettingScreen from '../screens/setting/SettingScreen'
import AddressScreen from "../screens/address/AddressScreen";
import BookDetails from "../component/CustomInput/BookDetails";
import GoToShop from '../component/CustomInput/GoToShop';
import Constants from "../component/comman/Constant";
const Stack = createNativeStackNavigator();
const NewBookStack = (props) => {
    const [linkingURL, setLinkingURL] = useState(false);
    return (
        <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName={"Books"}>
            {/*  Constants.LinkingURL !== null &&  */}
            <Stack.Screen name="Books" component={HomeScreen} />
            <Stack.Screen name="Setting" component={SettingScreen} />
            <Stack.Screen name="Address" component={AddressScreen} />
            <Stack.Screen name="BookDetails" component={BookDetails} />
            <Stack.Screen name="GoToShop" component={GoToShop} />
        </Stack.Navigator>
    )
    // ) : true
}

export default NewBookStack;
