import React, { Component, useContext, useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, TextInput, TouchableOpacity, Image, ToastAndroid, PermissionsAndroid } from 'react-native';
import Header from "../../component/CustomInput/Header";
import { LocalizationContext } from '../../services/localization/LocalizationContext';
import Loader from "../../component/comman/Loader";
import { Colors, Fonts, Images } from '../../styles/Index'
import { ApiCall } from '../../component/comman/ApiCall';
import { CommanFunction } from "../../component/comman/CommanFunction";
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import ReviewComponent from "../../component/CustomInput/ReviewComponent";
const AllReviewsScreen = (props) => {
    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);
    const [isLoading, setIsLoading] = useState(false);
    const [isbn, setIsbn] = useState(props.route.params.isbn);
    const [reviewsData, setReviewsData] = useState([]);
    const [startFrom, setStartFrom] = useState(0);
    const [book, setBook] = useState(props.route.params.book)
    const [reviewsDataLength, setReviewsDataLength] = useState(0);
    const [avgRating, setAvgRating] = useState(0.0);
    const [ratingArr, setRatingArr] = useState([]);
    const [ratingCount, setRatingCount] = useState(0);
    useEffect(() => {
        fetchBookAllReview(isbn);

    })
    const backScreen = () => {
        props.navigation.pop();
    }
    const fetchBookAllReview = (isbn) => {
        var userData = {};
        userData.isbn = isbn;
        userData.startFrom = startFrom;
        // setIsLoading(true);
        ApiCall.Api_Call('/review/fetchBookAllReview', userData, 'POST', true).then((result) => {
            // console.log(result);
            if (result.status == 1) {
                setIsLoading(false)
                setRatingCount(result.result.totalCount);
                setReviewsDataLength(result.result.Records.length)
                setRatingArr(result.result.ratingArr.reverse());
                setAvgRating(result.result.avg_rating);
                setReviewsData(result.result.Records);
            } else if (result.status == 0) {
                setReviewsData([]);

                setIsLoading(false)

            } else {
                setReviewsData([]);
                setIsLoading(false)
            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });
    }

    return (
        <View style={{ flex: 1 }}>
            <Header headerTitle={translations.AllProductReview} backScreen={backScreen} navigation={props.navigation} />
            <Loader loading={isLoading} />
            { reviewsData.length > 0 &&
                < View style={{ flex: 1 }}>
                    <FlatList
                        data={reviewsData}
                        onEndReachedThreshold={200}
                        renderItem={({ item, index, }) => (
                            <ReviewComponent index={index} ratingCount={ratingCount} avaragRating={avgRating} ratingArr={ratingArr} item={item} props={props} book={book} isbn={isbn} />
                        )}
                        keyExtractor={(item, index) => item._id.toString()}
                    />
                </View>
            }
        </View >
    )
}

const styles = StyleSheet.create({
    textAreaContainer: {
        borderColor: 'grey',
        paddingHorizontal: responsiveWidth(3),
        paddingVertical: responsiveWidth(1),
        flex: 8,
    },
    textArea: {
        justifyContent: "flex-start",
        textAlignVertical: 'top',
    }
})
export default AllReviewsScreen;
