import React, { Component, useContext, useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, TextInput, TouchableOpacity, Image, ToastAndroid, PermissionsAndroid, ScrollView } from 'react-native';
import Header from "../../component/CustomInput/Header";
import { LocalizationContext } from '../../services/localization/LocalizationContext';
import Loader from "../../component/comman/Loader";
import { Colors, Fonts, Images } from '../../styles/Index'
import { ApiCall } from '../../component/comman/ApiCall';
import { CommanFunction } from "../../component/comman/CommanFunction";
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { FloatingLabelInput } from 'react-native-floating-label-input';
import { Rating, AirbnbRating } from 'react-native-ratings';
import Constants from "../../component/comman/Constant";
import {
    launchCamera,
    launchImageLibrary
} from 'react-native-image-picker';
const ReviewProduct = (props) => {
    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);
    const [isLoading, setIsLoading] = useState(false);
    const [reviewText, setReviewText] = useState('');
    const [reviewHeadingText, setReviewHeadingText] = useState('');
    const [book, setBook] = useState(props.route.params.bookItem);
    const [reviewNumber, setReviewNumber] = useState(0);
    const [reviewImageList, setReviewImageList] = useState([]);
    const [removeSelectedImage, setRemoveSelectedImage] = useState('');
    const [reviewHeader, setReviewHeader] = useState([]);
    const [reviewHeaderTextSelected, setReviewHeaderTextSelected] = useState('');
    useEffect(() => {
        if (reviewHeader == '') {
            getReviewHeadline();
        }

    })
    const backScreen = () => {
        props.navigation.pop();
    }




    const getReviewHeadline = () => {
        // setIsLoading(true);
        ApiCall.Api_Call('/review/getReviewHeadline', '', 'POST', true).then((result) => {
            console.log(result, "----");
            if (result.status == 1) {
                setIsLoading(false);
                setReviewHeader(result.result);
                // setReviewHeader(result.result._doc);
            } else if (result.status == 0) {

                setIsLoading(false)
            } else {

                setIsLoading(false)
            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });
    }
















    const requestCameraPermission = async () => {
        if (Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    {
                        title: 'Camera Permission',
                        message: 'App needs camera permission',
                    },
                );
                // If CAMERA Permission is granted
                return granted === PermissionsAndroid.RESULTS.GRANTED;
            } catch (err) {
                console.warn(err);
                return false;
            }
        } else return true;
    };

    const requestExternalWritePermission = async () => {
        if (Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                    {
                        title: 'External Storage Write Permission',
                        message: 'App needs write permission',
                    },
                );
                // If WRITE_EXTERNAL_STORAGE Permission is granted
                return granted === PermissionsAndroid.RESULTS.GRANTED;
            } catch (err) {
                console.warn(err);
                alert('Write permission err', err);
            }
            return false;
        } else return true;
    };

    const captureImage = async (type) => {
        let options = {
            mediaType: type,
            maxWidth: 300,
            maxHeight: 550,
            quality: 1,
            videoQuality: 'low',
            durationLimit: 30, //Video max duration in seconds
            saveToPhotos: true,
        };
        let isCameraPermitted = await requestCameraPermission();
        let isStoragePermitted = await requestExternalWritePermission();
        if (isCameraPermitted && isStoragePermitted) {
            launchCamera(options, (response) => {
                console.log('Response = ', response);

                if (response.didCancel) {
                    // alert('User cancelled camera picker');
                    return;
                } else if (response.errorCode == 'camera_unavailable') {
                    // alert('Camera not available on device');
                    return;
                } else if (response.errorCode == 'permission') {
                    // alert('Permission not satisfied');
                    return;
                } else if (response.errorCode == 'others') {
                    // alert(response.errorMessage);
                    return;
                }
                // console.log('base64 -> ', response.base64);
                // console.log('uri -> ', response.uri);
                // console.log('width -> ', response.width);
                // console.log('height -> ', response.height);
                // console.log('fileSize -> ', response.fileSize);
                // console.log('type -> ', response.type);
                // console.log('fileName -> ', response.fileName);
                setFilePath(response);
            });
        }
    };

    const chooseFile = (type) => {
        let options = {
            mediaType: type,
            maxWidth: 300,
            maxHeight: 550,
            quality: 1,
        };
        if (reviewImageList.length == 2) {
            ToastAndroid.show("No more image allowed", ToastAndroid.LONG);
            return

        }

        console.log("Helloe");
        launchImageLibrary(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                // alert('User cancelled camera picker');
                return;
            } else if (response.errorCode == 'camera_unavailable') {
                // alert('Camera not available on device');
                return;
            } else if (response.errorCode == 'permission') {
                // alert('Permission not satisfied');
                return;
            } else if (response.errorCode == 'others') {
                // alert(response.errorMessage);
                return;
            }
            console.log('base64 -> ', response.base64);
            console.log('uri -> ', response.uri);
            console.log('width -> ', response.width);
            console.log('height -> ', response.height);
            console.log('fileSize -> ', response.fileSize);
            console.log('type -> ', response.type);
            console.log('fileName -> ', response.fileName);
            var image = [...reviewImageList];
            image.push(response);
            setReviewImageList(image);
        });
    };



    const reviewSubmitButton = async () => {
        var userJsonData = {};
        let formData = new FormData();

        if (reviewNumber == 0) {
            ToastAndroid.show("Rating is required", ToastAndroid.LONG)
            return true;
        } else {
            userJsonData.rating = reviewNumber;
        }
        if (reviewHeadingText.trim() == '' || reviewHeadingText.trim() == undefined || reviewHeadingText.trim() == null) {
            ToastAndroid.show("Review Headline text field required", ToastAndroid.LONG)
            return true;
        } else {
            userJsonData.isbn = book._source.isbn;
            userJsonData.heading = reviewHeadingText.trim();
            userJsonData.imageLength = reviewImageList.length
        }

        if (reviewText.trim() == '' || reviewText.trim() == undefined || reviewText.trim() == null) {
            ToastAndroid.show("Review text field required", ToastAndroid.LONG)
            return true;
        } else {
            userJsonData.description = reviewText.trim();
        }
        for (var i = 0; i < reviewImageList.length; i++) {
            formData.append('file' + i, { uri: reviewImageList[i].uri, type: reviewImageList[i].type, name: reviewImageList[i].fileName });
        }

        formData.append('data', JSON.stringify(userJsonData));

        console.log(formData);

        setIsLoading(true);
        var headers = {
            'Accept': 'application/json',
            'Content-Type': 'multipart/form-data',
            "x-access-token": JSON.parse(Constants.LogIn_User).token,
        }
        await fetch(Constants.API_URL + "/review/createNewReview", {
            method: "POST",
            headers: headers,
            body: formData,
        }).then(async (response) => await response.json())
            .then((responseJson) => {
                setIsLoading(false);
                if (responseJson.status == 1) {
                    props.navigation.navigate("Books");
                }
                ToastAndroid.show(responseJson.message, ToastAndroid.LONG)

                console.log(responseJson);
            })
            .catch((error) => {
                setIsLoading(false)
                ToastAndroid.show(error, ToastAndroid.LONG)
            });
    }

    const removeImage = (imageItem) => {
        var imageList = reviewImageList;
        var index = imageList.indexOf(imageItem)
        setIsLoading(true)
        if (index !== -1) {
            imageList.splice(index, 1);
            setRemoveSelectedImage(imageItem);
            setReviewImageList(imageList)
        }
        setIsLoading(false);
        console.log(reviewImageList);
    }
    return (
        <View style={{ flex: 1, backgroundColor: '#ffff' }}>
            <Header headerTitle={translations.ProductReview} backScreen={backScreen} navigation={props.navigation} />
            <Loader loading={isLoading} />
            <View style={{
                paddingVertical: responsiveWidth(2),
                paddingHorizontal: responsiveWidth(3),
                alignItems: 'center',
                flexDirection: 'row',
                backgroundColor: '#ffff',
                borderBottomWidth: responsiveWidth(0.1)
            }}>
                <View>
                    <Image source={CommanFunction.checkImageAvailability(book._source.thumbnail)} resizeMode={"center"}
                        style={{ width: responsiveWidth(16), height: responsiveWidth(22), borderColor: 'blue', borderWidth: responsiveWidth(0.1) }} ></Image>
                </View>
                <View style={{ paddingLeft: responsiveWidth(3) }}>
                    <View>
                        <Text style={{
                            fontSize: responsiveFontSize(1.8), color: 'black',
                            fontFamily: Fonts.Roboto.Roboto_Bold, textAlign: 'left'
                        }}>{book._source.skutitle.length > 45 ? book._source.skutitle.substr(0, 42) + "..." : book._source.skutitle}</Text>
                    </View>
                    <View style={{ width: responsiveWidth(40) }}>
                        <Rating
                            onStartRating={(ratingNUmber) => { console.log(ratingNUmber); }}
                            imageSize={responsiveWidth(7.5)}
                            startingValue={0}

                            ratingImage={Images.star}
                            onSwipeRating={(ratingNUmber) => { console.log(ratingNUmber); }}
                            ratingCount={5}
                            minValue={0}
                            onFinishRating={(ratingNUmber) => { console.log(ratingNUmber); setReviewNumber(ratingNUmber) }}
                            style={{ paddingVertical: responsiveWidth(2), paddingHorizontal: responsiveWidth(3) }}
                        />
                    </View>
                </View>
            </View>
            <View style={{ flex: 4, }}>


                <View style={{
                    flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around',
                    paddingHorizontal: responsiveWidth(1.6), paddingVertical: responsiveWidth(3)
                }}>
                    {reviewHeader.map((item, index) => (
                        item !== '' &&
                        <TouchableOpacity key={index} onPress={() => { setReviewHeaderTextSelected(item); setReviewHeadingText(item) }}>
                            <View style={[styles.headerView, {
                                width: responsiveWidth(23),
                                backgroundColor: reviewHeaderTextSelected !== null && reviewHeaderTextSelected == item ? 'green' : Colors.appColor,
                            }]}>
                                <Text style={styles.headerText}>{item}</Text>
                            </View>
                        </TouchableOpacity>


                    ))
                    }
                </View>






                {/* {reviewHeader !== '' && reviewHeader !== undefined && reviewHeader !== null &&
                    <View style={{
                        flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around',
                        paddingHorizontal: responsiveWidth(1.6), paddingVertical: responsiveWidth(3)
                    }}>
                        <TouchableOpacity onPress={() => { setReviewHeaderTextSelected(reviewHeader.key1); setReviewHeadingText(reviewHeader.key1) }}>
                            <View style={[styles.headerView, {
                                width: responsiveWidth(23),
                                backgroundColor: reviewHeaderTextSelected !== null && reviewHeaderTextSelected == reviewHeader.key1 ? 'green' : Colors.appColor,
                            }]}>
                                <Text style={styles.headerText}>{reviewHeader.key1 !== undefined ? reviewHeader.key1 : ''}</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { setReviewHeaderTextSelected(reviewHeader.key2); setReviewHeadingText(reviewHeader.key2) }}>
                            <View style={[styles.headerView, { backgroundColor: reviewHeaderTextSelected !== null && reviewHeaderTextSelected == reviewHeader.key2 ? 'green' : Colors.appColor, }]}>
                                <Text style={styles.headerText}>{reviewHeader.key2 !== undefined ? reviewHeader.key2 : ''}</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { setReviewHeaderTextSelected(reviewHeader.key3); setReviewHeadingText(reviewHeader.key3) }}>
                            <View style={[styles.headerView, { backgroundColor: reviewHeaderTextSelected !== null && reviewHeaderTextSelected == reviewHeader.key3 ? 'green' : Colors.appColor, }]}>
                                <Text style={styles.headerText}>{reviewHeader.key3 !== undefined ? reviewHeader.key3 : ''}</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { setReviewHeaderTextSelected(reviewHeader.key4); setReviewHeadingText(reviewHeader.key4) }}>
                            <View style={[styles.headerView, { backgroundColor: reviewHeaderTextSelected !== null && reviewHeaderTextSelected == reviewHeader.key4 ? 'green' : Colors.appColor, }]}>
                                <Text style={styles.headerText}>{reviewHeader.key4 !== undefined ? reviewHeader.key4 : ''}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                } */}


                <View style={{ marginTop: responsiveWidth(5), paddingHorizontal: responsiveWidth(6), }}>
                    <FloatingLabelInput
                        label={"Add a headline"}
                        value={reviewHeadingText}
                        staticLabel
                        hintTextColor={'#aaa'}
                        hint={"What's most important to know ?  * "}
                        isPassword={false}
                        keyboardType={"default"}
                        containerStyles={{
                            borderWidth: responsiveWidth(0.1),
                            paddingHorizontal: responsiveWidth(1),
                            backgroundColor: '#fff',
                            //'blue',
                            paddingTop: responsiveWidth(1),
                            borderRadius: responsiveWidth(1),
                            color: 'red',
                        }}
                        customLabelStyles={{
                            // colorFocused: 'red',
                            fontSizeFocused: responsiveFontSize(1.6),
                        }}
                        labelStyles={styles.label}
                        inputStyles={styles.input}
                        numberOfLines={2}
                        underlineColorAndroid="transparent"
                        multiline={true}
                        onChangeText={(value) => {
                            setReviewHeadingText(value)
                            if (value == '') {
                                setReviewHeaderTextSelected('');
                            }
                        }}
                    />
                </View>
                <View style={{ marginTop: responsiveWidth(9), paddingHorizontal: responsiveWidth(6), }}>
                    <FloatingLabelInput
                        label={"Add a written review"}
                        value={reviewText}
                        staticLabel
                        hintTextColor={'#aaa'}
                        hint={"What did you like or dislike? What did you use this product for?  *"}
                        isPassword={false}
                        keyboardType={"default"}
                        containerStyles={{
                            borderWidth: responsiveWidth(0.1),
                            paddingHorizontal: responsiveWidth(1),
                            backgroundColor: '#fff',
                            //'blue',
                            paddingTop: responsiveWidth(1),
                            borderRadius: responsiveWidth(1),
                            color: 'red',
                        }}
                        customLabelStyles={{
                            // colorFocused: 'red',
                            fontSizeFocused: responsiveFontSize(1.6),
                        }}
                        labelStyles={styles.label}
                        inputStyles={styles.input}
                        numberOfLines={10}
                        underlineColorAndroid="transparent"
                        multiline={true}
                        onChangeText={(value) => { setReviewText(value) }}
                    />
                </View>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', }}>
                {reviewImageList.length > 0 &&
                    <View style={{ flexDirection: 'row' }}>
                        {
                            reviewImageList.map((image, index) => (
                                <View key={index}>
                                    <View key={index} style={{ paddingLeft: responsiveWidth(5) }}>
                                        <Image source={{ uri: image.uri }} style={{
                                            width: responsiveWidth(16), borderRadius: responsiveWidth(1),
                                            height: responsiveWidth(16)
                                        }}></Image>
                                    </View>
                                    <TouchableOpacity style={{
                                        flex: 1,
                                        backgroundColor: 'black', position: 'absolute',
                                        top: 0, left: responsiveWidth(15),  //index == 0 ? responsiveWidth(15) : 
                                        bottom: responsiveWidth(8),
                                        borderRadius: responsiveWidth(3), justifyContent: 'center',
                                        width: responsiveWidth(5), height: responsiveWidth(5)
                                    }} onPress={() => { removeImage(image) }}>
                                        <View>
                                            <Text style={{
                                                textAlign: 'center',
                                                color: '#ffff',
                                                fontSize: responsiveFontSize(1.6)
                                            }}>X</Text>

                                        </View>
                                    </TouchableOpacity>
                                </View>
                            ))
                        }
                    </View>
                }
            </View>


            <View style={{ flex: 0.6, borderWidth: responsiveWidth(0.1), flexDirection: 'row', paddingHorizontal: responsiveWidth(4), justifyContent: 'space-between' }}>
                <TouchableOpacity onPress={() => chooseFile('photo')} style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View>
                            <MaterialCommunityIcons name={"camera-plus"} size={responsiveWidth(8)} color={"grey"}></MaterialCommunityIcons>
                        </View>
                        <View >
                            <Text style={{ fontFamily: Fonts.Roboto.Roboto_Black, fontSize: responsiveFontSize(1.5), color: 'black', paddingLeft: responsiveWidth(2), textAlign: 'justify' }}>ADD IMAGE</Text>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { reviewSubmitButton() }} style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                        <View style={{
                            backgroundColor: '#ffc107',
                            borderRadius: responsiveWidth(0.7)
                        }}>
                            <Text style={[styles.bookText, {
                                color: "#ffff",
                                paddingHorizontal: responsiveWidth(5),
                                textAlign: 'justify',
                                paddingVertical: responsiveWidth(2),
                                fontFamily: Fonts.Roboto.Roboto_Black, fontSize: responsiveFontSize(1.5),
                            }]}>SUBMIT </Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>


        </View >

    )
}

const styles = StyleSheet.create({
    headerView: {
        justifyContent: 'center',
        alignItems: 'center',
        width: responsiveWidth(22), borderRadius: responsiveWidth(1.3)
    },
    headerText: {
        fontSize: responsiveFontSize(1), padding: responsiveWidth(1), paddingVertical: responsiveWidth(1.6),
        fontFamily: Fonts.Roboto.Roboto_Bold,
        color: '#ffff',
    },
    textAreaContainer: {
        borderColor: 'grey',
        paddingHorizontal: responsiveWidth(3),
        paddingVertical: responsiveWidth(1),
        flex: 8,
    },
    textArea: {
        justifyContent: "flex-start",
        textAlignVertical: 'top',
    },

    label: {
        backgroundColor: '#fff',
        paddingHorizontal: responsiveWidth(2),
    },
    input: {
        color: Colors.appColor, //'blue',
        paddingHorizontal: responsiveWidth(1),
        textAlignVertical: 'top',

    }
})
export default ReviewProduct;
