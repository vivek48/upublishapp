import React, { Component, useContext, useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ScrollView, Image, ActivityIndicator } from 'react-native';
import Header from "../../component/CustomInput/Header";
import { LocalizationContext } from '../../services/localization/LocalizationContext';
import { HardCodeData } from '../../hardcodedata/LanguageList'
import { Colors, Fonts, Images } from "../../styles/Index";
import { UserContext } from '../../services/loginuser/UserContext'
import { responsiveFontSize, responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import ContactUs from '../../component/CustomInput/ContactUs';
import Language from '../../component/CustomInput/Language';
import PrivacyPolicies from '../../component/CustomInput/PrivacyPolicies';
import ReturnCancellation from '../../component/CustomInput/ReturnCancellation';
import WebView from 'react-native-webview'
import { ScreenClass } from '../../component/comman/ScreenClass'
import { ScreenName } from '../../component/comman/ScreenName';
import { FirebaseConfig } from '../../component/comman/FirebaseConfig';
import { ClickedButtonName } from '../../component/comman/ClickedButtonName';

import Constant from '../../component/comman/Constant';
const SettingScreen = (props) => {
    const { appLanguage, initializeAppLanguage, setAppLanguage, translations } = useContext(LocalizationContext);
    // const [languageList, setLanguageList] = useState([]);

    const { setUserLogin, userLogin } = useContext(UserContext)
    const [privacyUri, setPrivacyUri] = useState('https://shop.reprobooks.in/privacy-policy');

    const [settingsList, setSettingsList] = useState([{ label: translations.language, index: 1, }, { label: translations.Privacy, index: 2 },
    { label: translations.ReturnCancellation, index: 3 }, { label: translations.ContactUs, index: 4, }]);
    const [selectedIndex, setSelectedIndex] = useState(-1);
    const [privacy, setPrivacy] = useState(false);
    const logOutUser = () => {
        setUserLogin(null);
    }
    const closeButton = () => {
        setSelectedIndex(-1);
    }
    const closeExistsBackScreen = () => {
        if (!privacy) {
            props.navigation.navigate("Books")
        } else {
            setPrivacy(false)
        }
    }
    const privacyPageOpen = (value) => {
        setPrivacy(value);
    }

    const IndicatorLoadingView = () => {
        return (
            <ActivityIndicator
                color={Colors.appColor}
                size="large"
                style={styles.IndicatorStyle}
            />
        );
    }

    useEffect(() => {
        FirebaseConfig.realtimeScreenLogContent(ScreenClass.Settings, ScreenName.Settings);
    })
    return (
        <View style={{ flex: 1 }}>
            <View>
                <Header headerTitle={translations.Setting} backScreen={closeExistsBackScreen} navigation={props.navigation} />
            </View>
            {!privacy && <ScrollView>
                <View>
                    {settingsList.map((item, index) => (
                        <View key={index}>
                            <View key={index} style={[styles.listHeader, { borderBottomWidth: responsiveWidth(0.1) }]}>
                                <TouchableOpacity activeOpacity={0.8} onPress={() => {
                                    FirebaseConfig.realtimeClickedButton(item.label.replace(" ", "_").replace(" & ", "_").replace(" ", "_").replace(" ", "_"))

                                    if (selectedIndex == index) {
                                        setSelectedIndex(-1)
                                    } else {
                                        setSelectedIndex(index);
                                    }
                                }}>
                                    <View style={styles.listHeaderView}>
                                        <View style={{ width: responsiveWidth(88.3) }}>
                                            <Text style={styles.listHeaderText}>{item.label}</Text>
                                        </View>
                                        <View>
                                            <MaterialIcons size={responsiveWidth(7)} name={selectedIndex == index ? 'keyboard-arrow-down' : 'keyboard-arrow-right'}></MaterialIcons>
                                        </View>
                                    </View>

                                </TouchableOpacity>
                            </View>
                            {selectedIndex == index && index == 0 &&
                                <View style={{ borderBottomWidth: responsiveWidth(0.1), }}>
                                    <View style={{ marginHorizontal: responsiveWidth(3.7), }}>
                                        <Language />
                                    </View>
                                </View>
                            }
                            {selectedIndex == index && index == 1 &&
                                <View style={{ borderBottomWidth: responsiveWidth(0.1), }}>
                                    <View style={{ marginHorizontal: responsiveWidth(3), }}>
                                        <PrivacyPolicies privacyPageOpen={privacyPageOpen} props={props} />
                                    </View>
                                </View>
                            }
                            {selectedIndex == index && index == 2 &&
                                <View style={{ borderBottomWidth: responsiveWidth(0.1), }}>
                                    <View style={{ marginHorizontal: responsiveWidth(3), }}>
                                        <ReturnCancellation />
                                    </View>
                                </View>
                            }
                            {selectedIndex == index && index == 3 &&
                                <View style={{ borderBottomWidth: responsiveWidth(0.1), }}>
                                    <View style={{ marginHorizontal: responsiveWidth(3), }}>
                                        <ContactUs props={props} closeButton={closeButton} />
                                    </View>
                                </View>
                            }
                        </View>
                    ))}
                    <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => {
                        FirebaseConfig.realtimeClickedButton(ClickedButtonName.LogOutButton);
                        logOutUser()
                    }}>
                        <View style={styles.logOutButton}>
                            <Text style={styles.logoutButtonText}>{translations.LogoutButton}</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={{ fontSize: responsiveFontSize(1.2), fontFamily: Fonts.Roboto.Roboto_Medium }}>{translations.BuildNumber} </Text>
                    </View>
                </View>
            </ScrollView>
            }
            {privacy &&
                <View style={{ flex: 1, height: responsiveHeight(90) }}>
                    <WebView source={{ uri: privacyUri }}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        renderLoading={IndicatorLoadingView}
                        startInLoadingState={true}
                    />
                </View>
            }
        </View>
    )
}
const styles = StyleSheet.create({
    flex: 1,
    listHeader: {
        alignContent: 'center', justifyContent: 'center',
        paddingHorizontal: responsiveWidth(3)
    },
    listHeaderView: {
        justifyContent: 'space-between',
        alignItems: 'center', alignContent: 'center',
        paddingVertical: responsiveWidth(1),
        flexDirection: 'row',
        height: responsiveHeight(8),
    },
    listHeaderTextSites: {
        opacity: 0.7,
        fontSize: responsiveFontSize(1.6),
        fontFamily: Fonts.Roboto.Roboto_Medium,
    },
    logOutButton: {
        borderColor: 'rgba(220, 220, 222, 1)'
        , borderWidth: responsiveWidth(0.2), borderRadius: responsiveWidth(2),
        justifyContent: 'center', alignItems: 'center', height: responsiveHeight(6),
        marginVertical: responsiveWidth(4), marginHorizontal: responsiveWidth(4),
        backgroundColor: '#ffc107',
    },
    IndicatorStyle: {
        position: "absolute",
        alignItems: "center",
        justifyContent: "center",
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    },
    logoutButtonText: { color: '#fff', fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(2) }
})



export default SettingScreen;
