import React, { Component } from 'react';
import { View, Text, Image, ImageBackground } from 'react-native'
import { UStyle, Images, Colors } from '../../styles/Index'
import Language from '../../component/Language'
class LanguageScreen extends Component {
    constructor() {
        super()
        this.state = {
        }
    }
    render() {
        return (
            <View>
                <ImageBackground source={Images.login_bg} style={UStyle.styles.imageBackGound}>
                    <View style={UStyle.styles.outerView}>
                        <Text style={UStyle.styles.languageText}> Please choose app language </Text>
                        <Image source={Images.world} tintColor={Colors.appColor} style={UStyle.styles.lanImage}></Image>
                    </View>
                    <View style={UStyle.styles.imageBook}>
                        <Image source={Images.repro_logo1} resizeMode={'center'}></Image>
                    </View>
                    <Language></Language>
                </ImageBackground>
            </View >
        )
    }
}

export default LanguageScreen;