import React, { Component, useContext, useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, TextInput, Image, TouchableOpacity, Platform, Linking, ActivityIndicator } from 'react-native';
import Header from "../../component/CustomInput/Header";
import { LocalizationContext } from '../../services/localization/LocalizationContext';
import Loader from "../../component/comman/Loader";
import { Colors, Fonts, Images } from '../../styles/Index'
import { ApiCall } from '../../component/comman/ApiCall'
import ShopBookRender from "../../component/CustomInput/ShopBookRender";
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import SearchRender from '../../component/CustomInput/SearchRender';
import { ScrollView } from 'react-native-gesture-handler';

import Constants from '../../component/comman/Constant';
import { HardCodeData } from '../../hardcodedata/LanguageList'
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import { CommanFunction } from '../../component/comman/CommanFunction';
import { CartItemsContext } from '../../services/cartitems/CartItemsContext'
import { ScreenClass } from '../../component/comman/ScreenClass'
import { ScreenName } from '../../component/comman/ScreenName';
import { FirebaseConfig } from '../../component/comman/FirebaseConfig';
import getSymbolFromCurrency from 'currency-symbol-map'
// import PushNotification from 'react-native-push-notification';
import { CurrencyContext } from '../../services/currency/CurrencyContext'
var topSellingData = [];
class HomeScreen extends Component {

    static contextType = CartItemsContext

    constructor(props) {
        super(props);
        this.state = {
            totalBooks: 0,
            isLoading: false,
            searchText: '',
            userID: JSON.parse(Constants.LogIn_User),
            currenIndex: 0,
            selectTextTop: 'Popular',
            bookShopList: [],
            recievedTotalBooks: 0,
            scrollEndValue: '',
            linkingURL: false,
            loading: false,
            topItem: [
                {
                    key: 1,
                    value: 'Popular',
                },
                {
                    key: 2,
                    value: 'Trending',
                },
                {
                    key: 3,
                    value: 'New',
                },
                {
                    key: 4,
                    value: 'Genres',
                },
            ]
        }
    }


    componentDidMount() {
        // const {userCartgBook,setUserCartItemst}=this.contextType
        // PushNotification.getChannels(function (channel_ids) {
        //     console.log(channel_ids); // ['channel_id_1']
        // });
        // PushNotification.configure({
        //     onNotification: (notification) => {
        //         if (notification.foreground) {
        //             if (notification.userInteraction) {
        //                 console.log('NOTIFICATION touched:', notification);
        //                 if (notification.data.screen == "OrderDetails") {
        //                     Constants.Notification = "OrderDetails";
        //                     this.navigateOrderDetail(notification.data.order_ID);
        //                 }
        //             }
        //             else {
        //                 console.log('NOTIFICATION foreground userInteraction:', notification.userInteraction);

        //             }
        //         }
        //         else {
        //             if (notification.userInteraction) {
        //                 console.log('NOTIFICATION touched:', notification);
        //                 if (notification.data.screen == "OrderDetails") {
        //                     Constants.Notification = "OrderDetails";
        //                     this.navigateOrderDetail(notification.data.order_ID);
        //                 }

        //             }
        //             else {
        //                 console.log('NOTIFICATION userInteraction:', notification.userInteraction);

        //             }
        //         }

        //     },
        //     // Android only: GCM or FCM Sender ID
        //     senderID: '599482376734',
        //     popInitialNotification: true,
        //     requestPermissions: true
        // });
        //        SplashScreen.hide();
     //   this.getSelectedUserCurrency(this.state.userID.user.userId);
        FirebaseConfig.realtimeScreenLogContent(ScreenClass.Books, ScreenName.Books)
        if (this.state.bookShopList.length == 0) {
            this.getTopSelling();
        }
        // console.log(SocketConnection.socketConnection());
        if (Platform.OS === 'android') {
            Linking.getInitialURL().then(url => {
                if (url !== null) {
                    if (Constants.LinkingURL == null) {
                        Constants.LinkingURL = url;
                        var splitUrl = url.split("/");
                        var isbn = splitUrl[splitUrl.length - 1];
                        this.byISBN(isbn)
                     //   this.setState({ linkingURL: url })
                        // setLinkingURL(url)
                    }
                    else {
                        this.setState({ linkingURL: true })
                        // setLinkingURL(true)
                    }
                } else {
                    this.setState({ linkingURL: null })
                    // setLinkingURL(null)
                }
            });
        }

    }

    navigateOrderDetail = (order_no) => {
        var userData = {};
        var order = {};
        order.orderno = order_no;
        userData.order = order;
        this.setState({ isLoading: true })
        // setIsLoading(true);
        console.log(userData);
        ApiCall.Api_Call('/order/fetchOrderDetails', userData, 'POST', true).then((result) => {

            this.setState({ isLoading: false })
            // console.log(result);
            if (result.status == 1) {
                this.props.navigation.navigate(translations.Orders, { screen: "OrderDetail", params: { orderItem: result.result } });
            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {
            this.setState({ isLoading: false })
            console.log(error);
        });
    }
    getSelectedUserCurrency = (user) => {
        this.setState({ isLoading: true })
        var userData = {};
        userData.userId = this.state.userID.user.userId
        ApiCall.Api_Call('/currency/getUserCurrency', userData, 'POST', true).then((result) => {
            if (result.status == 1) {
                console.log(result);
                if (result.result != null) {
                    <CurrencyContext.Consumer>
                        {currencyData => (
                            currencyData.setUserCurreny(result.result.currencyCode),
                            currencyData.setUserCurrenyValue(result.result.currencyValue)

                        )}
                    </CurrencyContext.Consumer>
                    // setUserCurreny(result.result.currencyCode);
                    // setUserCurrenyValue(result.result.currencyValue);
                }

            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {
            this.setState({ isLoading: false })
            console.log(error);
        });

    }

    byISBN = (ISBN) => {
        var userData = {}
        userData.isbn = ISBN;

        this.setState({ isLoading: true })
        ApiCall.Api_Call('/book/byISBN', userData, 'POST', true).then((result) => {
            this.setState({ isLoading: false })
            // console.log(result);
            if (result.status == 1) {
                if (result.result.length > 0) {
                    var bookData = result.result[0];
                    bookData.quantity = 0;
                    this.props.navigation.navigate('BookDetails', { bookItem: bookData, routeName: 'Books' })
                }
            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {
            this.setState({ isLoading: false })
            console.log(error);
        });

    }

    getBookShop = (userData) => {
        if (userData.startFrom == 0) {

            this.setState({ isLoading: true, bookShopList: [] })
        } else {
            this.setState({ loading: true })
        }
        ApiCall.Api_Call('/shop/advancedSearch', userData, 'POST', true).then((result) => {
            this.setState({ isLoading: false })

            console.log(result);
            if (result.status == 1) {
                let countdata = this.state.recievedTotalBooks;
                Constants.ServerImaagePath = result.result.imageurls;
                let data = countdata == 0 ? [] : [...this.state.bookShopList];
                if (result.result.records.length !== 0) {
                    var splitData;
                    if (userData.startFrom == 0 && topSellingData.length > 0) {
                        splitData = CommanFunction.spliArray(topSellingData, "topsellingbook");
                        //  topSellingData = topSellingData.concat(result.result.records);
                        splitData = splitData.concat(CommanFunction.spliArray(result.result.records, ""));
                    } else {
                        splitData = CommanFunction.spliArray(result.result.records, "");
                    }
                    // var splitData = CommanFunction.spliArray(result.result.records);
                    var concatData = data.concat(splitData);
                    var Addcountdata = countdata + result.result.records.length;
                    this.setState({
                        totalBooks: result.result.count, recievedTotalBooks: Addcountdata,
                        bookShopList: concatData, scrollEndValue: '', loading: false, isLoading: false
                    })
                } else if (result.result.count == '0') {
                    this.setState({
                        scrollEndValue: 'No more book available', loading: false, isLoading: false
                    })

                } else {
                    this.setState({
                        scrollEndValue: 'No more book available', loading: false, isLoading: false
                    })
                }
            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {
            this.setState({
                bookShopList: [], loading: false, isLoading: false
            })
        });
    }

    searchBookData = (searchText, type) => {
        //    console.log("Search Book-----", searchText);
        if (type == 'APICALL') {
            console.log("API CALL Search Book-----", searchText);
            this.setState({ totalBooks: 0, recievedTotalBooks: 0 }, () => {
                var userData = {};
                var filters = {};
                filters.keyword = searchText;
                userData.filters = filters;
                userData.startFrom = 0
                this.getBookShop(userData)
            })
        }
        this.setState({ searchText: searchText })
    }
    getTopSelling = () => {
        this.setState({ isLoading: true })
        var userData = {};
        userData.fromDate = '',
            userData.toDate = '',
            ApiCall.Api_Call('/shop/topSelling', userData, 'POST', true).then((result) => {
                this.setState({ isLoading: false })
                if (result.status == 1) {
                    if (result.result.books.length !== 0) {

                        topSellingData = result.result.books;
                        this.searchBookData('', "APICALL")
                    } else {
                        this.searchBookData('', "APICALL")
                    }
                    //   searchBookData('', "APICALL");
                    //  console.log(result.result.books);

                } else if (result.status == 0) {
                    this.searchBookData('', "APICALL")

                } else {

                    this.searchBookData('', "APICALL")
                }
            }).catch((error) => {
                this.searchBookData('', "APICALL")
                this.setState({ isLoading: true })
                console.log(error);
            });

    }

    renderItemTopHeader = ({ item, index }) => (
        <TouchableOpacity key={index} activeOpacity={Colors.activeOpecity} onPress={() => {
            this.setState({ currenIndex: index, selectTextTop: item.value })
        }}>
            <View style={styles.topScrollView}>
                <Text style={[styles.scrollItem, {
                    borderBottomColor: this.state.currenIndex == index ? '#5F8FFF' : '#fff',
                    color: this.state.currenIndex == index ? '#5F8FFF' : 'grey',
                    borderBottomWidth: this.state.currenIndex == index ? responsiveWidth(1) : responsiveWidth(0),
                }]}>{item.value}

                    <View>

                    </View>

                </Text>
            </View>
        </TouchableOpacity>
    );


    BookListItem = ({ item, index }) => (
        <View key={index} >
            <View style={styles.bookOuterView}>
                <Text style={[styles.bookOuterText, { textAlign: 'center' }]}>
                    {(item.type == 'topsellingbook' ? 'Top Selling ' : this.state.selectTextTop) + " " + item.value.split(" ")[1] + " " + item.value.split(" ")[2] + " " + item.value.split(" ")[3]}
                </Text>
            </View>
            <View />
            <ScrollView horizontal={true} overScrollMode={'never'}

                showsHorizontalScrollIndicator={false} >
                <View style={{ paddingHorizontal: responsiveWidth(6), flexDirection: 'row' }}>
                    {item.freeEbook.map((book, key) => (
                        <TouchableOpacity key={key} activeOpacity={Colors.activeOpecity} onPress={

                            () => { this.checkCartItem(book) }}
                            style={{}}
                        >
                            <View style={{ paddingHorizontal: responsiveWidth(2), alignItems: 'center', }}>
                                <View style={styles.imageView}>
                                    <Image source={CommanFunction.checkImageAvailability(book._source.thumbnail, book._source.isbn)}
                                        style={styles.imageStyle}></Image>
                                </View>
                                <View style={{
                                    width: responsiveWidth(24),
                                    paddingTop: responsiveWidth(1),
                                    flexWrap: 'wrap',
                                }}>
                                    <View>
                                        <Text style={{
                                            paddingVertical: responsiveWidth(0.5),
                                            fontSize: responsiveFontSize(1.4),
                                            fontFamily: Fonts.Awesome.FontAwesome,
                                            textAlign: 'center',
                                            lineHeight: responsiveWidth(3),
                                        }}> {book._source.skutitle.trim().length > 30 ? book._source.skutitle.substr(0, 22).trim() + "..." : book._source.skutitle}
                                        </Text>
                                    </View>
                                </View>
                                <CurrencyContext.Consumer>
                                    {currencyData => (
                                        <View style={{
                                            justifyContent: 'space-between',
                                            flexDirection: 'row', alignItems: 'center',
                                        }} >
                                            {book._source.sp !== book._source.rrp &&
                                                <Text style={[styles.booktext, { textDecorationStyle: 'solid', textDecorationLine: 'line-through', }]} >
                                                    {book._source.currency}{" " + getSymbolFromCurrency(currencyData.userCurrency) + " " + book._source.rrp}
                                                </Text>
                                            }
                                            <Text style={styles.booktext} >
                                                {book._source.currency !== currencyData.userCurrency && currencyData.userCurrency !== '' && currencyData.userCurrency && currencyData.userCurrency !== null ?
                                                    currencyData.userCurrency : book._source.currency}{book._source.currency !== currencyData.userCurrency && currencyData.userCurrency !== '' && currencyData.userCurrency &&
                                                        currencyData.userCurrency !== null ?
                                                        " " + getSymbolFromCurrency(currencyData.userCurrency) + " " + CommanFunction.currencyChangeValue(book._source.sp, currencyData.userCurrencyValue) : " " + getSymbolFromCurrency(currencyData.userCurrency) + " " + book._source.sp}
                                                {/* {book._source.currency}{" " + book._source.sp} */}
                                                {/* {book._source.currency}{" " + book._source.sp} */}
                                            </Text>
                                        </View>
                                    )}

                                </CurrencyContext.Consumer>
                            </View>
                        </TouchableOpacity>
                    ))}
                </View>
            </ScrollView>
        </View>
    )

    checkCartItem = (book) => {
        //  console.log("--------------", book);
        // const userCard=this.contextType.;
        const { setUserCartItems, userCartgBook } = this.context
        //  console.log(this.context);
        // var userCartgBook=[];
        // <CartItemsContext.Consumer>
        //     {cartData => (
        //         console.log(cartData.userCartgBook.length),
        //         userCartgBook.push(cartData.userCartgBook)
        //     )}
        // </CartItemsContext.Consumer>
        FirebaseConfig.realtimeClickedButton(book.contentid);
        var checkValueFind = false;
        // console.log("Cart Data", userCartgBook);

        for (var i = 0; i < userCartgBook.length; i++) {
            if (book._id == userCartgBook[i].itemID) {
                console.log(book._id == userCartgBook[i].itemID);
                console.log(userCartgBook[i].quantity);
                book.eBookQty = userCartgBook[i].eBookQty;
                book.paperbackQty = userCartgBook[i].paperbackQty;
                this.props.navigation.navigate('BookDetails', { bookItem: book, routeName: 'Books' })
                checkValueFind = true
                break;
            }
        }
        if (checkValueFind) {
            this.props.navigation.navigate('BookDetails', { bookItem: book, routeName: 'Books' })
        } else {
            book.eBookQty = 0;
            book.paperbackQty = 0;
            this.props.navigation.navigate('BookDetails', { bookItem: book, routeName: 'Books' })
        }
    }





    render() {
        const { bookShopList, currenIndex, isLoading, linkingURL, loading, recievedTotalBooks, scrollEndValue, searchText, selectTextTop, topItem, totalBooks, userID } = this.state
        return (
            <LocalizationContext.Consumer>
                {localizationData => (
                    <View style={[styles.outerView, {
                        marginBottom: responsiveWidth(25),
                        paddingBottom: bookShopList.length !== 0 ? responsiveWidth(29) : responsiveWidth(2)
                    }]}>
                        <Loader loading={isLoading} />
                        <CartItemsContext.Consumer>
                            {cartItemData => (
                                <SearchRender searchBook={this.searchBookData} placeHolder={localizationData.translations.BooksSearch} cartInBookCount={cartItemData.userCartgBook.length} props={this.props} searchTextValue={searchText} />

                            )
                            }
                        </CartItemsContext.Consumer>
                        {   this.state.linkingURL !== false &&
                            <>
                                <View style={styles.searchOuterView}>
                                    <FlatList
                                        data={topItem}
                                        nestedScrollEnabled={true}
                                        showsHorizontalScrollIndicator={false}
                                        horizontal={true}
                                        renderItem={this.renderItemTopHeader}
                                        keyExtractor={item => item.key.toString()}
                                    />
                                </View>
                                {
                                    this.state.totalBooks == 0 && this.state.totalBooks == this.state.recievedTotalBooks && !this.state.isLoading && this.state.currenIndex !== 1 ?
                                        <View style={{
                                            borderTopWidth: responsiveWidth(0.1),
                                            width: responsiveWidth(100), backgroundColor: '#fff',
                                            alignContent: 'center', alignItems: 'center',
                                            justifyContent: 'center',
                                        }}>
                                            <Text style={{
                                                color: '#27baba',
                                                paddingVertical: responsiveWidth(1),
                                                fontFamily: Fonts.Roboto.Roboto_Bold,
                                                fontSize: responsiveFontSize(1.4)
                                            }}>{localizationData.translations.SearchBookList}</Text>
                                        </View>
                                        : null
                                }
                                <View>
                                    <FlatList
                                        data={bookShopList}
                                        nestedScrollEnabled={true}
                                        renderItem={this.BookListItem}
                                        keyExtractor={item => item.key.toString()}
                                        onEndReached={() => console.log("end SCroll")}
                                        onEndReached={this.handleLoadMoreData}
                                        onEndReachedThreshold={400}
                                        ListFooterComponent={this.renderFooter}
                                        extraData={bookShopList}
                                    // refreshing={true}
                                    //onRefresh={bookShopList}
                                    />

                                </View>
                            </>
                        }
                    </View>
                )}
            </LocalizationContext.Consumer>

        )
    }


    renderFooter = () => {
        return (
            <>
                {this.state.loading ?
                    <View style={styles.loader}>
                        <ActivityIndicator size={'large'} color={'#27baba'} ></ActivityIndicator>
                    </View> : null}
                {
                    this.state.totalBooks !== 0 && this.state.totalBooks == this.state.recievedTotalBooks && !this.state.loading ?
                        <View style={{
                            borderTopWidth: responsiveWidth(0.1),
                            width: responsiveWidth(100), backgroundColor: '#fff',
                            alignContent: 'center', alignItems: 'center', justifyContent: 'center',
                            marginBottom: responsiveWidth(1)
                        }}>
                            {/* <Text style={{
                                color: '#27baba',
                                paddingVertical: responsiveWidth(1),
                                fontFamily: Fonts.Roboto.Roboto_Bold,
                                fontSize: responsiveFontSize(1.4)
                            }}>No more books available</Text> */}
                        </View>
                        : null
                }
            </>
        )
    }
    handleLoadMoreData = () => {
        // console.log(recievedTotalBooks, "this.state.totalNews" + totalBooks);
        if (this.state.totalBooks !== this.state.recievedTotalBooks && !this.state.loading && this.state.scrollEndValue == '' && this.state.currenIndex !== 1) {
            //  console.log("API Call");
            var userData = {};
            var filters = {};
            filters.keyword = this.state.searchText;
            // filters.startFrom = recievedTotalBooks;
            userData.filters = filters;
            userData.startFrom = this.state.recievedTotalBooks
            this.getBookShop(userData);
        } else {
            console.log("API NOT Call");
        }
    }
}

const styles = StyleSheet.create({
    outerView: {
        paddingTop: responsiveWidth(3),
        backgroundColor: '#ffffff',

    },
    searchOuterView: {
        marginTop: responsiveWidth(1),
        paddingHorizontal: responsiveWidth(2),
        borderBottomWidth: responsiveWidth(0.2),
        borderBottomColor: 'lightgrey',


    },
    topScrollView: {
        justifyContent: 'center', alignItems: 'center',
        paddingHorizontal: responsiveWidth(5),

    },
    scrollItem: {
        fontFamily: Fonts.Roboto.Roboto_Medium,
        paddingVertical: responsiveWidth(3),
    },
    bookOuterView: {
        // paddingVertical: responsiveWidth(6),
        paddingVertical: responsiveWidth(4),
        paddingHorizontal: responsiveWidth(7),
        justifyContent: 'center'
    },
    bookOuterText: {
        fontFamily: Fonts.Roboto.Roboto_Medium,
        fontSize: responsiveFontSize(2),
    },
    imageView: {
        justifyContent: 'center',
        alignItems: 'center',
        opacity: 0.8,
        borderRadius: responsiveWidth(2),
        backgroundColor: '#ffff',// '#d9d9d9',
        shadowColor: '#30C1DD',
        shadowRadius: 10,
        shadowOpacity: 0.6,
        elevation: 8,
        width: responsiveWidth(25),
        height: responsiveHeight(20),
        shadowOffset: { width: 0, height: 4 },
    },
    imageStyle: {
        width: responsiveWidth(23),
        height: responsiveHeight(19),
        borderWidth: responsiveWidth(0.2),
        // borderColor: 'red',
    },
    booktext: {
        //paddingLeft: responsiveWidth(1), fontSize: responsiveFontSize(1.2), color: "grey"
        textAlign: 'center', letterSpacing: responsiveWidth(0.1),
        fontFamily: Fonts.Awesome.FontAwesome5_Brands,
        color: 'grey', fontSize: responsiveFontSize(1.0),
    }
})


export default HomeScreen;