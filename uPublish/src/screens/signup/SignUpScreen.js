import React, { Component, useContext, useState, useEffect } from 'react';
import { View, Text, Image, ImageBackground, StyleSheet, TouchableOpacity } from 'react-native'
import { UStyle, Images, Fonts, Colors } from '../../styles/Index'
import { LocalizationContext } from '../../services/localization/LocalizationContext'
import { UserContext } from '../../services/loginuser/UserContext'
import { responsiveFontSize, responsiveWidth } from 'react-native-responsive-dimensions';
import CustomInput from '../../component/CustomInput/CustomInput'
import CustomButton from '../../component/CustomInput/CustomButton';
import Constants from "../../component/comman/Constant";
import auth from '@react-native-firebase/auth';
import Loader from '../../component/comman/Loader'
import { ScrollView } from 'react-native-gesture-handler';
import { ApiCall } from '../../component/comman/ApiCall';
import { ScreenClass } from '../../component/comman/ScreenClass'
import { ScreenName } from '../../component/comman/ScreenName';
import { ClickedButtonName } from '../../component/comman/ClickedButtonName';
import { FirebaseConfig } from '../../component/comman/FirebaseConfig'
import { CommanFunction } from '../../component/comman/CommanFunction';
const SignUpScreen = (props) => {
    const { appLanguage, initializeAppLanguage, setAppLanguage, translations } = useContext(LocalizationContext);

    const [name, setName] = useState('');
    const [emailID, setEMailID] = useState('');
    const [contactNumber, setContactNumber] = useState('');
    const [password, setPassword] = useState('');
    const [nameError, setNameError] = useState('');
    const [emailIDError, setEMailIDError] = useState('');
    const [contactNumberError, setContactNumberError] = useState('');
    const [passwordError, setPasswordError] = useState('');
    const [loginUserNotExist, setLoginUserNotExist] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    useEffect(() => {
        FirebaseConfig.realtimeScreenLogContent(ScreenClass.SignUP, ScreenName.SignUP)
    }, []);

    const signUpUser = () => {
        FirebaseConfig.realtimeScreenLogContent(ClickedButtonName.SignUpSaveButtonTitle)
        var userData = {}
        // if (name.trim() == '' || name.trim() == undefined || name.trim() == null) {
        //     setNameError(true);
        //     return true;
        // } else {
       // console.log("Name check" + CommanFunction.CheckName(name));
        if (CommanFunction.CheckName(name)) {
            userData.UserName = name.trim();
            setNameError(false);
        } else {
            setNameError(true);
            return true;
        }
        // }
        // if (emailID.trim() == '' || emailID.trim() == undefined || emailID.trim() == null) {
        //     setEMailIDError(true);
        //     return true;
        // } else {
        // if(CommanFunction.ValidateEmail(emailID))

        if (CommanFunction.ValidateEmail(emailID.trim())) {
            userData.UserEmail = emailID.trim();
            setEMailIDError(false);
        } else {
            setEMailIDError(true);
            return true;
        }
        // }

        // if (contactNumber.trim() == '' || contactNumber.trim() == undefined || contactNumber.trim() == null) {
        //     setContactNumberError(true);
        //     return true;
        // } else {
      //  console.log("Phone Number :" + CommanFunction.CheckPhoneNumber(contactNumber));
        if (CommanFunction.CheckPhoneNumber(contactNumber.trim())) {
            userData.UserContactNo = contactNumber.trim();
            setContactNumberError(false);
        } else {
            setContactNumberError(true);
            return true;
        }
        // }
        // if (password.trim() == '' || password.trim() == undefined || password.trim() == null ||
        //     passwordError) {
        //     setPasswordError(true)
        //     return true;
        // } else {
        console.log("Password Number :" + CommanFunction.CheckPassword(password));

        if (CommanFunction.CheckPassword(password)) {
            userData.UserPassword = password.trim()
            setPasswordError(false)
        } else {
            setPasswordError(true)
            return true;
        }
        // }
        setIsLoading(true);
        ApiCall.Api_Call('/auth/signup', userData, 'POST', false).then((result) => {
            setIsLoading(false);
            console.log(result);
            if (result.status == 1) {
                // props.navigation.navigate('LogIn');
                // setLoginUserNotExist(false)
                props.navigation.navigate("VerifyOTPScreen", {
                    mobileNo: contactNumber, userName: name.trim(),
                    emailId: emailID.trim(), mobileOTPVerify: false, emailOTPVerify: false,
                });
            } else if (result.status == 0) {
                setLoginUserNotExist(true)
            } else {
            }
        }).catch((error) => {
            console.log(error);
        });
    }

    const setUserName = (value) => {
        setNameError(false);
        setName(value);
    }
    const setUserEmailID = (value) => {
        setEMailIDError(false);
        setEMailID(value);
    }

    const setContactNo = (value) => {
        setContactNumberError(false);
        setContactNumber(value);
    }

    const setUserPassword = (value) => {
        setPasswordError(false)
        setPassword(value);
    }



    return (
        <View>
            <ScrollView>
                <Loader loading={isLoading} />
                <ImageBackground source={Images.login_bg} style={UStyle.styles.imageBackGound}>
                    <View style={{ flexDirection: 'row', marginTop: responsiveWidth(4) }}>

                    </View>
                    <View style={[UStyle.styles.commanStyle, { alignItems: 'center' }]}>
                        <Image source={Images.repro_logo1} resizeMode={'center'} ></Image>
                    </View>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={{ fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(2) }}>
                            {translations.signUpHeaderText}</Text>
                    </View>
                    {loginUserNotExist ?
                        <View style={{ paddingTop: responsiveWidth(1), paddingLeft: responsiveWidth(1) }}>
                            <Text style={styles.errorColor}>{translations.SignUpUserCheck}</Text>
                        </View> : null
                    }
                    <View style={{ paddingVertical: responsiveWidth(4), }}>
                        <CustomInput inputvalue={name} OnChangeText={setUserName}
                            errorField={nameError}
                            secureText={false}

                            label={translations.NameLabel} placeholder={translations.NamePlaceHolder} />
                    </View>
                    {nameError ?
                        <View style={{}}>
                            <Text style={styles.errorColor}>{translations.NameTextValidation}</Text>
                        </View> : null
                    }
                    <View style={{ paddingVertical: responsiveWidth(4), }}>
                        <CustomInput inputvalue={emailID} OnChangeText={setUserEmailID}
                            errorField={emailIDError}
                            secureText={false}
                            textType={'email-address'}
                            label={translations.EMAIL} placeholder={translations.EMAIL_PlaceHolder} />
                    </View>
                    {emailIDError ?
                        <View style={{}}>
                            <Text style={styles.errorColor}>{translations.EmailTextValidation}</Text>
                        </View> : null
                    }


                    <View style={{ paddingVertical: responsiveWidth(4), }}>
                        <CustomInput inputvalue={contactNumber} OnChangeText={setContactNo}
                            errorField={contactNumberError}
                            secureText={false}
                            textType={"numeric"}
                            label={translations.ContactNumber} placeholder={translations.ContactNumberPlaceHolder} />
                    </View>
                    {contactNumberError ?
                        <View style={{}}>
                            <Text style={styles.errorColor}>{translations.PhoneNumberValidation}</Text>
                        </View> : null
                    }
                    <View style={{ paddingVertical: responsiveWidth(4), }}>
                        <CustomInput inputvalue={password} OnChangeText={setUserPassword}
                            errorField={passwordError}
                            label={translations.PASSWORD}
                            secureText={true}
                            placeholder={translations.PASSWORD_PlaceHolder} />
                    </View>
                    {passwordError ?
                        <View>
                            <Text style={styles.errorColor}>{translations.PasswordTextValidation}</Text>
                        </View> : null
                    }
                    <View>
                        <CustomButton buttonTitle={translations.SignUpButtonTitle} onPress={signUpUser}></CustomButton>
                    </View>
                    <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { props.navigation.navigate('LogIn') }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: 'black', fontSize: responsiveFontSize(2), textDecorationLine: 'underline' }}>{translations.BackHeaderButton}</Text>
                        </View>
                    </TouchableOpacity>

                </ImageBackground>
            </ScrollView>
        </View >
    )

}

const styles = StyleSheet.create({

    outerView: {
        marginVertical: responsiveWidth(4),
        borderRadius: responsiveWidth(3),
        marginHorizontal: responsiveWidth(5.9),
        backgroundColor: '#3b5998'// Colors.appColor
    },
    innerView: {
        flexDirection: 'row', alignContent: 'center',
        alignItems: 'center',
        padding: responsiveWidth(1),
        paddingLeft: responsiveWidth(8)
    },
    innerViewText: {
        paddingLeft: responsiveWidth(12),
        fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(2),
        color: '#ffff',
    },
    imageStyle: {
        width: responsiveWidth(10),
        height: responsiveWidth(10),
    },
    errorColor: {
        color: 'red', fontSize: responsiveFontSize(1.5), paddingHorizontal: responsiveWidth(7)
    },
    backButtonView: {
        marginLeft: responsiveWidth(2), width: responsiveWidth(20),
        height: responsiveWidth(8),
        backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center',
        borderRadius: responsiveWidth(2)
    },
    backButtonText: {
        paddingHorizontal: responsiveWidth(2),
        fontFamily: Fonts.Roboto.Roboto_Bold,
        color: '#fff', textAlign: 'center',
        fontSize: responsiveFontSize(1.8)
    }
})

export default SignUpScreen;