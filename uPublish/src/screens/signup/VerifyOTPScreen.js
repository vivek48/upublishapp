import React, { Component, useContext, useState, useEffect } from 'react';
import { View, Text, Image, ImageBackground, StyleSheet, TouchableOpacity, TextInput, ToastAndroid } from 'react-native'
import { UStyle, Images, Fonts, Colors } from '../../styles/Index'
import { LocalizationContext } from '../../services/localization/LocalizationContext'
import { UserContext } from '../../services/loginuser/UserContext'
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import CustomInput from '../../component/CustomInput/CustomInput'
import CustomButton from '../../component/CustomInput/CustomButton';
import Constants from "../../component/comman/Constant";
import Loader from '../../component/comman/Loader'
import { ScrollView } from 'react-native-gesture-handler';
import { ApiCall } from '../../component/comman/ApiCall';
const VerifyOTPScreen = (props) => {
    const { appLanguage, initializeAppLanguage, setAppLanguage, translations } = useContext(LocalizationContext);
    const { setUserLogin, userLogin } = useContext(UserContext)
    const [isLoading, setIsLoading] = useState(false);
    const [mobileOTP, setMobileOTP] = useState('');
    const [emailOTP, setEmailOTP] = useState('');
    const [mobileNo, setMobileNo] = useState(props.route.params.mobileNo);
    const [userName, setUserName] = useState(props.route.params.userName);
    const [emailId, setEmailId] = useState(props.route.params.emailId);
    const [mobileOTPVerify, setMobileOTPVerify] = useState(props.route.params.mobileOTPVerify);
    const [emailOTPVerify, setEmailOTPVerify] = useState(props.route.params.emailOTPVerify);
    useEffect(() => {
    }, []);
    const resendOTPAPI = (type) => {
        var userData = {}
        userData.mobileNo = mobileNo;
        userData.UserName = userName;
        userData.emailId = emailId;
        if (type == "MOBILE_NUMBER") {
            userData.otptype = 1;

            userData.otp = '';
        } else {
            console.log("EMail --OTP");
            userData.otptype = 2;
            userData.otp = '';


        }
        setIsLoading(true);
        ApiCall.Api_Call('/resendOtp', userData, 'POST', false).then((result) => {
            setIsLoading(false);
            console.log(result);
            ToastAndroid.show(result.message, ToastAndroid.LONG);
            if (result.status == 1) {

            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {
            console.log(error);
        });
    }


    const verifyOTPAPI = (type) => {
        var userData = {}
        // 

        userData.UserName = userName;
        userData.emailId = emailId;
        if (type == "MOBILE_NUMBER") {
            userData.mobileNo = mobileNo;
            userData.otptype = 1;
            userData.otp = mobileOTP;
        } else {
            console.log("EMail --OTP");
            userData.otptype = 2;
            userData.otp = emailOTP;

        }
        setIsLoading(true);
        ApiCall.Api_Call('/verifyaccOTP', userData, 'POST', false).then((result) => {
            setIsLoading(false);
            console.log(result);
            ToastAndroid.show(result.message, ToastAndroid.LONG)
            if (result.status == 1) {
                if (type == "MOBILE_NUMBER") {
                    setMobileOTPVerify(true)
                } else {
                    setEmailOTPVerify(true)
                }
                if (result.result != undefined) {
                    console.log(result, "Verified--------------------- BEfore ");
                    verifyAllOTP(result);
                }
                // if (emailOTPVerify && mobileOTPVerify) {

                // }
            } else if (result.status == 0) {
            } else {
            }
        }).catch((error) => {
            console.log(error);
        });
    }


    const verifyAllOTP = (result) => {
        console.log(result, "Verified---------------------");
        Constants.LogInUserID = result.result.userId;
        Constants.LogIn_User = JSON.stringify(result.result);
        //  setLoginUserNotExist(false)
        setUserLogin(result.result);
        saveUserLanguage(result.result.userId);
    }

    const saveUserLanguage = (userID) => {
        //  console.log("language",);
        var requestData = {};
        requestData.language = appLanguage;
        requestData.userId = userID;
        ApiCall.Api_Call('/language/saveUserlanguage', requestData, 'POST', true).then((result) => {
            console.log("result language", result);
            if (result.status == 1) {

            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {

            console.log(error);
        });
    }


    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <ScrollView>
                <Loader loading={isLoading} />
                <ImageBackground source={Images.login_bg} style={UStyle.styles.imageBackGound}>
                    <View style={{
                        // height: responsiveWidth(14),
                        paddingHorizontal: responsiveWidth(5),
                        alignSelf: 'center',
                        marginTop: responsiveWidth(5),
                        marginBottom: responsiveWidth(2)
                    }}>
                        <Image source={Images.repro_logo1} resizeMode={'center'} ></Image>
                    </View>
                    <View style={{ alignItems: 'center', paddingVertical: responsiveWidth(1) }}>
                        <Text style={{ fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(2) }}>
                            Verify OTP</Text>
                    </View>
                    <View style={styles.outerView}>
                        <View>
                            <Text style={styles.textLabel}>
                                Verify EMail OTP <Text style={{ color: 'red' }}>*</Text>
                            </Text>
                        </View>
                        <View style={styles.verifyView}>
                            <View>
                                <TextInput style={styles.textInputStyle}
                                    placeholder="Enter email otp..."
                                    value={emailOTP}
                                    name="emailOTP"
                                    editable={emailOTPVerify ? false : true}
                                    keyboardType={'numeric'}
                                    onChangeText={(emailOTP) => { setEmailOTP(emailOTP) }}

                                ></TextInput>
                            </View>
                            <View>
                                <Image source={emailOTPVerify ? Images.checkedIcon : Images.removeIcon} style={{ width: responsiveWidth(6), height: responsiveWidth(6), }}></Image>
                            </View>
                            {!emailOTPVerify &&
                                <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { if (!emailOTPVerify) { verifyOTPAPI("E-MAIL") } }}>
                                    <View>
                                        <Text style={styles.verifyOTP}>Verify</Text>
                                    </View>
                                </TouchableOpacity>
                            }
                        </View>
                        {!emailOTPVerify &&
                            <View style={{ flexDirection: 'row' }}>
                                <View>
                                    <Text style={styles.getCode}>Didn't get code? </Text>
                                </View>

                                <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { resendOTPAPI("E-MAIL") }}>
                                    <View>
                                        <Text style={styles.resendOTP}> Resend OTP</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        }
                    </View>
                    <View style={[styles.outerView, { marginBottom: responsiveWidth(10) }]}>
                        <View>
                            <Text style={styles.textLabel}>
                                Verify Mobile OTP <Text style={{ color: 'red' }}>*</Text>
                            </Text>
                        </View>
                        <View style={styles.verifyView}>
                            <View>
                                <TextInput style={styles.textInputStyle}
                                    placeholder="Enter mobile otp..."
                                    value={mobileOTP}
                                    name="mobileOTP"
                                    editable={mobileOTPVerify ? false : true}
                                    keyboardType={'numeric'}
                                    onChangeText={(mobileOTP) => { setMobileOTP(mobileOTP) }}
                                ></TextInput>
                            </View>
                            <View>
                                <Image source={mobileOTPVerify ? Images.checkedIcon : Images.removeIcon} style={{ width: responsiveWidth(6), height: responsiveWidth(6), }}></Image>
                            </View>
                            {!mobileOTPVerify &&
                                <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { if (!mobileOTPVerify) { verifyOTPAPI("MOBILE_NUMBER") } }}>
                                    <View>
                                        <Text style={styles.verifyOTP}>Verify</Text>
                                    </View>
                                </TouchableOpacity>
                            }
                        </View>
                        {!mobileOTPVerify &&
                            <View style={{ flexDirection: 'row' }}>
                                <View>
                                    <Text style={styles.getCode}>Didn't get code? </Text>
                                </View>
                                <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { resendOTPAPI("MOBILE_NUMBER") }}>
                                    <View>
                                        <Text style={styles.resendOTP}> Resend OTP</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        }
                    </View>
                    <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { props.navigation.pop() }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: 'black', fontSize: responsiveFontSize(2), textDecorationLine: 'underline' }}>{translations.BackHeaderButton}</Text>
                        </View>
                    </TouchableOpacity>

                </ImageBackground>
            </ScrollView>
        </View >
    )

}

const styles = StyleSheet.create({
    textInputStyle: {
        backgroundColor: '#f2f4f5', width: responsiveWidth(50), height: responsiveWidth(10), borderColor: '#ced4da',
        borderWidth: responsiveWidth(0.2),
        borderRadius: responsiveWidth(2)
    },
    textLabel: {
        fontFamily: Fonts.Roboto.Roboto_BoldItalic, fontSize: responsiveFontSize(1.8)
    },
    verifyView: { flexDirection: 'row', marginVertical: responsiveWidth(2), justifyContent: 'space-between', alignItems: 'center' },
    outerView: { paddingHorizontal: responsiveWidth(7), marginTop: responsiveWidth(5) },
    getCode: {
        fontFamily: Fonts.Roboto.Roboto_Regular, fontSize: responsiveFontSize(1.7),
        textDecorationLine: 'underline'
    },
    resendOTP: { fontFamily: Fonts.Roboto.Roboto_BoldItalic, color: Colors.appColor, textDecorationLine: 'underline' },
    verifyOTP: {
        fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(2),
        color: 'green',
        textDecorationLine: 'underline'
    }
})

export default VerifyOTPScreen;