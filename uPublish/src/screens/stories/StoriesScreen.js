import React, { Component, useContext, useEffect, useState } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import Header from "../../component/CustomInput/Header";
import { LocalizationContext } from '../../services/localization/LocalizationContext';
import { ApiCall } from '../../component/comman/ApiCall'
import Loader from '../../component/comman/Loader'
import Constants from '../../component/comman/Constant'
import StoriesRender from "../../component/CustomInput/StoriesRender"
import { Colors, Fonts, Images } from '../../styles/Index'
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
const StoriesScreen = (props) => {
    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);
    const [stories, setStories] = useState([]);
    const [storiesCount, setStoriesCount] = useState(0);
    const [isLoading, setIsLoading] = useState(false);
    const [userID, setUserID] = useState(JSON.parse(Constants.LogIn_User).userId);
    useEffect(() => {
        getUserStories(userID);
    }, [])
    const getUserStories = (userID) => {
        var userData = {};
        userData.createdBy = [userID]   //userID
        setIsLoading(true);
        ApiCall.Api_Call('/story/advancedSearch', userData, 'POST', true).then((result) => {
            setIsLoading(false)
            // console.log(result.result.records);
            if (result.status == 1) {
                if (result.result.records.length > 0) {
                    setStories(result.result.records);
                    setStoriesCount(result.result.count);
                }
            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });
    }
    return (
        <View>
            <Header headerTitle={translations.Stories} navigation={props.navigation} />
            <Loader loading={isLoading} />
            {stories.length == 0 &&
                <View style={styles.storiesList}>
                    <Text style={[styles.noStoriesListText, {
                        textAlign: 'center',
                        fontFamily: Fonts.Roboto.Roboto_Light
                    }]}>{translations.StoriesList}</Text>
                </View>}
            <View style={{ paddingBottom: responsiveWidth(27) }}>
                <FlatList
                    data={stories}
                    // onEndReached={this.handleLoadMoreData}
                    onEndReachedThreshold={200}
                    renderItem={({ item, index, }) => (
                        <StoriesRender index={index} item={item} navigation={props} />
                    )}
                    keyExtractor={(item, index) => item._id.toString()}
                // ListFooterComponent={this.renderFooter}
                // extraData={this.state.refresh}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    storiesList: {
        width: responsiveWidth(100),
        paddingHorizontal: responsiveWidth(4),
        height: responsiveHeight(5),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#fefefe',
        borderBottomColor: 'grey',
        borderBottomWidth: responsiveWidth(0.1)
    },
    noStoriesListText: {
        fontSize: responsiveFontSize(1.8),
        fontFamily: Fonts.Roboto.Roboto_Medium
    },
})
export default StoriesScreen;
