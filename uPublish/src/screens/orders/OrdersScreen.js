import React, { Component, useContext, useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, TextInput, TouchableOpacity } from 'react-native';
import Header from "../../component/CustomInput/Header";
import { LocalizationContext } from '../../services/localization/LocalizationContext';
import Loader from "../../component/comman/Loader";
import { Colors, Fonts, Images } from '../../styles/Index'
import { ApiCall } from '../../component/comman/ApiCall'
import OrdersRender from "../../component/CustomInput/OrdersRender";
import Constacts from '../../component/comman/Constant'
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import { ScreenClass } from '../../component/comman/ScreenClass'
import { ScreenName } from '../../component/comman/ScreenName';
import { FirebaseConfig } from '../../component/comman/FirebaseConfig';

import Constant from '../../component/comman/Constant'

const OrdersScreen = (props) => {
    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);
    const [isLoading, setIsLoading] = useState(false);
    const [ordersList, setOrdersList] = useState([]);
    const [searchOrdersText, setSearchOrdersText] = useState('');
    const [userID, setUserID] = useState('');

    useEffect(() => {
        FirebaseConfig.realtimeScreenLogContent(ScreenClass.Orders, ScreenName.Orders)
        const unsubscribe = props.navigation.addListener('focus', () => {
            setUserID(JSON.parse(Constacts.LogIn_User).userId)
            getOrdersBook(searchOrdersText);
        })
    }, [])

    const getOrdersBook = (searchOrdersText) => {
        var userData = {};
        userData.searchText = searchOrdersText;
        userData.userId = userID
        setIsLoading(true);
        ApiCall.Api_Call('/order/search/simple', userData, 'POST', true).then((result) => {
            setIsLoading(false)
          //  console.log(result.result.orders);
            if (result.status == 1) {
                setOrdersList(result.result.orders)
            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });
    }


   





    return (
        <View style={{ flex: 1 }}>
            {/* height: responsiveHeight(90)  */}
            <Header headerTitle={translations.Orders} navigation={props.navigation} />
            <Loader loading={isLoading} />

            <View style={[styles.ViewDate, { backgroundColor: '#F4F6F6' }]}>
                <View style={{ marginRight: responsiveWidth(1) }}>
                    <TextInput
                        placeholder={translations.OrdersSearchInput}
                        value={searchOrdersText}
                        name={'searchOrdersText'}
                        testID={'searchOrdersText'}
                        onChangeText={(searchOrdersText) => setSearchOrdersText(searchOrdersText)}
                        style={{
                            backgroundColor: '#ffff',
                            paddingHorizontal: responsiveWidth(2),
                            fontFamily: Fonts.Roboto.Roboto_Medium,
                            borderColor: Colors.appColor,
                            borderWidth: responsiveWidth(0.2),
                            paddingVertical: responsiveWidth(1), borderRadius: responsiveWidth(1.5),
                            height: responsiveHeight(5), width: responsiveWidth(72)
                        }}>
                    </TextInput>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => {
                        getOrdersBook(searchOrdersText);
                    }}>
                        <View style={{
                            borderRadius: responsiveWidth(3),
                            width: responsiveWidth(20), justifyContent: 'center', alignItems: 'center',
                            height: responsiveHeight(4.8), backgroundColor: Colors.appColor   //'#3DE4B2'
                        }}>
                            <Text style={[styles.dateStyle, { color: '#ffff' }]}>{translations.SearchButton}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
            {ordersList.length == 0 &&
                <View style={styles.noBookList}>
                    <Text style={[styles.noBookListText, {
                        textAlign: 'center',
                        fontFamily: Fonts.Roboto.Roboto_Light
                    }]}>{translations.OrdersList}</Text>
                </View>}
            <View style={{ flex: 1 }}>
                <FlatList
                    data={ordersList}
                    // onEndReached={this.handleLoadMoreData}
                    onEndReachedThreshold={200}
                    renderItem={({ item, index, }) => (
                        <OrdersRender index={index} item={item} navigation={props} />
                    )}
                    keyExtractor={(item, index) => item._id.toString()}
                // ListFooterComponent={this.renderFooter}
                // extraData={this.state.refresh}
                />
            </View>
        </View>

    )
}

const styles = StyleSheet.create({
    noBookList: {
        width: responsiveWidth(100),
        paddingHorizontal: responsiveWidth(4),
        height: responsiveHeight(5),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#fefefe',
        borderBottomColor: 'grey',
        borderBottomWidth: responsiveWidth(0.1)
    },
    noBookListText: {
        fontSize: responsiveFontSize(1.8),
        fontFamily: Fonts.Roboto.Roboto_Medium
    },
    ViewDate: {
        width: responsiveWidth(100),
        paddingHorizontal: responsiveWidth(4),
        height: responsiveHeight(7),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#fefefe',
        borderBottomColor: Colors.appColor,
        borderBottomWidth: responsiveWidth(0.1)
    },
    dateStyle: {
        fontSize: responsiveFontSize(1.8),
        fontFamily: Fonts.Roboto.Roboto_Bold
    },
})


export default OrdersScreen;