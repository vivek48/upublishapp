import React, { useState, useContext, useEffect } from 'react';
import { Button, View, Text, SafeAreaView, StyleSheet, Dimensions, Touchable, PermissionsAndroid, TouchableOpacity, ToastAndroid, Platform } from 'react-native';
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import Pdf from 'react-native-pdf';
import { Colors, Fonts, Images } from '../../styles/Index'
import Header from '../../component/CustomInput/Header'
import RNFetchBlob from 'rn-fetch-blob';
import Loader from "../../component/comman/Loader";
import { LocalizationContext } from "../../services/localization/LocalizationContext";
import RNFS from 'react-native-fs';
import { ScreenClass } from '../../component/comman/ScreenClass'
import { ScreenName } from '../../component/comman/ScreenName';
import { ClickedButtonName } from '../../component/comman/ClickedButtonName';
import { FirebaseConfig } from '../../component/comman/FirebaseConfig'
const PdfInvoicePreviewScreen = ({ modalOpen, props, closeModal, pdfBase64, OrderNumber }) => {
    const source = { uri: "data:application/pdf;base64," + pdfBase64 };
    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);
    const [isLoading, setIsLoading] = useState(false);


    useEffect(() => {
        FirebaseConfig.realtimeScreenLogContent(ScreenClass.InvoicePDFViewer, ScreenName.InvoicePDFViewer);
    })
    const permissionFunc = async () => {
        FirebaseConfig.realtimeClickedButton(ClickedButtonName.DownloadButton)
        if (Platform.OS == 'ios') {
            actualDownload();
        } else {

            try {
                const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    actualDownload();
                } else {
                    ToastAndroid.show('You need to give storage permission to download the file', ToastAndroid.LONG);
                }
            } catch (err) {
                console.warn(err);
            }

        }
    }
    const actualDownload = () => {
        const path = `${RNFetchBlob.fs.dirs.DownloadDir}/${OrderNumber}.pdf`; // `${RNFS.ExternalStorageDirectoryPath}/Download/${OrderNumber}.pdf`;//
        RNFetchBlob.fs.writeFile(path, pdfBase64, "base64", { caches: false }).then(res => {
            console.log("File : ", res);
            ToastAndroid.show("Order Invoice Download successfully,You can see internal  download folder", ToastAndroid.LONG);
        }).catch((err) => {
            ToastAndroid.show(err, ToastAndroid.LONG);
            console.log(err);
        });
        // RNFetchBlob
        //     .config({
        //         // path: downloadDest,
        //         addAndroidDownloads: {
        //             useDownloadManager: true, // <-- this is the only thing required
        //             // Optional, override notification setting (default to true)
        //             notification: true,
        //             fileCache: true,

        //             // Optional, but recommended since android DownloadManager will fail when
        //             // the url does not contains a file extension, by default the mime type will be text/plain
        //             mime: 'application/pdf',
        //             description: 'File downloaded by download manager.',
        //             mediaScannable: true
        //         }
        //     })
        // // .fetch('POST', 'http://samples.leanpub.com/thereactnativebook-sample.pdf')
        // // .then((resp) => {
        // //     // the path of downloaded file
        // //     resp.path()
        // let base64Str = pdfBase64; // resp.data; //
        // let pdfLocation = RNFetchBlob.fs.dirs.DocumentDir + '/' + 'sample1.pdf';
        // RNFetchBlob.fs.writeFile(pdfLocation, RNFetchBlob.base64.encode(base64Str), 'base64').then((res) => {

        //     console.log(res);
        // }).catch((err) => {
        //     console.log(err);
        // });
        // // }).catch((err) => {
        // //     console.log(err);
        // // })
        // // })
    }

    const closeExistsAddress = () => {
        closeModal()
    }
    return (
        <View style={{ flex: 1, }}>
            <Header headerTitle={translations.OrderDetail} backScreen={closeExistsAddress} navigation={props.navigation} />
            <Loader loading={isLoading} />
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity onPress={() => { permissionFunc() }}>
                    <View style={styles.downloadButton}>
                        <Text style={styles.downloadText}>Download</Text>
                    </View>
                </TouchableOpacity>
                <Pdf
                    source={source}
                    onLoadComplete={(numberOfPages, filePath) => {
                        console.log(`number of pages: ${numberOfPages} `);
                    }}
                    onPageChanged={(page, numberOfPages) => {
                        console.log(`current page: ${page} `);
                    }}
                    onError={(error) => {
                        console.log(error);
                    }}
                    onPressLink={(uri) => {
                        console.log(`Link presse: ${uri} `)
                    }}
                    onPageSingleTap={(uri) => {
                        console.log(uri);
                    }}

                    style={styles.pdf} />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ecf0f1',
        marginVertical: responsiveWidth(12)
    },
    pdf: {
        flex: 1,
        width: responsiveWidth(90),
        height: responsiveHeight(70),
        //fitWidth: true
    },

    downloadText: {
        fontFamily: Fonts.Roboto.Roboto_Regular,
        fontSize: responsiveFontSize(1.5),
        paddingVertical: responsiveWidth(2),
        paddingHorizontal: responsiveWidth(12),
        color: '#ffff'
    },
    downloadButton: {
        marginVertical: responsiveWidth(2),
        backgroundColor: Colors.appColor,
        borderRadius: responsiveWidth(1)
    },















    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        height: responsiveHeight(80),
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});

export default PdfInvoicePreviewScreen;