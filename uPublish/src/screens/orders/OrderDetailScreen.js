import React, { Component, useContext, useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import Header from "../../component/CustomInput/Header";
import { LocalizationContext } from '../../services/localization/LocalizationContext';
import Loader from "../../component/comman/Loader";
import { Colors, Fonts, Images } from '../../styles/Index'
import { ApiCall } from '../../component/comman/ApiCall'
import Constacts from '../../component/comman/Constant'
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import ValidationPopup from "../../component/CustomInput/ValidationPopup";
import { CommanFunction } from '../../component/comman/CommanFunction'
import PdfInvoicePreviewScreen from './PdfInvoicePreviewScreen';

import { ScreenClass } from '../../component/comman/ScreenClass'
import { ScreenName } from '../../component/comman/ScreenName';
import { ClickedButtonName } from '../../component/comman/ClickedButtonName';
import { FirebaseConfig } from '../../component/comman/FirebaseConfig'
import getSymbolFromCurrency from 'currency-symbol-map'
const OrderDetailScreen = (props) => {
    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);
    const [isLoading, setIsLoading] = useState(false);
    const [orderItem, setOrderItem] = useState(props.route.params.orderItem);
    const [errorMessage, setErrorMessage] = useState('');
    const [openModal, setOpenModal] = useState(false);
    const [pdfBase64, setPdfBase64] = useState('');

    useEffect(() => {
        FirebaseConfig.realtimeScreenLogContent(ScreenClass.OrdersDetails, ScreenName.OrdersDetails);
    })

    const closeExistsAddress = () => {
        props.navigation.navigate("Orders")
    }

    const sendOrdersEmail = (orderID) => {
        FirebaseConfig.realtimeClickedButton(ClickedButtonName.SendEmail)
        var userData = {};
        userData.orderNo = orderID;
        setIsLoading(true);
        ApiCall.Api_Call('/order/sendOrderEmail', userData, 'POST', true).then((result) => {
            setIsLoading(false)
            if (result.status == 1) {
                setErrorMessage(result.message + " Successfully");
            } else if (result.status == 0) {
                setErrorMessage(result.message + " Successfully");
            } else {
                setErrorMessage(result.message + " Successfully");
            }
        }).catch((error) => {
            setIsLoading(false)
            // setErrorMessage('');
            console.log(error);
        });
    }


    const getInvoiceDownload = (orderID) => {
        FirebaseConfig.realtimeClickedButton(orderID + "_" + ClickedButtonName.OrderInvoiceButton);
        var userData = {};
        var order = {};
        order._id = orderID;
        userData.order = order;
        console.log(userData);
        // userData.order = { _id: orderID };// orderID;
        setIsLoading(true);
        ApiCall.Api_Call('/getInvoiceDownloadLink', userData, 'POST', true).then((result) => {
            setIsLoading(false)
            // console.log(result);
            if (result.status == 1) {
                setPdfBase64(result.result.data);
                setOpenModal(true);
            } else if (result.status == 0) {
                setErrorMessage(result.message + " Successfully");
            } else {
                setErrorMessage(result.message + " Successfully");
            }
        }).catch((error) => {
            setIsLoading(false)
            // setErrorMessage('');
            console.log(error);
        });
    }
    const hideValidationMesage = () => {
        setErrorMessage('');
    }


    const closeModal = (value) => {

        setOpenModal(value);
    }

    // console.log(props.route.params.orderItem);


    const OrderDetails = () => {
        return (
            <>
                <Header headerTitle={translations.OrderDetail} backScreen={closeExistsAddress} navigation={props.navigation} />
                <Loader loading={isLoading} />

                {errorMessage !== '' && <ValidationPopup
                    message={errorMessage}
                    showHide={hideValidationMesage}
                />
                }
                <View style={{ justifyContent: 'space-around', flexDirection: 'row', marginHorizontal: responsiveWidth(2), marginVertical: responsiveWidth(2), }}>
                    <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { getInvoiceDownload(orderItem._id) }}>
                        <View style={[styles.orderItemHeader, { width: responsiveWidth(45) }]}>
                            <Text style={styles.orderItemHeaderText}>{translations.OrderInvoice}</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { sendOrdersEmail(orderItem.order_no) }}>
                        <View style={[styles.orderItemHeader, { width: responsiveWidth(47) }]}>
                            <Text style={styles.orderItemHeaderText}>{translations.OrderSendMail}</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                <ScrollView>
                    <View>
                        <View style={{
                            marginHorizontal: responsiveWidth(2), marginTop: responsiveWidth(1),
                            borderRadius: responsiveWidth(1),
                            borderWidth: responsiveWidth(0.1),
                        }}>
                            <View>
                                <View style={styles.orderItemHeader} >
                                    <Text style={styles.orderItemHeaderText}>{translations.OrderDetails} : - </Text>
                                </View>
                                <View>
                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderNumber} :</Text>
                                        <Text style={styles.orderText}> {orderItem.order_no}</Text>
                                    </View>

                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderStatus} : </Text>
                                        <Text style={styles.orderText}>  {orderItem.status}</Text>
                                    </View>

                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderAwbNo} : </Text>
                                        <Text style={styles.orderText}> {orderItem.awbNo}</Text>
                                    </View>

                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderDate} : </Text>
                                        <Text style={styles.orderText}> {CommanFunction.orderDateFormatter(orderItem.order_date)}</Text>
                                    </View>

                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderShipDate} : </Text>
                                        <Text style={styles.orderText}> {CommanFunction.orderDateFormatter(orderItem.shipby_date)}</Text>
                                    </View>
                                    {/* <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderCurrency} : </Text>
                                        <Text style={styles.orderText}> {orderItem.order_currency}</Text>
                                    </View> */}
                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderAmmout} : </Text>
                                        <Text style={styles.orderText}>  {orderItem.order_currency + " " + getSymbolFromCurrency(orderItem.order_currency) + " " + orderItem.order_amount}</Text>
                                    </View>


                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderBookRemarks}: </Text>
                                        <Text style={styles.orderText}>  {orderItem.order_remarks} </Text>
                                    </View>
                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderChannelLocation} : </Text>
                                        <Text style={styles.orderText}>  {orderItem.order_location} </Text>
                                    </View>
                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderRemarks} : </Text>
                                        <Text style={styles.orderText}>  {orderItem.remarks} </Text>
                                    </View>
                                </View>
                            </View>
                            <View>
                                <View style={[styles.orderItemHeader, { marginTop: responsiveWidth(1) }]}>
                                    <Text style={styles.orderItemHeaderText}>{translations.OrderCustomerDetails} : - </Text>
                                </View>
                                <View>
                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderCustomerName} : </Text>
                                        <Text style={styles.orderText}>  {orderItem.customer_name} </Text>
                                    </View>
                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderCustomerAddress} : </Text>
                                        <Text style={styles.orderText}>  {orderItem.ship_address1} {" " + orderItem.ship_address2} </Text>
                                    </View>

                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderCustomerPincode} : </Text>
                                        <Text style={styles.orderText}>  {orderItem.ship_pincode} </Text>
                                    </View>

                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderCustomerCity} : </Text>
                                        <Text style={styles.orderText}>  {orderItem.ship_city} </Text>
                                    </View>

                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderCustomerState} : </Text>
                                        <Text style={styles.orderText}>  {orderItem.ship_state} </Text>
                                    </View>

                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderCustomerCountry} : </Text>
                                        <Text style={styles.orderText}>  {orderItem.ship_country} </Text>
                                    </View>
                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderCustomerContactNumber} : </Text>
                                        <Text style={styles.orderText}>  {orderItem.ship_phone1} </Text>
                                    </View>
                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderCustomerEmailID} : </Text>
                                        <Text style={styles.orderText}>  {orderItem.ship_email1} </Text>
                                    </View>
                                </View>
                            </View>
                            <View>
                                <View style={[styles.orderItemHeader, { marginTop: responsiveWidth(1) }]} >
                                    <Text style={styles.orderItemHeaderText}>{translations.OrderPaymentDetails} : - </Text>
                                </View>
                                <View>
                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderPaymentAmmount} :</Text>
                                        <Text style={styles.orderText}>  {orderItem.order_currency + " " + getSymbolFromCurrency(orderItem.order_currency) + " " + orderItem.order_amount}</Text>
                                    </View>

                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderPaymentID} :</Text>
                                        <Text style={styles.orderText}>{orderItem.paymentRef.razorpay_payment_id}</Text>
                                    </View>

                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderPaymentDate} : </Text>
                                        <Text style={styles.orderText}> {CommanFunction.orderDateFormatter(orderItem.paymentRef.createdOn)}</Text>
                                    </View>
                                </View>
                            </View>
                            <View>
                                <View style={[styles.orderItemHeader, { marginTop: responsiveWidth(1) }]}>
                                    <Text style={styles.orderItemHeaderText}>{translations.OrderCouponDetails} : - </Text>
                                </View>
                                <View>
                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderCouponCode} :</Text>
                                        <Text style={styles.orderText}>{orderItem.couponCode}</Text>
                                    </View>
                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderDiscount} :</Text>
                                        <Text style={styles.orderText}>  {orderItem.order_currency + " " + getSymbolFromCurrency(orderItem.order_currency) + " " + orderItem.discount}</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={[styles.orderItemHeader, { marginTop: responsiveWidth(1) }]}>
                                <Text style={styles.orderItemHeaderText}>{translations.OrderItemList} : - </Text>
                            </View>
                            {orderItem.items.map((item, index) => (
                                console.log(item),

                                <View key={index} style={{ paddingBottom: responsiveWidth(1) }}>
                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderISBN} :</Text>
                                        <Text style={styles.orderText}> {item.isbn}</Text>
                                    </View>
                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderQuantity} :</Text>
                                        <Text style={styles.orderText}> {item.order_qty}</Text>
                                    </View>
                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderItemType} :</Text>
                                        <Text style={styles.orderText}> {item.orderitemtype}</Text>
                                    </View>

                                    <View style={styles.viewH1Text}>
                                        <Text style={styles.orderTextH1}>{translations.OrderUnitPrice} :</Text>
                                        <Text style={styles.orderText}> {item.unit_price}</Text>
                                    </View>
                                </View>
                            ))
                            }
                        </View>
                    </View>
                </ScrollView>
            </>
        )

    }






    return (
        <View style={{ flex: 1, }}>
            {openModal ?

                <PdfInvoicePreviewScreen OrderNumber={orderItem.order_no} closeModal={closeModal} modalOpen={openModal} pdfBase64={pdfBase64} props={props} />
                : <OrderDetails />
            }
        </View>

    )

}



const styles = StyleSheet.create({

    orderItemHeader: {
        justifyContent: 'center',
        alignItems: 'center',
        borderTopWidth: responsiveWidth(0.1),
        borderBottomWidth: responsiveWidth(0.1),
        backgroundColor: Colors.appColor,
        borderColor: 'lightgrey',
        borderRadius: responsiveWidth(0.7),
    },
    orderItemHeaderText: {
        textAlign: 'center',
        padding: responsiveWidth(1.5),
        color: '#ffff',
        fontFamily: Fonts.Roboto.Roboto_Black,
        letterSpacing: responsiveWidth(0.2),
        fontSize: responsiveFontSize(1.5),
    },

    viewH1Text: {
        flexDirection: 'row', paddingHorizontal: responsiveWidth(2),
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: responsiveWidth(1.8)
    },
    orderTextH1: {
        fontFamily: Fonts.Roboto.Roboto_Bold,
        fontSize: responsiveFontSize(1.4),
        letterSpacing: responsiveWidth(0.2),
        color: '#031215',// '#706A69',//#8C8887',//#9D9A9A',// '#292827',// '#827A78'


    },
    orderText: {
        fontFamily: Fonts.Roboto.Roboto_Medium,
        fontSize: responsiveFontSize(1.5),
        letterSpacing: responsiveWidth(0.2),
        color: '#89A8AD',//#1C8A9E',// '#ADABAB',// '#827A78',// '#786865'

    }


})


export default OrderDetailScreen;