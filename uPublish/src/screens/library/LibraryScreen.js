import React, { Component, useContext, useEffect, useState } from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';
import Header from "../../component/CustomInput/Header";
import { LocalizationContext } from '../../services/localization/LocalizationContext';
import { ApiCall } from "../../component/comman/ApiCall";
import RenderBook from '../../component/CustomInput/RenderBook';
import Loader from '../../component/comman/Loader'
import { Colors, Fonts, Images } from '../../styles/Index';
import { responsiveFontSize, responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions'
const LibraryScreen = (props) => {
    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);
    const [isLoading, setIsLoading] = useState(false);
    const [listBook, setListBook] = useState([]);

    useEffect(() => {
        getAllLibrariesBook();
    }, [])

    const getAllLibrariesBook = () => {
        setIsLoading(true);
        ApiCall.Api_Call('/getBooks', '', 'POST', true).then((result) => {
            setIsLoading(false)
            // console.log(result);
            if (result.status == 1) {
                setListBook(result.result)

            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });
    }
    return (
        <View style={{flex:1}} >
            {/* style={{ height: responsiveHeight(95) }} */}
            <Loader loading={isLoading} />
            <Header headerTitle={translations.Library} navigation={props.navigation} />
            {listBook.length == 0 &&
                <View style={styles.noBookList}>
                    <Text style={[styles.noBookListText, {
                        textAlign: 'center',
                        fontFamily: Fonts.Roboto.Roboto_Light
                    }]}>{translations.LibraryList}</Text>
                </View>}
            <View style={{ paddingBottom: responsiveWidth(10) }}>
                <FlatList
                    data={listBook}
                    // onEndReached={this.handleLoadMoreData}
                    onEndReachedThreshold={200}
                    renderItem={({ item, index, }) => (
                        <RenderBook index={index} item={item} navigation={props} />
                    )}
                    keyExtractor={(item, index) => item._id.toString()}
                // ListFooterComponent={this.renderFooter}
                // extraData={this.state.refresh}
                />
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    noBookList: {
        width: responsiveWidth(100),
        paddingHorizontal: responsiveWidth(4),
        height: responsiveHeight(5),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#fefefe',
        borderBottomColor: 'grey',
        borderBottomWidth: responsiveWidth(0.1)
    },
    noBookListText: {
        fontSize: responsiveFontSize(1.8),
        fontFamily: Fonts.Roboto.Roboto_Medium
    },
})

export default LibraryScreen;
