import React, { Component } from 'react';
import { StyleSheet, Text, View, Animated, Modal, StatusBar, NativeModules } from 'react-native';
import { Epub, Streamer } from "epubjs-rn-62";
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import TopBar from './TopBar'
import BottomBar from './BottomBar'
import Nav from './Nav'
import Header from '../../component/CustomInput/Header';
import AsyncStorage from '@react-native-community/async-storage';
import RNFS from 'react-native-fs';
import { CommanFunction } from '../../component/comman/CommanFunction';
import Constant from '../../component/comman/Constant';
import { ApiCall, } from '../../component/comman/ApiCall';
import { Fonts } from '../../styles/Fonts';
// const path = RNFS.ExternalStorageDirectoryPath + '/uPublish/';
class EPubReader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            flow: "paginated", // paginated || scrolled-continuous
            location: 0,  //"epubcfi(/6/8[content_2_item_2]!/4/198/1:201)",
            url: props.route.params.ePubFile,
            epubUrlKey: props.route.params.epubUrlKey,
            //   props.route.params.ePubFile,
            src: "",
            origin: "",
            title: "",
            toc: [],
            showBars: true,
            showNav: false,
            sliderDisabled: true,
            visibleLocation: '',
            startLocation: 0,
            endLocation: 0,
            pageNumber: 0,

        };
        this.streamer = new Streamer({ port: '8899' });
    }
    componentDidMount() {
        // console.log("url " + this.state.url);


        this.streamer.start()
            .then((origin) => {
                this.setState({ origin })
                // console.log("origin------", origin);
                // return this.streamer.get(" http://localhost:8899/5ff5934625c50c02fda648a3.epub ");
                return this.streamer.get(this.state.url);
            })
            .then((src) => {
                console.log("EPUB FIle Location done -----" + src + '--------')

                if (!Constant.BuyEPubBookLIST.includes(src)) {
                    CommanFunction.setBuyBook(src)
                }
                if (this.state.epubUrlKey !== null) {
                    this.deleteEpubBookUrlFromServer(this.state.epubUrlKey)
                }

                return this.setState({ src });
            }).catch((err) => console.log("file err--------", err));

        // AsyncStorage.getItem('Location').then((res) => {
        //     console.log("Asyinccc", res, res.start);
        //     if (res != null) {
        //         var parseData = JSON.parse(res);
        //         this.setState({
        //             // visibleLocation: parseData,
        //             startLocation: parseData.start.location,
        //             location: parseData.end.location,
        //             endLocation: parseData.end.location, pageNumber: parseData.end.displayed.page
        //         });
        //     }
        // }).catch((err) => console.log(err)),
        setTimeout(() => this.toggleBars(), 1000);
    }

    componentWillUnmount() {
        this.streamer.kill();
    }
    toggleBars() {
        this.setState({ showBars: !this.state.showBars });
    }



    deleteEpubBookUrlFromServer = (booklUrl) => {
        var userData = {};
        userData.key = booklUrl; //
        ApiCall.Api_Call('/deleteFromS3', userData, 'POST', true).then((result) => {
            console.log(result);
            if (result.status == 1) {
                console.log("Result-------", result);
            } else if (result.status == 0) {
                alert(result.message);
            } else {
            }
        }).catch((error) => {
            console.log(error);
        });
    }


    render() {
        return (
            <View style={styles.container}>
                {/* <Header headerTitle={"Library"} navigation={this.props.navigation} /> */}
                {/* <StatusBar hidden={!this.state.showBars}
                    translucent={true}
                    animated={false} /> */}
                {/* <View style={{ paddingHorizontal: responsiveWidth(2), backgroundColor: 'green' }}> */}
                <Epub style={styles.reader}
                    ref="epub"
                    // src={"https://s3.amazonaws.com/epubjs/books/moby-dick.epub"}
                    src={this.state.src}
                    flow={this.state.flow}
                    location={this.state.location}
                    // visibleLocation={this.state.visibleLocation}
                    onLocationChange={(visibleLocation) => {
                        console.log("locationChanged------", visibleLocation)

                        if (visibleLocation != undefined && visibleLocation != null) {
                            AsyncStorage.setItem('Location', JSON.stringify(visibleLocation));
                            this.setState({
                                visibleLocation, startLocation: visibleLocation.start.location,
                                endLocation: visibleLocation.end.location, pageNumber: visibleLocation.end.displayed.page
                            });
                        }
                    }}
                    onLocationsReady={(locations) => {
                        console.log("location total", locations.total);
                        this.setState({ sliderDisabled: false });
                    }}
                    onReady={(book) => {
                        // console.log("Metadata", book.package.metadata)
                        // console.log("Table of Contents", book.toc)
                        // console.log("Table of Contents", book)
                        this.setState({
                            title: book.package.metadata.title,
                            toc: book.navigation.toc,
                            book: book
                        });
                    }}
                    onPress={(cfi, position, rendition) => {
                        this.toggleBars();
                        // console.log("press", cfi);
                    }}
                    onLongPress={(cfi, rendition) => {
                        // console.log("longpress", cfi);
                    }}
                    onDblPress={(cfi, position, imgSrc, rendition) => {
                        // Path relative to where the book is opened
                        // console.log(this.state.book.path.directory);
                        // imgSrc is the actual src in the img html tag
                        // console.log("dblpress", cfi, position, imgSrc)
                    }}
                    onViewAdded={(index) => {
                        // console.log("added", index)
                    }}
                    beforeViewRemoved={(index) => {
                        // console.log("removed", index)
                    }}
                    onSelected={(cfiRange, rendition) => {
                        // console.log("selected", cfiRange)
                        // Add marker
                        rendition.highlight(cfiRange, {});
                    }}
                    onMarkClicked={(cfiRange, data, rendition) => {
                        // console.log("mark clicked", cfiRange)
                        rendition.unhighlight(cfiRange);
                    }}


                // themes={{
                //     tan: {
                //         body: {
                //             "-webkit-user-select": "none",
                //             "user-select": "none",
                //             "background-color": "tan",
                //             "font-size": "50px"
                //         }
                //     }
                // }}
                // theme="tan"
                // regenerateLocations={true}
                // generateLocations={true}
                // origin={this.state.origin}
                // onError={(message) => {
                //     console.log("EPUBJS-Webview", message);
                // }}
                />
                {this.state.endLocation !== 0 && this.state.endLocation !== -1 &&
                    <View style={styles.bookLocationAndPageView}>
                        <View>
                            <Text style={styles.bookLocationText}>
                                Location  {this.state.startLocation} of {" " + this.state.endLocation}
                            </Text>
                        </View>
                        <View>
                            <Text style={styles.bookLocationText}>
                                Page No.  {this.state.pageNumber}
                            </Text>
                        </View>
                    </View>
                }
                {/* </View> */}
                {/* <View
                    style={[styles.bar, { top: 0 }]}>
                     <TopBar
                        title={this.state.title}
                        shown={true}
                        onLeftButtonPressed={() => this._nav.show()}
                        onRightButtonPressed={
                            (value) => {
                                if (this.state.flow === "paginated") {
                                    this.setState({ flow: "scrolled-continuous" });
                                } else {
                                    this.setState({ flow: "paginated" });
                                }
                            }
                        }
                    /> 
                </View> */}
                <View
                    style={[styles.bar, { bottom: 0 }]}>
                    <BottomBar
                        disabled={this.state.sliderDisabled}
                        value={this.state.visibleLocation ? this.state.visibleLocation.start.percentage : 0}
                        shown={this.state.showBars}
                        onSlidingComplete={
                            (value) => {
                                this.setState({ location: value.toFixed(6) })
                            }
                        } />
                </View>
                <View>
                    <Nav ref={(nav) => this._nav = nav}
                        display={(loc) => {
                            this.setState({ location: loc });
                        }}
                        toc={this.state.toc}
                    />
                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: responsiveWidth(3),
        marginBottom: responsiveWidth(1),
        // backgroundColor: 'red',
        justifyContent: 'center',
        // alignItems:'center',
    },
    reader: {
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: '#3F3F3C'
    },
    bar: {
        position: "absolute",
        left: 0,
        right: 0,
        height: 55,
        // marginTop: responsiveWidth(3)
    },
    bookLocationAndPageView: {
        backgroundColor: '#ffff',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        paddingVertical: responsiveWidth(2),
        paddingHorizontal: responsiveWidth(6)
    },
    bookLocationText: {
        fontFamily: Fonts.Roboto.Roboto_Light,
        fontSize: responsiveFontSize(1.7)
    }
});

export default EPubReader;
