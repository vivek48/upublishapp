import React, { Component } from 'react';
import { StyleSheet, Text, View, Animated, Modal, StatusBar, NativeModules } from 'react-native';
import { Epub, Streamer } from "epubjs-rn-62";
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import TopBar from './TopBar'
// import BottomBar from './BottomBar'
import Nav from './Nav'
import Header from '../../component/CustomInput/Header';
import AsyncStorage from '@react-native-community/async-storage';
import RNFS from 'react-native-fs';
import { CommanFunction } from '../../component/comman/CommanFunction';
class EPubReader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            flow: "paginated", // paginated || scrolled-continuous
            location: 0,  //"epubcfi(/6/8[content_2_item_2]!/4/198/1:201)",
            url: props.route.params.ePubFile,
            // `file:/${RNFS.ExternalStorageDirectoryPath}/uPublish/igp-twss.epub`,                             //'file://data/user/0/com.upublish/files/61570.epub',    //props.route.params.ePubFile,                       //"https://upublish-in-public.s3-ap-south-1.amazonaws.com/20210126/211/5ff5934625c50c02fda648a3.epub",// "https://s3.amazonaws.com/epubjs/books/moby-dick.epub",
            src: "",
            origin: "",
            title: "",
            toc: [],
            showBars: true,
            showNav: false,
            sliderDisabled: true,
            visibleLocation: ''
        };
        //this.streamer = new Streamer({ port: '8899', root: `${RNFS.ExternalStorageDirectoryPath}/uPublish` });

        this.streamer = new Streamer();
    }
    componentDidMount() {
        // var epubPath;
        // var path = `${RNFS.ExternalStorageDirectoryPath}/uPublish`;
        // RNFS.readDir(path) // On Android, use "RNFS.DocumentDirectoryPath" (MainBundlePath is not defined)
        //     .then((result) => {
        //         console.log('GOT RESULT', result);
        //         // stat the first file
        //         return Promise.all([RNFS.stat(result[0].path), result[0].path]);
        //     })
        //     .then((statResult) => {
        //         if (statResult[0].isFile()) {
        //             epubPath = statResult[0].path;
        //             console.log("state Result", statResult[0].path);
        //         }

        //         return 'no file';
        //     })
        //     .then((contents) => {
        //         // log the file contents
        //         console.log("Content", contents);
        //     })
        //     .catch((err) => {

        //         console.log(err.message, err.code);
        //         return err
        //     });
        AsyncStorage.getItem("Location").then((res) => {
            console.log("location-----------", res);
            if (res != null) {
                console.log("locationSet----------", JSON.parse(res).end.cfi );
                this.setState({ location: JSON.parse(res).end.cfi })
            }
        }).catch((err) => console.log(err));
        console.log("url " + this.state.url);
        this.streamer.start()
            .then((origin) => {
                this.setState({ origin })
                console.log("origin------", origin);
                // var splitEPubName = epubPath.split("/");
                // return this.streamer.get(`file://${RNFS.ExternalStorageDirectoryPath}/uPublish/demo.epub.zip`);
                // this.streamer.check(`'http://localhost:8899/${splitEPubName[splitEPubName.length - 1]}'`).then((t) => console.log("EPUB FILES ----", t));
                // return this.streamer.get(`"http://127.0.0.1:8899/${splitEPubName[splitEPubName.length - 1]}"`);
                // return this.streamer.get("http://127.0.0.1:8899/igp-twss.epub");
                return this.streamer.get(this.state.url);
            })
            .then((src) => {
                console.log("EPUB FIle Location done -----" + src + '--------')
                return this.setState({ src });
            }).catch((err) => console.log("file err--------", err));

        setTimeout(() => this.toggleBars(), 1000);

    }

    componentWillUnmount() {
        this.streamer.kill();
    }

    toggleBars() {
        this.setState({ showBars: !this.state.showBars });
    }


    render() {
        console.log(`${RNFS.ExternalStorageDirectoryPath}/uPublish/`);

        return (
            <View style={styles.container}>
                <Header headerTitle={"Library"} navigation={this.props.navigation} />
                {/* <StatusBar hidden={!this.state.showBars}
                    translucent={true}
                    animated={false} /> */}
                <Epub style={styles.reader}
                    ref="epub"
                    //src={"https://s3.amazonaws.com/epubjs/books/moby-dick.epub"}
                    src={this.state.src}
                    flow={this.state.flow}

                    location={this.state.location}
                    // visibleLocation={this.state.visibleLocation}
                    onLocationChange={(visibleLocation) => {
                         console.log("locationChanged------", visibleLocation)
                        if (visibleLocation != undefined && visibleLocation != null) {
                            AsyncStorage.setItem('Location', JSON.stringify(visibleLocation));
                            this.setState({ visibleLocation });
                        }
                    }}
                    onLocationsReady={(locations) => {
                        // console.log("location total", locations.total);
                        this.setState({ sliderDisabled: false });
                    }}
                    onReady={(book) => {
                        // console.log("Metadata", book.package.metadata)
                        // console.log("Table of Contents", book.toc)
                        // console.log("Table of Contents", book)
                        this.setState({
                            title: book.package.metadata.title,
                            toc: book.navigation.toc,
                            book: book
                        });
                    }}
                    onPress={(cfi, position, rendition) => {
                        this.toggleBars();
                        // console.log("press", cfi);
                    }}
                    onLongPress={(cfi, rendition) => {
                        // console.log("longpress", cfi);
                    }}
                    onDblPress={(cfi, position, imgSrc, rendition) => {
                        // Path relative to where the book is opened
                        // console.log(this.state.book.path.directory);
                        // imgSrc is the actual src in the img html tag
                        // console.log("dblpress", cfi, position, imgSrc)
                    }}
                    onViewAdded={(index) => {
                        // console.log("added", index)
                    }}
                    beforeViewRemoved={(index) => {
                        // console.log("removed", index)
                    }}
                    onSelected={(cfiRange, rendition) => {
                        // console.log("selected", cfiRange)
                        // Add marker
                        rendition.highlight(cfiRange, {});
                    }}
                    onMarkClicked={(cfiRange, data, rendition) => {
                        // console.log("mark clicked", cfiRange)
                        rendition.unhighlight(cfiRange);
                    }}
                // themes={{
                //     tan: {
                //         body: {
                //             "-webkit-user-select": "none",
                //             "user-select": "none",
                //             "background-color": "tan"
                //         }
                //     }
                // }}
                // theme="tan"
                // regenerateLocations={true}
                // generateLocations={true}
                // origin={this.state.origin}
                // onError={(message) => {
                //     console.log("EPUBJS-Webview", message);
                // }}
                />
                <View
                    style={[styles.bar, { top: 0 }]}>
                    <TopBar
                        title={this.state.title}
                        shown={true}
                        onLeftButtonPressed={() => this._nav.show()}
                        onRightButtonPressed={
                            (value) => {
                                if (this.state.flow === "paginated") {
                                    this.setState({ flow: "scrolled-continuous" });
                                } else {
                                    this.setState({ flow: "paginated" });
                                }
                            }
                        }
                    />
                </View>
                {/* <View
                    style={[styles.bar, { bottom: 0 }]}>
                    <BottomBar
                        disabled={this.state.sliderDisabled}
                        value={this.state.visibleLocation ? this.state.visibleLocation.start.percentage : 0}
                        shown={this.state.showBars}
                        onSlidingComplete={
                            (value) => {
                                this.setState({ location: value.toFixed(6) })
                            }
                        } />
                </View> */}
                <View>
                    <Nav ref={(nav) => this._nav = nav}
                        display={(loc) => {
                            this.setState({ location: loc });
                        }}
                        toc={this.state.toc}
                    />
                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    reader: {
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: '#3F3F3C'
    },
    bar: {
        position: "absolute",
        left: 0,
        right: 0,
        height: 55,
        // marginTop: responsiveWidth(3)
    }
});

export default EPubReader;
