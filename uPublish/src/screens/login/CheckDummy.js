
import React, { Component } from 'react';
import { Text, View, Image, PermissionsAndroid } from 'react-native';
import RNFS from 'react-native-fs';


export default class CheckDummy extends Component {
    constructor() {
        super()
        this.state = {
            isDone: false,
        };
        this.onDownloadImagePress = this.onDownloadImagePress.bind(this);
    }

    async onDownloadImagePress() {
        try {
            const granted = await PermissionsAndroid.requestMultiple([
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            ]);
        } catch (err) {
            console.warn(err);
        }
        const readGranted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE);
        const writeGranted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
        if (!readGranted || !writeGranted) {
            console.log('Read and write permissions have not been granted');
            return;
        }
        var path = `${RNFS.ExternalStorageDirectoryPath}/uPublish`;
        const fileExists = await FileSystem.fileExists(path)
        console.log(`file exists: ${fileExists}`)
        RNFS.

        RNFS.mkdir(path);
        RNFS.downloadFile({
            fromUrl: 'http://files.infogridpacific.com/ss/igp-twss.epub',//'https://facebook.github.io/react-native/img/header_logo.png',//
            toFile: `${path}/Metamorphosis.epub`,
        }).promise.then((r) => {
            console.log(r)
            this.setState({ isDone: true })
        });
        RNFS.readDir(path) // On Android, use "RNFS.DocumentDirectoryPath" (MainBundlePath is not defined)
            .then((result) => {
                console.log('GOT RESULT', result);
                // stat the first file
                return Promise.all([RNFS.stat(result[0].path), result[0].path]);
            })
            .then((statResult) => {
                if (statResult[0].isFile()) {
                    // if we have a file, read it
                    console.log(statResult[0]);
                    // return RNFS.readFile(statResult[1], 'utf8');
                }

                return 'no file';
            })
            .then((contents) => {
                // log the file contents
                console.log(contents);
            })
            .catch((err) => {
                console.log(err.message, err.code);
            });







    }

    render() {
        const preview = this.state.isDone ? (<View>
            <Image style={{
                width: 100,
                height: 100,
                backgroundColor: 'black',
            }}
                source={{
                    uri: `file://${RNFS.ExternalStorageDirectoryPath}/uPublish/react-native.png`,
                    scale: 1
                }}
            />
            <Text>{`file://${RNFS.ExternalStorageDirectoryPath}/uPublish/react-native.png`}</Text>
            <Image style={{
                width: 100,
                height: 100,
                backgroundColor: 'black',
            }} source={{ uri: `file://${RNFS.ExternalStorageDirectoryPath}/uPublish/react-native.png` }}>

            </Image>
        </View>
        ) : null;
        return (
            <View>
                <Text onPress={this.onDownloadImagePress}>Download Image</Text>
                {preview}
            </View>
        );
    }
}