import React, { Component, useContext, useState, useEffect } from 'react';
import { View, Text, Image, ImageBackground, StyleSheet, TouchableOpacity } from 'react-native'
import { UStyle, Images, Fonts, Colors } from '../../styles/Index'
import { LocalizationContext } from '../../services/localization/LocalizationContext'
import { UserContext } from '../../services/loginuser/UserContext'
import { responsiveFontSize, responsiveScreenWidth, responsiveWidth } from 'react-native-responsive-dimensions';
import CustomInput from '../../component/CustomInput/CustomInput'
import CustomButton from '../../component/CustomInput/CustomButton';
import Constants from "../../component/comman/Constant";
import auth from '@react-native-firebase/auth';
import Loader from '../../component/comman/Loader'
import {
    LoginButton, AccessToken, LoginManager, GraphRequestManager,
    GraphRequest
} from 'react-native-fbsdk';
import {
    GoogleSignin, GoogleSigninButton, statusCodes,
} from '@react-native-community/google-signin';
import { ScrollView } from 'react-native-gesture-handler';
import { ApiCall } from '../../component/comman/ApiCall';
import { color } from 'react-native-reanimated';

import RNFetchBlob from 'rn-fetch-blob'
import { ScreenClass } from '../../component/comman/ScreenClass'
import { ScreenName } from '../../component/comman/ScreenName';
import { FirebaseConfig } from '../../component/comman/FirebaseConfig';
import { ClickedButtonName } from '../../component/comman/ClickedButtonName';

const LoginScreenChanges = (props) => {
    const { appLanguage, initializeAppLanguage, setAppLanguage, translations } = useContext(LocalizationContext);
    const { setUserLogin, userLogin } = useContext(UserContext)
    const [emailID, setEMailID] = useState('');
    const [password, setPassword] = useState('');
    const [emailIDError, setEMailIDError] = useState('');
    const [passwordError, setPasswordError] = useState('');
    const [loginUserNotExist, setLoginUserNotExist] = useState('');
    const [isLoading, setIsLoading] = useState(false);




    useEffect(() => {
        FirebaseConfig.realtimeScreenLogContent(ScreenClass.LOGINGOOGLE, ScreenName.LOGINGOOGLE);
        GoogleSignin.configure({
            webClientId: '1053152632629-ire1emrshsaai5e92qm57jp5qr62ms9g.apps.googleusercontent.com',// '580156373196-2en6hoku6gvt9506etdtcuq8vg9bjcgs.apps.googleusercontent.com',// '428452491769-qutsn9oo8itrtbiquosjjijvln9ccivh.apps.googleusercontent.com',//761466784967-37jdlmnsa2mfg8qndivehoq49k14g88h.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
            offlineAccess: true,
        });
    }, []);

    const googleLogIn = async () => {
        FirebaseConfig.realtimeClickedButton(ClickedButtonName.LoginGoggle);
        FirebaseConfig.realtimeScreenLogContent(ScreenClass.LOGINGOOGLE, ScreenClass.LOGINGOOGLE);
        setIsLoading(true);
        const logout = await GoogleSignin.signOut()
        const isSignedIn = await GoogleSignin.isSignedIn()
        console.log("googleLogIn---", isSignedIn);
        if (isSignedIn) {
            const userInfo = await GoogleSignin.signInSilently();
            socialLogin(userInfo, 'GOOGLE')
        } else {
            // await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn().then(res => res).catch(err => {
                setIsLoading(false);
                console.log("Log error ---", err)
            });
            socialLogin(userInfo, 'GOOGLE')
        }
    };

    const socialLogin = (userInfo, provider) => {
        var userData = {}
        if (userInfo != null && userInfo != undefined) {
            userData.id = userInfo.user.id;
            userData.name = userInfo.user.name;
            userData.email = userInfo.user.email;
            userData.photoUrl = userInfo.user.photo;
            userData.firstName = userInfo.user.givenName;
            userData.lastName = userInfo.user.familyName;
            if (provider == 'GOOGLE') {
                userData.idToken = userInfo.idToken;
                userData.provider = "GOOGLE"
            } else {
                userData.idToken = userInfo.idToken;
                userData.authToken = userInfo.authToken;
                userData.provider = "FACEBOOK"
            }
            setIsLoading(true)
            //   console.log('User Info --> ', userData);
            ApiCall.Api_Call('/socialLogin', userData, 'POST', false).then((result) => {
                setIsLoading(false)
                //  console.log("Google LOGIN---", result);
                if (result.status == 1) {
                    Constants.LogInUserID = result.result.user.userId;
                    setLoginUserNotExist(false)
                    setUserLogin(result.result);
                    saveUserLanguage(result.result.user.userId);
                    insertUserDeviceID();
                } else if (result.status == 0) {
                    setLoginUserNotExist(true)
                } else {
                }
            }).catch((error) => {

                console.log(error);
            });
        }
    }

    const saveUserLanguage = (userID) => {
        //  console.log("language",);
        var requestData = {};
        requestData.language = appLanguage;
        requestData.userId = userID;
        ApiCall.Api_Call('/language/saveUserlanguage', requestData, 'POST', true).then((result) => {
            console.log("result language", result);
            if (result.status == 1) {

            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {

            console.log(error);
        });
    }





    const insertUserDeviceID = () => {
        var userData = {}
        userData.deviceType = Constants.DeviceType;
        userData.deviceId = Constants.DeviceID;
        userData.isLogin = true,
            // setIsLoading(true);
            ApiCall.Api_Call('/CreateAndUpdateDeviceDetails', userData, 'POST', true).then((result) => {
                // setIsLoading(false);
                console.log(result);
                if (result.status == 1) {

                } else if (result.status == 0) {
                } else {
                }
            }).catch((error) => {
                console.log(error);
            });
    }


    const fbLogin = async () => {
        FirebaseConfig.realtimeClickedButton(ClickedButtonName.LoginFB);
        try {
            const result = await LoginManager.logInWithPermissions(['public_profile', 'email']).then((result, err) => {
                console.log("Facebook Result---", result);

            })
            if (result.isCancelled) {
                throw 'User cancelled the login process';
            }
            const userDetails = {};
            const data = await AccessToken.getCurrentAccessToken();
            console.log("Facebook Result   Token---", data);
            if (!data) {
                throw 'Something went wrong obtaining access token';
            }
            const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);
        }
        catch (error) {
            console.log(error.message)
        }
    }





    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            {/* <ScrollView> */}
            <Loader loading={isLoading} />
            {/* paddingRight: responsiveWidth(27), */}
            <View style={[UStyle.styles.commanStyle, { height: responsiveWidth(14), paddingRight: responsiveScreenWidth(27), alignSelf: 'center', marginTop: responsiveWidth(5) }]}>
                <Image source={Images.repro_logo1} resizeMode={'center'}></Image>
            </View>
            <View style={{ paddingLeft: responsiveWidth(7), marginTop: responsiveWidth(8) }}>
                <View>
                    <View>
                        <Text style={{ textAlignVertical: 'center', fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(3) }}>
                            {translations.LoginH1} </Text>
                        {/* Design Anything. */}
                    </View>
                    <View>
                        <Text style={{ textAlignVertical: 'center', fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(3) }}>
                            {translations.LoginH2}
                        </Text>
                        {/* Anytime Anywhere. */}
                    </View>
                </View>
                <View style={{ paddingTop: responsiveWidth(3), paddingRight: responsiveWidth(9) }}>
                    <Text style={{ color: 'grey', lineHeight: 20, fontSize: responsiveFontSize(1.7) }}>{translations.LoginPageDescription}</Text>
                </View>
            </View>


            <View style={{ justifyContent: 'center', paddingTop: responsiveWidth(10) }}>
                <View style={{ alignItems: 'center' }}>
                    <Text style={{ fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(2) }}>{translations.headerText}</Text>
                </View>
                {/* <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { fbLogin() }}>
                    <View style={styles.outerView}>
                        <View style={styles.innerView}>
                            <View>
                                <Image source={Images.facebook1} style={styles.imageStyle}></Image>
                            </View>
                            <View>
                                <Text style={styles.innerViewText}>{translations.LoginFB}</Text>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity> */}

                <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => { googleLogIn() }}>
                    <View style={[styles.outerView,]}>
                        <View style={styles.innerView}>
                            <View>
                                <Image source={Images.google} style={styles.imageStyle}></Image>
                            </View>
                            <View>
                                <Text style={styles.innerViewText}>{translations.LoginGoggle}</Text>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => {
                    FirebaseConfig.realtimeClickedButton(ClickedButtonName.EMailLogin_Button);
                    props.navigation.navigate('LogIn')
                }}>
                    {/* when added facebook button marginTop: responsiveWidth(10), */}
                    <View style={[styles.outerView, { alignItems: 'center', marginTop: responsiveWidth(3), backgroundColor: Colors.appColor }]}>
                        <View style={[styles.innerView,]}>
                            <View style={{}}>
                                <Text style={[styles.innerViewText, { textAlign: 'center', paddingLeft: responsiveWidth(0), color: '#ffff' }]}>{translations.EMailLogin}</Text>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>

        </View >
    )
}

const styles = StyleSheet.create({

    outerView: {
        marginVertical: responsiveWidth(4),
        borderRadius: responsiveWidth(1.3),
        marginHorizontal: responsiveWidth(5.9),
        // backgroundColor: '#ffff',// '#d9d9d9',
        shadowColor: '#30C1DD',
        shadowRadius: 2,
        shadowOpacity: 0.6,
        elevation: 3,
        paddingVertical: responsiveWidth(2.5)
        // backgroundColor: '#3b5998'// Colors.appColor
    },
    innerView: {
        flexDirection: 'row',
        alignContent: 'center',
        alignItems: 'center',
        padding: responsiveWidth(1),
        paddingLeft: responsiveWidth(3),
    },
    innerViewText: {
        paddingLeft: responsiveWidth(24),
        paddingRight: responsiveWidth(4),
        fontFamily: Fonts.Roboto.Roboto_Italic, fontSize: responsiveFontSize(1.5),
    },
    imageStyle: {
        width: responsiveWidth(5),
        height: responsiveWidth(5),
    },
    errorColor: {
        color: 'red', fontSize: responsiveFontSize(1.5), paddingHorizontal: responsiveWidth(7)
    }
})

export default LoginScreenChanges;