import React, { Component, useContext, useState, useEffect } from 'react';
import { View, Text, Image, ImageBackground, StyleSheet, TouchableOpacity, ToastAndroid } from 'react-native'
import { UStyle, Images, Fonts, Colors } from '../../styles/Index'
import { LocalizationContext } from '../../services/localization/LocalizationContext'
import { UserContext } from '../../services/loginuser/UserContext'
import { responsiveFontSize, responsiveWidth } from 'react-native-responsive-dimensions';
import CustomInput from '../../component/CustomInput/CustomInput'
import CustomButton from '../../component/CustomInput/CustomButton';
import Constants from "../../component/comman/Constant";
import auth from '@react-native-firebase/auth';
import Loader from '../../component/comman/Loader'
import {
    LoginButton, AccessToken, LoginManager, GraphRequestManager,
    GraphRequest
} from 'react-native-fbsdk';
import {
    GoogleSignin, GoogleSigninButton, statusCodes,
} from '@react-native-community/google-signin';
import { ScrollView } from 'react-native-gesture-handler';
import { ApiCall } from '../../component/comman/ApiCall';
import { color } from 'react-native-reanimated';

import { ScreenClass } from '../../component/comman/ScreenClass'
import { ScreenName } from '../../component/comman/ScreenName';
import { FirebaseConfig } from '../../component/comman/FirebaseConfig';
import { ClickedButtonName } from '../../component/comman/ClickedButtonName';

import { CommanFunction } from '../../component/comman/CommanFunction';

const LogInScreen = (props) => {
    const { appLanguage, initializeAppLanguage, setAppLanguage, translations } = useContext(LocalizationContext);
    const { setUserLogin, userLogin } = useContext(UserContext)
    const [emailID, setEMailID] = useState('');
    const [password, setPassword] = useState('');
    const [emailIDError, setEMailIDError] = useState('');
    const [passwordError, setPasswordError] = useState('');
    const [loginUserNotExist, setLoginUserNotExist] = useState('');
    const [isLoading, setIsLoading] = useState(false);




    useEffect(() => {
        FirebaseConfig.realtimeScreenLogContent(ScreenClass.EmailLogin, ScreenName.EmailLogin);
        GoogleSignin.configure({
            webClientId: '580156373196-2en6hoku6gvt9506etdtcuq8vg9bjcgs.apps.googleusercontent.com',// '428452491769-qutsn9oo8itrtbiquosjjijvln9ccivh.apps.googleusercontent.com',// '761466784967-37jdlmnsa2mfg8qndivehoq49k14g88h.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
            offlineAccess: true,
        });
    }, []);
    const loginUserViaMail = () => {
        FirebaseConfig.realtimeClickedButton(ClickedButtonName.E_Mail_LogIN);
        var userData = {}
        // if (emailID.trim() == '' || emailID.trim() == undefined || emailID.trim() == null) {
        //     setEMailIDError(true);
        //     return true;
        // } else {
        if (CommanFunction.ValidateEmail(emailID.trim())) {
            userData.emailId = emailID.trim();
            setEMailIDError(false);
        } else {
            setEMailIDError(true);
            return true;
        }
        // }

        if (password.trim() == '' || password.trim() == undefined || password.trim() == null ||
            passwordError) {
            setPasswordError(true)
            return true;
        } else {
            // if (CommanFunction.CheckPassword(password)) {
            userData.password = password.trim()
            userData.extensionId = ""
            setPasswordError(false)
            // } else {
            //     setPasswordError(true)
            //     return true;
            // }
        }
        setIsLoading(true);
        ApiCall.Api_Call('/auth/login', userData, 'POST', false).then((result) => {
            setIsLoading(false);
            console.log(result);
            ToastAndroid.show(result.description, ToastAndroid.LONG);
            if (result.status == 1) {
                // console.log(result.result);
                setLoginUserNotExist(false)
                setUserLogin(result.result);
                saveUserLanguage(result.result.user.userId);
            } else if (result.status == 0) {
                if (result.result != null) {
                    resendOTPAPI("EMAIL-ID", result.result.user);

                } else {
                    setLoginUserNotExist(true)
                }
            } else {
            }
        }).catch((error) => {
            console.log(error);
        });
    }
    const setUserPassword = (value) => {
        setPasswordError(false)
        setPassword(value);
    }
    const setUserEmailID = (value) => {
        // console.log("demojkjgj", value);
        setEMailIDError(false);
        setEMailID(value);
    }

    const signUpScreen = () => {
        props.navigation.navigate('SignUp');
    }


    const resendOTPAPI = (type, userLoginData) => {
        var userData = {}
        userData.mobileNo = "";
        userData.UserName = userLoginData.userName;
        userData.emailId = userLoginData.userEmail;
        if (type == "MOBILE_NUMBER") {
            userData.otptype = 1;
            userData.otp = '';
        } else {
            userData.otptype = 2;
            userData.otp = '';

        }
        setIsLoading(true);
        ApiCall.Api_Call('/resendOtp', userData, 'POST', false).then((result) => {
            setIsLoading(false);
            console.log(userLoginData);
            ToastAndroid.show(result.message, ToastAndroid.LONG);
            if (result.status == 1) {
                // if (type != "MOBILE_NUMBER") {
                //     resendOTPAPI("EMAIL-ID", userLoginData)
                // } else {
                props.navigation.navigate("VerifyOTPScreen", {
                    mobileNo: "", userName: userLoginData.userName,
                    emailId: userLoginData.userEmail, mobileOTPVerify: userLoginData.mobStatus != null ? true : false,
                    emailOTPVerify: userLoginData.emailStatus != null ? true : false,
                });

                // }
            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {
            console.log(error);
        });
    }


    const saveUserLanguage = (userID) => {
        //  console.log("language",);
        var requestData = {};
        requestData.language = appLanguage;
        requestData.userId = userID;
        ApiCall.Api_Call('/language/saveUserlanguage', requestData, 'POST', true).then((result) => {
            console.log("result language", result);
            if (result.status == 1) {

            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {

            console.log(error);
        });
    }


    return (
        <View>
            <ScrollView>
                <Loader loading={isLoading} />
                <ImageBackground source={Images.login_bg} style={UStyle.styles.imageBackGound}>

                    <View style={{ flexDirection: 'row', marginTop: responsiveWidth(4) }}>
                    </View>
                    <View style={[UStyle.styles.commanStyle, { alignItems: 'center' }]}>
                        <Image source={Images.repro_logo1} resizeMode={'center'} ></Image>
                    </View>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={{ fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(2) }}>{translations.headerText}</Text>
                    </View>
                    {loginUserNotExist ?
                        <View style={{ paddingTop: responsiveWidth(1), paddingLeft: responsiveWidth(1) }}>
                            <Text style={styles.errorColor}>{translations.LoginValidation}</Text>
                        </View> : null
                    }
                    <View style={{ paddingVertical: responsiveWidth(4), }}>
                        <CustomInput inputvalue={emailID} OnChangeText={setUserEmailID}
                            errorField={emailIDError}
                            secureText={false}
                            label={translations.EMAIL} placeholder={translations.EMAIL_PlaceHolder} />
                    </View>
                    {emailIDError ?
                        <View style={{}}>
                            <Text style={styles.errorColor}>{translations.EmailTextValidation}</Text>
                        </View> : null
                    }
                    <View style={{ paddingVertical: responsiveWidth(4), }}>
                        <CustomInput inputvalue={password} OnChangeText={setUserPassword}
                            errorField={passwordError}
                            label={translations.PASSWORD}
                            secureText={true}
                            placeholder={translations.PASSWORD_PlaceHolder} />
                    </View>
                    {passwordError ?
                        <View>
                            <Text style={styles.errorColor}>{translations.PasswordTextValidation}</Text>
                        </View> : null
                    }
                    <View>
                        <CustomButton buttonTitle={translations.buttonTitle} onPress={loginUserViaMail}></CustomButton>
                    </View>

                    <View>
                        <CustomButton buttonTitle={translations.SignUpButtonTitle} onPress={signUpScreen}></CustomButton>
                    </View>
                    <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => {
                        FirebaseConfig.realtimeClickedButton(ClickedButtonName.SignUpButtonTitle);
                        props.navigation.navigate('LoginScreenChanges');
                    }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: 'black', fontSize: responsiveFontSize(2), textDecorationLine: 'underline' }}>{translations.BackHeaderButton}</Text>
                        </View>
                    </TouchableOpacity>
                </ImageBackground>
            </ScrollView>
        </View >
    )




}

const styles = StyleSheet.create({

    outerView: {
        marginVertical: responsiveWidth(4),
        borderRadius: responsiveWidth(3),
        marginHorizontal: responsiveWidth(5.9),
        backgroundColor: '#3b5998'// Colors.appColor
    },
    innerView: {
        flexDirection: 'row', alignContent: 'center',
        alignItems: 'center',
        padding: responsiveWidth(1),
        paddingLeft: responsiveWidth(8)
    },
    innerViewText: {
        paddingLeft: responsiveWidth(12),
        fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(2),
        color: '#ffff',
    },
    imageStyle: {
        width: responsiveWidth(10),
        height: responsiveWidth(10),
    },
    errorColor: {
        color: 'red', fontSize: responsiveFontSize(1.5), paddingHorizontal: responsiveWidth(7)
    },
    backButtonView: {
        marginLeft: responsiveWidth(5), width: responsiveWidth(20),
        height: responsiveWidth(8),
        backgroundColor: Colors.appColor, alignItems: 'center', justifyContent: 'center',
        borderRadius: responsiveWidth(2)
    },
    backButtonText: {
        paddingHorizontal: responsiveWidth(2),
        fontFamily: Fonts.Roboto.Roboto_Bold,
        color: '#fff', textAlign: 'center',
        fontSize: responsiveFontSize(1.8)
    }
})

export default LogInScreen;