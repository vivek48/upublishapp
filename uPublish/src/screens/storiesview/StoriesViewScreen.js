import React, { Component, useContext, useEffect, useState } from 'react';
import { View, Text, Image, ImageBackground } from 'react-native'
import { UStyle, Images, Fonts } from '../../styles/Index'
import { LocalizationContext } from '../../services/localization/LocalizationContext'
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import CustomInput from '../../component/CustomInput/CustomInput'
import { WebView } from 'react-native-webview';
import Header from '../../component/CustomInput/Header'
const StoriesViewScreen = (props) => {
    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);
    const [storiesSourceURL, setStoriesSourceURL] = useState('')
    useEffect(() => {
        setStoriesSourceURL(props.route.params.sourceURL)
    }, [])
    return (
        <View>
            <Header headerTitle={translations.Stories} navigation={props.navigation} />
            <View style={{ height: responsiveHeight(100), width: responsiveWidth(100) }}>
                {storiesSourceURL != '' &&
                    <WebView
                        style={{ flex: 1 }}
                        originWhitelist={['*']}
                        source={{
                            uri: storiesSourceURL
                        }}
                        style={{ marginTop: 0 }}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                    />
                }
            </View>
        </View>
    );
}

export default StoriesViewScreen;