import React, { Component, useContext, useEffect, useState } from 'react';
import { View, Text, FlatList, StyleSheet, ScrollView, Image, TouchableOpacity, Animated, Dimensions } from 'react-native';
import Header from "../../component/CustomInput/Header";
import { LocalizationContext } from '../../services/localization/LocalizationContext';
import { ApiCall } from "../../component/comman/ApiCall";
import RenderBook from '../../component/CustomInput/RenderBook';
import Loader from '../../component/comman/Loader'
import RadioButtonRN from 'radio-buttons-react-native';
import { Colors, Fonts, Images } from '../../styles/Index';
import { responsiveFontSize, responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions'
import ExistingAddressRender from '../../component/CustomInput/ExistingAddressRender'
import AddNewAddressRender from '../../component/CustomInput/AddNewAddressRender'
import { BuyingBookContext } from "../../services/buyingbook/BuyingBookContext";
import { CommanFunction } from '../../component/comman/CommanFunction'
import RazorpayCheckout from 'react-native-razorpay';
import { UserAddressContext } from "../../services/address/UserAddressContext";
import ValidationPopup from "../../component/CustomInput/ValidationPopup";
import { CommonActions } from '@react-navigation/native';
import { TextInput } from 'react-native-gesture-handler';
import { CartItemsContext } from '../../services/cartitems/CartItemsContext'
import { ScreenClass } from '../../component/comman/ScreenClass'
import { ScreenName } from '../../component/comman/ScreenName';
import { FirebaseConfig } from '../../component/comman/FirebaseConfig'
import { ClickedButtonName } from '../../component/comman/ClickedButtonName';
import Constant from "../../component/comman/Constant";
import { CurrencyContext } from "../../services/currency/CurrencyContext";
import useForceUpdate from 'use-force-update';
import getSymbolFromCurrency from 'currency-symbol-map'
const { width } = Dimensions.get('window');
const AddressScreen = (props) => {
    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);
    const { setUserBuyingBook, userBuyingBook } = useContext(BuyingBookContext);
    const { userCartgBook, setUserCartItems } = useContext(CartItemsContext);
    const [isLoading, setIsLoading] = useState(false);
    const [selectedAdress, setSelectedAdress] = useState('');
    const [selectedTab, setSelectedTab] = useState('ExistAddress');
    const { setUserAddress, userAddress } = useContext(UserAddressContext);
    const { setUserCurreny, setUserCurrenyValue, userCurrency, userCurrencyValue } = useContext(CurrencyContext);
    const [errorMessage, setErrorMessage] = useState('');
    const [checkPaymentDone, setCheckPaymentDone] = useState(false);
    const [discountAmmount, setDiscountAmmount] = useState(0);
    const [applyCouponText, setApplyCouponText] = useState('');
    // Final coupon text stored
    const [couponCode, setCouponText] = useState('');
    // store coupon type
    const [couponType, setCouponType] = useState('');
    // store coupon value
    const [couponValue, setCouponValue] = useState('');
    const [totalAmount, setTotalAmount] = useState(0);
    const forceUpdate = useForceUpdate();
    useEffect(() => {
        FirebaseConfig.realtimeScreenLogContent(ScreenClass.Address, ScreenName.Address)
        totalBookAmount(userBuyingBook);

    }, [])
    const setAddress = (selectedAddress) => {
        setSelectedAdress(selectedAddress)
    }
    const quantityIncreaseDecrease = (type, book, bookType) => {

        if (type == 'INCREASE') {
            if (bookType == "PrintBook") {
                book.paperbackQty = book.paperbackQty + 1;
                addCartData(book.itemID, book.paperbackQty, book.eBookQty, book)
            } else {
                book.eBookQty = book.eBookQty + 1;
                addCartData(book.itemID, book.paperbackQty, book.eBookQty, book)
            }
        } else {
            // book.quantity = book.quantity - 1;
            // addCartData(book.itemID, book.quantity, book)
            if (bookType == "PrintBook") {
                book.paperbackQty = book.paperbackQty - 1;
                addCartData(book.itemID, book.paperbackQty, book.eBookQty, book)
            } else {
                book.eBookQty = book.eBookQty - 1;
                addCartData(book.itemID, book.paperbackQty, book.eBookQty, book)
            }
        }
        forceUpdate()
        setCouponText('');
        setDiscountAmmount(0);
        totalBookAmount(userBuyingBook);
    }

    const addCartData = (ItemID, paperbackQty, eBookQty, book) => {
        FirebaseConfig.realtimeClickedButton(ItemID + "_" + ClickedButtonName.Add_To_Cart)
        var userData = {};
        userData.bookID = ItemID
        userData.paperbackQty = paperbackQty
        userData.eBookQty = eBookQty
        setIsLoading(true)
        ApiCall.Api_Call('/cart/add', userData, 'POST', true).then((result) => {
            // console.log(result.result.items);
            setIsLoading(false)
            if (result.status == 1) {
                setUserCartItems(result.result.items);

                if (paperbackQty == 0) {
                    if (eBookQty == 0) {
                        var idx = userBuyingBook.indexOf(book);
                        console.log(idx);
                        if (idx != -1) {
                            userBuyingBook.splice(idx, 1);
                        }
                    }
                }
                //  console.log(userBuyingBook.length);
                forceUpdate();
            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });
    }
    const closeExistsAddress = () => {
        props.navigation.navigate(props.route.params.routeName)
    }
    const bookPayment = () => {
        FirebaseConfig.realtimeClickedButton(ClickedButtonName.Pay_Button);
        if (selectedAdress !== '' && selectedAdress !== undefined && selectedAdress !== null) {
            rozayPayOrder();
        } else {
            setErrorMessage("Select one delivery address");
        }
    }


    const rozayPayOrder = () => {
        var userData = {}
        setIsLoading(true);
        userData.contentId = '',// userBuyingBook.bookItem.contentid,
            userData.address = selectedAdress,
            userData.orderQty = 2,// quantity,
            userData.couponCode = couponCode,
            userData.currency = userCurrency;
        userData.usercurrencyValue = userCurrencyValue;
        //  console.log("quantity----", userData);
        ApiCall.Api_Call('/razorpay/order', userData, 'POST', true).then((result) => {
            setIsLoading(false)
            console.log(" Order Create ----", result);
            if (result.status == 1) {
                RazorpayCheckout.open(CommanFunction.paymentOptions(
                    result.result.amount,
                    result.result.currency,
                    '',
                    // userBuyingBook.bookItem.thumbnail,
                    result.result.booktitle,
                    result.result.id,
                    result.result.address.phoneNumber

                )).then((data) => {
                    //  console.log("Rozay Data----", data);
                    verifyPayment(result.result._id, data);
                    // alert(`Success: ${data.razorpay_payment_id}`);
                }).catch((error) => {
                    // handle failure
                    setErrorMessage(`Error: ${error.code} | ${error.description}`);
                });
            } else if (result.status == 0) {
                setErrorMessage(result.message)

            } else {
                setErrorMessage(result.message)
            }
        }).catch((error) => {
            setIsLoading(false)
            alert(error);
            // console.log(error);
        });
    }
    const verifyPayment = (orderID, rozayPayData) => {
        //  console.log("--------", selectedAdress, "-----");
        var userData = {};
        userData.paymentId = orderID,
            userData.contentId = '',// userBuyingBook.bookItem.contentid,
            userData.razorPayRes = rozayPayData,
            userData.address = selectedAdress,
            userData.orderQty = '',// quantity,
            userData.couponCode = couponCode,
            userData.discount = discountAmmount !== 0 ? (totalAmount - discountAmmount).toFixed(2) : 0;
        setIsLoading(true);
        console.log("Verify Order Data----", userData);
        ApiCall.Api_Call('/verifypaymentandcreateorder', userData, 'POST', true).then((result) => {
            setIsLoading(false)
               console.log("Verify Order----", result);
            if (result.status == 1) {
                // setUserBuyingBook([]);

                setDiscountAmmount(0);
                setTotalAmount(0);
                setUserCartItems([]);
                setErrorMessage(result.message + " \nYour Order No is " + result.result.orderNo);

                setCheckPaymentDone(true);

                // props.navigation.navigate(translations.Orders);
            } else if (result.status == 0) {
                setErrorMessage(result.message)
                // alert(result.message)
            } else {
            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });

    }

    const hideValidationMesage = () => {
        setErrorMessage('');
        if (checkPaymentDone) {

            // props.navigation.pop();
            props.navigation.push("Books");
            setUserBuyingBook([]);
            props.navigation.navigate(translations.Orders);
        }
    }


    const applyCouponPayment = (coupanCode) => {
        var userData = {};

        if (coupanCode.length > 0 && coupanCode.trim() !== '') {
            userData.code = coupanCode.trim()
        } else {
            setErrorMessage("Coupon code is empty")
            return true
        }

        setIsLoading(true);
        //  console.log("Verify Order Data----", userData);
        ApiCall.Api_Call('/coupon/validate', userData, 'POST', true).then((result) => {
            setIsLoading(false)
            console.log("Verify Order----", result);
            if (result.status == 1) {
                setCouponText(coupanCode.trim())
                setDiscountAmmount(CommanFunction.applyCouponAmmount(totalAmount,
                    result.result.value, result.result.discount));

                setCouponType(result.result.discount);
                setCouponValue(result.result.value);

            } else if (result.status == 0) {
                setErrorMessage(result.message)
                setDiscountAmmount(0)
                setCouponText('')
                // alert(result.message)
            } else {
                setErrorMessage(result.description)
                setDiscountAmmount(0)
                setCouponText('')
            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });

    }


    const totalBookAmount = (userBuyingBook) => {
        var amount = 0;
        userBuyingBook.map((printBookItem, index) => {
            // console.log(userBuyingBook, "----------------");
            amount = amount + CommanFunction.buyingBookPrice(printBookItem.paperbackQty, printBookItem.itemDetails.sp)
        })

        userBuyingBook.map((eBbookItem, index) => {
            amount = amount + CommanFunction.buyingBookPrice(eBbookItem.eBookQty, eBbookItem.itemDetails.ebooksp != undefined && eBbookItem.itemDetails.ebooksp != 0 ? eBbookItem.itemDetails.ebooksp : 0)
        })


        setTotalAmount(amount, userCurrencyValue);
    }

    // position will be a value between 0 and photos.length - 1 assuming you don't scroll pass the ends of the ScrollView
    // const [contentOffset, setContentOffset] = React.useState({ x: 0, y: 0 });
    // const [contentSize, setContentSize] = React.useState(0);
    const scrollX = new Animated.Value(0);
    // position will be a value between 0 and photos.length - 1 assuming you don't scroll pass the ends of the ScrollView
    let position = Animated.divide(scrollX, width);
    //    console.log(userBuyingBook);

    return (
        <View style={{ flex: 1 }}>
            <Loader loading={isLoading} />
            {errorMessage !== '' && <ValidationPopup
                message={errorMessage}
                showHide={hideValidationMesage}
            />
            }
            <Header headerTitle={translations.PurchaseBook} backScreen={closeExistsAddress} navigation={props.navigation} />
            {selectedTab == 'ExistAddress' &&
                <>
                    <View style={{ height: responsiveHeight(33), width, }}>
                        <Animated.ScrollView

                            horizontal={true}
                            pagingEnabled={true} // animates ScrollView to nearest multiple of it's own width
                            showsHorizontalScrollIndicator={false}
                            onScroll={
                                Animated.event( // Animated.event returns a function that takes an array where the first element...
                                    [{ nativeEvent: { contentOffset: { x: scrollX } } }],
                                    { useNativeDriver: true }// ... is an object that maps any nativeEvent prop to a variable
                                )
                            }// in this case we are mapping the value of nativeEvent.contentOffset.x to this.scrollX
                        //  scrollEventThrottle={16}

                        // onScroll={() => Animated.event( // Animated.event returns a function that takes an array where the first element...
                        //     [{ nativeEvent: { contentOffset: { x: scrollX } } }],
                        //     { useNativeDriver: true }// ... is an object that maps any nativeEvent prop to a variable
                        // )}

                        // scrollEnabled={true} horizontal={true}
                        >
                            <View style={[{ flexDirection: 'row', }]}>



                                {userBuyingBook.map((book, index) => {
                                    return book.eBookQty != 0 ?
                                        <View key={index + 123} style={{
                                            marginHorizontal: responsiveWidth(1),
                                            width: responsiveWidth(98),
                                            flex: 1,
                                        }}>
                                            <View>
                                                <Text style={[styles.quantityStyle, { textAlign: 'center', color: Colors.appColor }]}>{
                                                    book.itemDetails.skutitle}</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row', height: responsiveHeight(20) }}>
                                                <View style={{
                                                    width: responsiveWidth(29),
                                                    alignItems: 'center', justifyContent: 'center'
                                                }} >
                                                    <Text style={styles.bookText}>{translations.PuchaseBookPrice}</Text>
                                                    <Text style={{
                                                        fontFamily: Fonts.Roboto.Roboto_Regular,
                                                        fontSize: responsiveFontSize(1.6),
                                                        color: Colors.inactiveButtonColor,
                                                    }}>
                                                        {book.itemDetails.currency !== userCurrency && userCurrency !== '' && userCurrency &&
                                                            userCurrency !== null ?
                                                            userCurrency : book.itemDetails.currency}{book.itemDetails.currency !== userCurrency && userCurrency !== '' && userCurrency &&
                                                                userCurrency !== null ?
                                                                " " + getSymbolFromCurrency(userCurrency) + " " + CommanFunction.currencyChangeValue(book.itemDetails.ebooksp, userCurrencyValue) :  " " + getSymbolFromCurrency(userCurrency) + " "  + book.itemDetails.ebooksp}

                                                        {/* Rs. {book.itemDetails.ebooksp != undefined ? book.itemDetails.ebooksp : '0'} */}

                                                    </Text>
                                                    <Text style={styles.bookText}>eBook</Text>
                                                </View>
                                                <View style={{ width: responsiveWidth(31), alignItems: 'center', justifyContent: 'center' }} >
                                                    <View style={styles.bookImageOterView}>
                                                    <Image style={styles.imageStyle}
                                                            source={CommanFunction.checkImageAvailability(book.itemDetails.thumbnail, book.itemDetails.isbn)}></Image>

                                                    </View>
                                                </View>
                                                <View style={{ width: responsiveWidth(36), justifyContent: 'center', alignItems: 'center', }} >
                                                    <View style={{ paddingBottom: responsiveWidth(2) }}>
                                                        <Text style={styles.bookText}>{translations.PuchaseBookQuantity}</Text>
                                                    </View>

                                                    <View style={{ flexDirection: 'row' }}>
                                                        <TouchableOpacity activeOpacity={Colors.activeOpecity}
                                                            onPress={() => { quantityIncreaseDecrease('DECREASE', book, "eBook") }}>
                                                            <View style={styles.quantityView}>
                                                                <Text style={styles.quantityStyle}>-</Text>
                                                            </View>
                                                        </TouchableOpacity>
                                                        <View style={{
                                                            width: responsiveWidth(15),
                                                            backgroundColor: 'lightgrey',
                                                            justifyContent: 'center',
                                                            alignItems: 'center'
                                                        }}>
                                                            <Text style={{
                                                                color: 'black',
                                                                fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(1.6)
                                                            }}>{book.eBookQty}</Text>
                                                        </View>
                                                        <TouchableOpacity activeOpacity={Colors.activeOpecity}
                                                            onPress={() => { quantityIncreaseDecrease('INCREASE', book, "eBook") }}>
                                                            <View style={styles.quantityView}>
                                                                <Text style={styles.quantityStyle}>+</Text>
                                                            </View>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                        : null
                                })
                                }
                                {userBuyingBook.map((book, index) => {
                                    return book.paperbackQty !== 0 ? <View key={index} style={{
                                        marginHorizontal: responsiveWidth(1),
                                        width: responsiveWidth(98),
                                        flex: 1,
                                    }}>
                                        <View>
                                            <Text style={[styles.quantityStyle, { textAlign: 'center', color: Colors.appColor }]}>{
                                                book.itemDetails.skutitle}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', height: responsiveHeight(20) }}>
                                            <View style={{
                                                width: responsiveWidth(29),
                                                alignItems: 'center', justifyContent: 'center'
                                            }} >
                                                <Text style={styles.bookText}>{translations.PuchaseBookPrice}</Text>
                                                <Text style={{
                                                    fontFamily: Fonts.Roboto.Roboto_Regular,
                                                    fontSize: responsiveFontSize(1.6),
                                                    color: Colors.inactiveButtonColor,
                                                }}>
                                                    {book.itemDetails.currency !== userCurrency && userCurrency !== '' && userCurrency && userCurrency !== null ?
                                                        userCurrency : book.itemDetails.currency}{book.itemDetails.currency !== userCurrency && userCurrency !== '' && userCurrency &&
                                                            userCurrency !== null ?
                                                            " " + getSymbolFromCurrency(userCurrency) + " " + CommanFunction.currencyChangeValue(book.itemDetails.sp, userCurrencyValue) : " " + getSymbolFromCurrency(userCurrency) + " " + book.itemDetails.sp}

                                                    {/* {book.itemDetails.currency}{" " + book.itemDetails.sp} */}

                                                </Text>
                                                {/* <Text style={styles.bookText}>Print book</Text> */}
                                            </View>
                                            <View style={{ width: responsiveWidth(31), alignItems: 'center', justifyContent: 'center' }} >
                                                <View style={styles.bookImageOterView}>
                                                <Image style={styles.imageStyle}
                                                            source={CommanFunction.checkImageAvailability(book.itemDetails.thumbnail, book.itemDetails.isbn)}></Image>

                                                </View>
                                            </View>
                                            <View style={{ width: responsiveWidth(36), justifyContent: 'center', alignItems: 'center', }} >
                                                <View style={{ paddingBottom: responsiveWidth(2) }}>
                                                    <Text style={styles.bookText}>{translations.PuchaseBookQuantity}</Text>
                                                </View>

                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableOpacity activeOpacity={Colors.activeOpecity}
                                                        onPress={() => { quantityIncreaseDecrease('DECREASE', book, "PrintBook") }}>
                                                        <View style={styles.quantityView}>
                                                            <Text style={styles.quantityStyle}>-</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                    <View style={{
                                                        width: responsiveWidth(15),
                                                        backgroundColor: 'lightgrey',
                                                        justifyContent: 'center',
                                                        alignItems: 'center'
                                                    }}>
                                                        <Text style={{
                                                            color: 'black',
                                                            fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(1.6)
                                                        }}>{book.paperbackQty}</Text>
                                                    </View>
                                                    <TouchableOpacity activeOpacity={Colors.activeOpecity}
                                                        onPress={() => { quantityIncreaseDecrease('INCREASE', book, "PrintBook") }}>
                                                        <View style={styles.quantityView}>
                                                            <Text style={styles.quantityStyle}>+</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                        : null
                                })
                                }
                            </View>
                        </Animated.ScrollView>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            {userBuyingBook.map((book, index) => { // the _ just means we won't use that parameter
                                let opacity = position.interpolate({
                                    inputRange: [index - 0.50000000001, index - 0.5, index, index + 0.5, index + 0.50000000001], // only when position is ever so slightly more than +/- 0.5 of a dot's index...
                                    outputRange: [0.3, 1, 1, 1, 0.3], // ... is when the opacity changes from 1 to 0.3
                                    extrapolate: 'clamp'
                                });
                                return (book.eBookQty != 0 ?
                                    <Animated.View // we will animate the opacity of the dots so use Animated.View instead of View here
                                        key={index} // we will use i for the key because no two (or more) elements in an array will have the same index
                                        style={{
                                            marginHorizontal: responsiveWidth(3),
                                            opacity, height: responsiveWidth(2.5), width: responsiveWidth(2.5), backgroundColor: '#595959', margin: responsiveWidth(1), borderRadius:
                                                responsiveWidth(2)
                                        }}
                                    />
                                    : null
                                );
                            })}
                            {userBuyingBook.map((book, index) => { // the _ just means we won't use that parameter
                                let opacity = position.interpolate({
                                    inputRange: [index - 0.50000000001, index - 0.5, index, index + 0.5, index + 0.50000000001], // only when position is ever so slightly more than +/- 0.5 of a dot's index...
                                    outputRange: [0.3, 1, 1, 1, 0.3], // ... is when the opacity changes from 1 to 0.3
                                    extrapolate: 'clamp'
                                });
                                return (book.paperbackQty != 0 ? (
                                    <Animated.View // we will animate the opacity of the dots so use Animated.View instead of View here
                                        key={index} // we will use i for the key because no two (or more) elements in an array will have the same index
                                        style={{
                                            marginHorizontal: responsiveWidth(3),
                                            opacity, height: responsiveWidth(2.5), width: responsiveWidth(2.5), backgroundColor: '#595959', margin: responsiveWidth(1), borderRadius:
                                                responsiveWidth(2)
                                        }}
                                    />
                                ) : null);
                            })}

                        </View>

                        <View style={{
                            flexDirection: 'row',
                            marginVertical: responsiveWidth(1.3),
                            paddingHorizontal: responsiveWidth(4),
                        }}>
                            <View style={{ width: responsiveWidth(68), }} >
                                <TextInput style={{
                                    paddingHorizontal: responsiveWidth(2),
                                    paddingVertical: responsiveWidth(2),
                                    height: responsiveHeight(5.5), borderRadius: responsiveWidth(1),
                                    borderColor: Colors.appColor,
                                    borderWidth: responsiveWidth(0.2),
                                    fontFamily: Fonts.Roboto.Roboto_Medium,
                                    fontSize: responsiveFontSize(1.7)
                                }}
                                    placeholder={'Enter coupon'}
                                    placeholderTextColor={'grey'}
                                    value={applyCouponText}
                                    name="couponName"

                                    onChangeText={(couponName) => { setApplyCouponText(couponName) }}
                                ></TextInput>
                            </View>
                            <TouchableOpacity activeOpacity={Colors.activeOpecity}
                                onPress={() => {
                                    applyCouponPayment(applyCouponText)

                                }} >
                                <View style={{
                                    backgroundColor: Colors.appColor,
                                    borderRadius: responsiveWidth(1),
                                    height: responsiveHeight(5.4),
                                    width: responsiveWidth(20),
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                }} >
                                    <Text style={[styles.buttonText, { fontSize: responsiveFontSize(1.4) }]}>Apply</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    {/* For Total Book  amount */}
                    <View style={{ height: responsiveHeight(6), justifyContent: 'center', paddingHorizontal: responsiveWidth(3) }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={[styles.bookText], { fontFamily: Fonts.Roboto.Roboto_Bold }}> Total Amount :   </Text>
                            <Text style={[styles.bookText], { color: 'black', fontSize: responsiveFontSize(1.5), fontFamily: Fonts.Roboto.Roboto_Bold, textDecorationLine: discountAmmount !== 0 ? 'line-through' : 'none' }}> {userCurrency}{" " + getSymbolFromCurrency(userCurrency) + " "}{(CommanFunction.currencyChangeValue(totalAmount, userCurrencyValue))}   </Text>
                            {/* {totalAmount.toFixed(2)} */}
                            {discountAmmount !== 0 &&
                                <Text style={[styles.bookText], { fontSize: responsiveFontSize(1.5), fontFamily: Fonts.Roboto.Roboto_Bold, color: 'black' }}> Save {userCurrency}{" " + getSymbolFromCurrency(userCurrency) + " "}{(CommanFunction.currencyChangeValue((totalAmount - discountAmmount), userCurrencyValue))}  </Text>
                            }

                            {discountAmmount !== 0 &&
                                <Text style={{
                                    paddingLeft: responsiveWidth(1),
                                    fontFamily: Fonts.Roboto.Roboto_Black,
                                    fontSize: responsiveFontSize(1.6),
                                    color: Colors.inactiveButtonColor,
                                }}>

                                    {userCurrency}{" " + getSymbolFromCurrency(userCurrency) + " "}{(CommanFunction.currencyChangeValue(discountAmmount, userCurrencyValue))}


                                    {/* Rs. {" " + discountAmmount.toFixed(2)} */}
                                </Text>
                            }
                        </View>
                    </View>
                </>


            }
            <View style={{ flexDirection: 'row', }}>
                <TouchableOpacity activeOpacity={Colors.activeOpecity}
                    onPress={() => { setSelectedTab('ExistAddress') }}>
                    <View style={[styles.ViewAddress, {
                        backgroundColor: selectedTab == 'ExistAddress' ?
                            Colors.appColor : 'lightgrey',
                    }]}>
                        <Text style={{
                            fontFamily: selectedTab == 'ExistAddress' ?
                                Fonts.Roboto.Roboto_Bold : Fonts.Roboto.Roboto_Regular,
                            color: selectedTab == 'ExistAddress' ? '#fff' : 'black'
                        }}>{translations.ExistingAddress}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={Colors.activeOpecity}
                    onPress={() => { setSelectedTab('NewAddress') }} >
                    <View style={[styles.ViewAddress, {
                        backgroundColor: selectedTab == 'NewAddress' ?
                            Colors.appColor : 'lightgrey',
                    }]}>
                        <Text style={{
                            fontFamily: selectedTab == 'NewAddress' ?
                                Fonts.Roboto.Roboto_Bold : Fonts.Roboto.Roboto_Regular,
                            color: selectedTab == 'NewAddress' ? '#fff' : 'black'
                        }}>{translations.NewAddress}</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{
                height: selectedTab == 'ExistAddress' ? responsiveHeight(30.8) : responsiveHeight(90),
                flex: 1,
                marginBottom: responsiveWidth(1),
                borderBottomWidth: responsiveWidth(0.1),
                borderBottomColor: 'grey'
            }}>
                <ScrollView nestedScrollEnabled={true} scrollEnabled={true}>
                    <View>
                        {selectedTab == 'ExistAddress' ?
                            <ExistingAddressRender
                                currentAddress={selectedAdress}
                                selectedAddress={setAddress}
                            />
                            : <AddNewAddressRender navigation={props} routeName={props.route.params.routeName} selectedTab={() => setSelectedTab('ExistAddress')} />

                        }
                    </View>
                </ScrollView>
            </View>
            {
                selectedTab == 'ExistAddress' &&
                <View style={{
                    flexDirection: 'row',
                    //  justifyContent: 'flex-end',
                    justifyContent: 'center',
                    // position: 'absolute',
                    // top: 240,
                    // right: 14,
                    // bottom: 2,
                    alignItems: 'center'
                }}>
                    <TouchableOpacity activeOpacity={Colors.activeOpecity}
                        onPress={() => {
                            bookPayment();
                        }} >
                        <View style={styles.buttonStyle}>
                            <Text style={styles.buttonText}>{translations.PaymentButton}</Text>
                        </View>
                    </TouchableOpacity>
                </View>

            }
        </View >
    )
}

const styles = StyleSheet.create({
    ViewAddress:
    {
        width: responsiveWidth(50),
        justifyContent: 'center',
        alignItems: 'center'
        , height: responsiveWidth(10),
    },
    bookQuantityHeader: {
        marginBottom: responsiveWidth(1.8),
        marginHorizontal: responsiveWidth(2),
        justifyContent: 'center',
    },
    imageStyle: {
        width: responsiveWidth(20),
        height: responsiveHeight(15),

    },
    quantityView: {
        width: responsiveWidth(10),
        backgroundColor: Colors.appColor,
        justifyContent: 'center',
        alignItems: 'center'
    },
    quantityStyle: {
        color: '#ffff',
        fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(1.8),
        paddingVertical: responsiveWidth(1)
    },
    buttonStyle: {
        width: responsiveWidth(88),
        borderRadius: responsiveWidth(2),
        backgroundColor: '#ffc107',
        alignItems: 'center', marginRight: responsiveWidth(3)
    },
    buttonText: {
        fontFamily: Fonts.Roboto.Roboto_Bold, fontSize: responsiveFontSize(2),
        color: '#fff',
        paddingVertical: responsiveWidth(2)

    },
    bookText: {
        fontFamily: Fonts.Roboto.Roboto_Regular,
        fontSize: responsiveFontSize(1.6), paddingBottom: responsiveWidth(1),
        color: Colors.appColor
    },


    bookImageOterView: {
        justifyContent: 'center',
        alignItems: 'center',
        opacity: 0.8,
        borderRadius: responsiveWidth(2),
        backgroundColor: '#ffff',// '#d9d9d9',
        shadowColor: '#30C1DD',
        shadowRadius: 10,
        shadowOpacity: 0.6,
        elevation: 8,
        width: responsiveWidth(24),
        height: responsiveHeight(18),
        shadowOffset: { width: 0, height: 4 },
    },

})

export default AddressScreen;
