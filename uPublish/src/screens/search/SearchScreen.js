import React, { Component, useContext, useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, TextInput, TouchableOpacity } from 'react-native';
import Header from "../../component/CustomInput/Header";
import { LocalizationContext } from '../../services/localization/LocalizationContext';
import Loader from "../../component/comman/Loader";
import { Colors, Fonts, Images } from '../../styles/Index'
import { ApiCall } from '../../component/comman/ApiCall'
import ShopBookRender from "../../component/CustomInput/ShopBookRender";
import FilterComponent from "../../component/CustomInput/FilterComponent";
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';

import { useIsFocused } from '@react-navigation/native';
const SearchScreen = (props) => {
    const { appLanguage, initializeAppLanguage
        , setAppLanguage, translations } = useContext(LocalizationContext);
    const [isLoading, setIsLoading] = useState(false);
    const [bookShopList, setBookShopList] = useState([]);
    const [searchText, setSearchText] = useState('');
    const [publisherList, setPublisherList] = useState([]);
    const [filterListData, setFilterListData] = useState([]);
    const [authorList, setAuthorList] = useState([]);
    const [languageList, setLanguageList] = useState([]);
    const [modalOpen, setModalOpen] = useState(false);
    const [firstApiCall, setFirstApiCall] = useState(true);
    useEffect(() => {
        var userData = {};
        var filters = {};
        filters.keyword = searchText;
        filters.author = authorList;
        filters.publisher = publisherList;
        filters.language = languageList;
        userData.filters = filters;
        getBookShop(userData);
        const unsubscribe = props.navigation.addListener('focus', () => {
            // props.navigation.navigate('Search')

            // getBookShop(userData);
        });
        return unsubscribe
    }, [props.navigation])

    const getBookShop = (userData) => {
        setIsLoading(true);
        ApiCall.Api_Call('/shop/advancedSearch', userData, 'POST', true).then((result) => {
            setIsLoading(false)
            // console.log(result);
            if (result.status == 1) {
                setBookShopList(result.result.records)
                if (firstApiCall) {
                    setFilterListData(result.result.aggregations)
                    setFirstApiCall(false)
                }
            } else if (result.status == 0) {

            } else {
            }
        }).catch((error) => {
            setIsLoading(false)
            console.log(error);
        });
    }
    const setAuthor = (author) => {
        // console.log(author);
        // console.log(authorList);
        if (authorList.includes(author)) {
            var idx = authorList.indexOf(author);
            // console.log(idx);
            if (idx != -1) {
                authorList.splice(idx, 1);
            }
        } else {
            authorList.push(author);
            // console.log(authorList);
        }
    }

    const setPublisher = (publisher) => {

        console.log(publisher);
        console.log(publisherList);
        if (publisherList.includes(publisher)) {
            var idx = publisherList.indexOf(publisher);
            // console.log(idx);
            if (idx != -1) {
                publisherList.splice(idx, 1);
            }
        } else {
            publisherList.push(publisher);
            // console.log(publisherList);
        }
    }

    const setLanguage = (language) => {
        // console.log(language);
        // console.log(languageList);
        if (languageList.includes(language)) {
            var idx = languageList.indexOf(language);
            console.log(idx);
            if (idx != -1) {
                languageList.splice(idx, 1);
            }
        } else {
            languageList.push(language);
            // console.log(languageList);
        }
    }

    const applyOrCloseModal = (type) => {
        if (type == 'Apply') {
            // console.log(authorList);
            var userData = {};
            var filters = {};
            filters.keyword = searchText;
            filters.author = authorList;
            filters.publisher = publisherList;
            filters.language = languageList;
            userData.filters = filters;
            // console.log(userData);
            getBookShop(userData);
            setModalOpen(false);
        } else {
            setModalOpen(false);
        }
    }
    return (
        <View style={{ opacity: modalOpen ? 0.3 : 1, flex: 1 }}>
            <Header headerTitle={translations.Search} navigation={props.navigation} />
            <Loader loading={isLoading} />

            <View style={[styles.ViewDate, { backgroundColor: '#F4F6F6' }]}>
                <View style={{ marginRight: responsiveWidth(1) }}>
                    <TextInput
                        placeholder={translations.SearchInput}
                        value={searchText}
                        name={'Keyword'}
                        testID={'Keyword'}
                        onChangeText={(Keyword) => setSearchText(Keyword)}
                        style={{
                            backgroundColor: '#ffff',
                            paddingHorizontal: responsiveWidth(2),
                            fontFamily: Fonts.Roboto.Roboto_Medium,
                            borderColor: Colors.appColor,
                            borderWidth: responsiveWidth(0.2),
                            paddingVertical: responsiveWidth(1), borderRadius: responsiveWidth(1.5),
                            height: responsiveHeight(5), width: responsiveWidth(58)
                            //  width: responsiveWidth(72)
                        }}>
                    </TextInput>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => {
                        var userData = {};
                        var filters = {};
                        filters.keyword = searchText;
                        filters.author = authorList;
                        filters.publisher = publisherList;
                        filters.language = languageList;
                        userData.filters = filters;
                        getBookShop(userData);
                    }}>
                        <View style={{
                            borderRadius: responsiveWidth(3),
                            width: responsiveWidth(20), justifyContent: 'center', alignItems: 'center',
                            height: responsiveHeight(4.8), backgroundColor: Colors.appColor   //'#3DE4B2'
                        }}>
                            <Text style={[styles.dateStyle, { color: '#ffff' }]}>{translations.SearchButton}</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={Colors.activeOpecity} onPress={() => {
                        setModalOpen(true)
                    }}>
                        <View style={{
                            width: responsiveWidth(16), justifyContent: 'center', alignItems: 'flex-end',
                            height: responsiveHeight(5),
                        }}>
                            <Text style={[styles.dateStyle, { color: Colors.appColor }]}>{translations.FilterText}  {">"}</Text>
                        </View>
                    </TouchableOpacity>

                </View>
            </View>
            {bookShopList.length == 0 &&
                <View style={styles.noBookList}>
                    <Text style={[styles.noBookListText, {
                        textAlign: 'center',
                        fontFamily: Fonts.Roboto.Roboto_Light
                    }]}>{translations.SearchBookList}</Text>
                </View>}

            <View style={{ marginBottom: responsiveWidth(24) }}>
                <FlatList
                    data={bookShopList}
                    // onEndReached={this.handleLoadMoreData}
                    onEndReachedThreshold={200}
                    renderItem={({ item, index, }) => (
                        <ShopBookRender index={index} item={item} navigation={props} />
                    )}
                    keyExtractor={(item, index) => item._id.toString()}
                // ListFooterComponent={this.renderFooter}
                // extraData={this.state.refresh}
                />
            </View>
            {modalOpen &&
                <FilterComponent filterList={filterListData}
                    authorList={authorList}
                    publisherList={publisherList}
                    languageList={languageList}
                    setAuthor={setAuthor}
                    setPublisher={setPublisher}
                    setLanguage={setLanguage}
                    applyOrCloseModal={applyOrCloseModal}
                    modalOpen={modalOpen}
                />
            }
        </View>
    )
}



const styles = StyleSheet.create({
    noBookList: {
        width: responsiveWidth(100),
        paddingHorizontal: responsiveWidth(4),
        height: responsiveHeight(5),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#fefefe',
        borderBottomColor: 'grey',
        borderBottomWidth: responsiveWidth(0.1)
    },
    noBookListText: {
        fontSize: responsiveFontSize(1.8),
        fontFamily: Fonts.Roboto.Roboto_Medium
    },
    ViewDate: {
        width: responsiveWidth(100),
        paddingHorizontal: responsiveWidth(4),
        height: responsiveHeight(7),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#fefefe',
        borderBottomColor: Colors.appColor,
        borderBottomWidth: responsiveWidth(0.1)
    },
    dateStyle: {
        fontSize: responsiveFontSize(1.8),
        fontFamily: Fonts.Roboto.Roboto_Bold
    },
})


export default SearchScreen;
