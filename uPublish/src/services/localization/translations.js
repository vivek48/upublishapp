import LocalizedStrings from 'react-native-localization';

export const DEFAULT_LANGUAGE = null;

const translations = {
  en: {
    BACK: 'Back',
    Currency: "Currency",
    //Login Screen Label
    headerText: 'Log in to your account',
    EMAIL: 'Email ID',
    EMAIL_PlaceHolder: 'Enter your email id*',//XXX--XXXX---XXX@.com655',
    PASSWORD: 'Password',
    PASSWORD_PlaceHolder: ' Enter your password* ',
    buttonTitle: 'Login',
    LoginFB: 'Continue with Facebook',// 'Login with facebook',
    LoginGoggle: 'Continue with Google',// 'Login with google',

    EMailLogin: 'Continue with email',
    ForgotPassword: "Forgot Password?",
    Continue: "Continue",
    ReturnLogin: "Return to log in",
    NewPassowrd: "Enter your new password*",
    Verify: "Verify",
    EmailOTPLabel: "E-Mail OTP",
    EmailOTPPlacHolder: "Enter your OTP*",
    EMaiOTPError: "Email OTP field required.",
    ForgottenPassword: "Forgotten your password?",
    ForgotPasswordText: "Don't worry, we'll send you a message to help you reset your password.",

    Quantity: 'Quantity',
    LoginH1: 'Books on Demand.',
    LoginH2: 'Anytime Anywhere.',
    LoginPageDescription: 'Work or play, Repro Books is loved by publishers worldwide and customers.',


    EmailTextValidation: "User Email field can't be empty and must contain a valid email address.",
    PasswordTextValidation: "Password field can't be empty and must contain at least 8 characters  one lowercase letter, one uppercase letter, one numeric digit, and one special character.",
    LoginValidation: 'User not found.',
    // SiGNUP SCREEN LABEL
    signUpHeaderText: 'Create your account',
    NamePlaceHolder: 'Enter your name',
    NameLabel: 'Name',
    ContactNumber: 'Phone Number',
    MessageContact:"Message",
    MessageValidation:"Message field  can't be empty.",
    ContactNumberPlaceHolder: 'Enter Phone Number',
    NameTextValidation: "User name field can't be empty and must contains only letters.",
    PhoneNumberValidation: "Phone number field can't be empty and must contains 10 numeric digit.",
    SignUpButtonTitle: 'Sign Up',
    SignUpUserCheck: 'User already exists.',



    // for Home Screen
    Home: "Books",     //"What's new",
    BooksSearch: 'Search Books...',
    // for Stories Screen
    Stories: 'Stories',
    StoriesList: 'No stories available found.',
    StoriesSearch: 'Seach Stories...',

    //for  Library Screen
    Library: 'library',
    LibraryList: 'No book found.',
    // for Search Screen
    Search: 'Search',
    SearchInput: 'Search books...',
    SearchEbookPlaceHolder: 'Search for ',
    SearchEbook: 'ebook ...',
    SearchButton: 'Search',
    FilterText: 'Filter',
    SearchBookList: 'No book available in Repro Books.',
    BuyButton: 'Buy Now',
    SaveDiscount: 'SAVE',
    AuthorBy: 'By',
    PublishedBy: 'Published By :',
    FilterApplyButton: 'Apply',
    FilterCloseButton: 'Close',
    PurchaseBook: 'Purchase book',
    PuchaseBookPrice: 'Book Price',
    PuchaseBookQuantity: 'Book Quantity',
    ExistingAddress: 'Existing Addresses',
    NewAddress: 'Add New Address',
    PaymentButton: 'Pay',
    SaveButton: 'Save Address',
    AddressLine1Label: 'Address Line 1',
    AddressLine1PlaceHolder: 'Enter Address Line 1 *',
    AddressLine1Validation: 'Address Line 1 field required.',
    AddressLine2Label: 'Address Line 2',
    AddressLine2PlaceHolder: 'Enter Address Line 2 *',
    AddressLine2Validation: 'Address Line 2 field required.',
    ZipCode: 'Zip Code',
    ZipCodePlaceHolder: 'Enter Zip Code *',
    ZipCodeValidation: 'Zip Code field required.',
    City: 'City',
    CityPlaceHolder: 'Enter City *',
    CityValidation: 'City field required.',
    State: 'State',
    StatePlaceHolder: 'Enter State *',
    StateValidation: 'State field required.',
    BackHeaderButton: 'Back',
    // for Setting Screen
    Setting: 'Setting',
    LogoutButton: 'Log Out',
    // for Orders Screen
    Orders: 'Orders',
    OrdersSearchInput: 'Search order...',
    OrdersList: "No order's available in Repro Books.",
    OrderNumber: 'Order No',
    OrderAmmout: 'Order Amount',
    OrderChannel: 'Channel :',
    OrderPending: 'Channel Status :',
    OrderBusinessUnit: 'Business Unit :',
    OrderStatus: 'Status',
    OrderDate: 'Order Date',
    OrderShipDate: 'Ship by Date',
    OrderItemType: "Item Type",

    // Order Details Screens


    OrderInvoice: "Order Invoice",
    OrderSendMail: "Send EMail",
    OrderDetail: "Order Detail",
    OrderDetails: "Order Details",
    OrderCurrency: "Order Currency",
    OrderBookRemarks: "Order Remarks",
    OrderChannelLocation: "Order Channel/Order Location",
    OrderAwbNo: "AWBNo",

    OrderRemarks: "Remarks",



    OrderItemList: "ItemList",
    OrderISBN: "ISBN",
    OrderQuantity: "Order Quantity",
    OrderUnitPrice: "Unit Price",

    OrderCouponDetails: "Coupons Details",
    OrderCouponCode: "Coupon Code",
    OrderDiscount: "Discount",

    OrderPaymentDetails: "Payment Details ",
    OrderPaymentAmmount: "Payment Amount",
    OrderPaymentID: "Payment Id",
    OrderPaymentDate: "Payment Date",

    OrderCustomerDetails: "Customer Details",
    OrderCustomerName: "Name",
    OrderCustomerAddress: "Address",
    OrderCustomerPincode: "Pin Code",
    OrderCustomerCity: "City",
    OrderCustomerState: "State",
    OrderCustomerCountry: "Country",
    OrderCustomerContactNumber: "Contact Number",
    OrderCustomerEmailID: "Email ID",

    //Settings Screen
    language: "Language",
    Privacy: "Privacy Policies & Terms Of Use",
    ReturnCancellation: 'Returns & Cancellations',
    ContactUs: 'Contact Us',
    ContactWriteUs:'Write Us',
    ContactWriteUsDes:"Jot us a note and we'll get back to you as quickly as possible.",
    SubmitButton:'Submit',
    ResetButton:"Reset",
    CorpoRateName:"CORPORATE AND REGISTERED OFFICE",
    CancellationText: "All our products shall be non - returnable.",
    Text1:'Repro India Limited',
    Text2:'11th Floor',
    Text3:'Sun Paradise Business Plaza,',
    Text4:'B Wing, Senapati Bapat Marg,',
    Text5:'Lower Parel, Mumbai 400013,',
    Text6:'INDIA.',
    Text7:'Tel: +91 22 7191 4000',
    Text8:'Corporate Website : https://www.reproindialtd.com/ ',
    b2bBusiness:'b2bsupport@reproindialtd.com',
    BuildNumber: "Build Number : 4.0.00",


    //

    HeaderName: 'Seller Information',


    // Chat Screen 

    chat: "Chat",
    // Product Review Screen 
    ProductReview: "Add Product Review",
    AllProductReview: "All Review's",


  },
  hi: {
    BACK: 'वापस जाए',
    Currency: "मुद्रा",
    // Login Screen Label
    headerText: 'अपने खाते में प्रवेश करें',
    EMAIL: 'ईमेल आईडी',
    EMAIL_PlaceHolder: 'अपनी ईमेल आईडी दर्ज करें *',
    PASSWORD: 'पासवर्ड ',
    PASSWORD_PlaceHolder: 'अपना पासवर्ड डालें *',
    buttonTitle: 'लॉग इन करें',
    LoginFB: 'फेसबुक के साथ जारी रखें',// 'फेसबुक के साथ लॉगिन करें',
    LoginGoggle: 'गूगल के साथ जारी रखें',// 'गूगल के साथ लॉगिन करें',

    EMailLogin: 'ईमेल के साथ जारी रखें',
    EmailTextValidation: 'उपयोगकर्ता ईमेल फ़ील्ड खाली नहीं हो सकता और उसमें एक मान्य ईमेल पता होना चाहिए।', //'उपयोगकर्ता का नाम / ईमेल फ़ील्ड आवश्यक है।',
    PasswordTextValidation: 'पासवर्ड फ़ील्ड खाली नहीं हो सकता है और इसमें कम से कम 8 अक्षर एक लोअरकेस अक्षर, एक अपरकेस अक्षर, एक संख्यात्मक अंक और एक विशेष वर्ण होना चाहिए।',
    LoginValidation: 'उपयोगकर्ता नहीं मिला।',

    ForgotPassword: "फॉरगेट पासवर्ड?",
    Continue: "जारी रखें",
    ReturnLogin: "लॉगिन पर लौटें",
    NewPassowrd: "अपना नया पासवर्ड दर्ज करें*",
    Verify: "सत्यापित करें",

    EmailOTPLabel: "ई-मेल ओटीपी",
    EmailOTPPlacHolder: "अपना ईमेल ओटीपी दर्ज करें*",
    EMaiOTPError: "ईमेल ओटीपी आवश्यक है।",
    ForgottenPassword: "अपना पासवर्ड भूल गये?",
    ForgotPasswordText: "चिंता न करें, हम आपको अपना पासवर्ड रीसेट करने में मदद करने के लिए एक संदेश भेजेंगे।",


    // SIGNUP SCREEN LABEL
    signUpHeaderText: 'अपना खाता बनाएँ',
    NamePlaceHolder: 'अपना नाम दर्ज करें',
    NameLabel: 'नाम',
    ContactNumber: 'फ़ोन नंबर',
    MessageContact:"संदेश",
    MessageValidation:"संदेश फ़ील्ड खाली नहीं हो सकता.",
    ContactNumberPlaceHolder: 'अपना फोन नंबर दर्ज करें',
    NameTextValidation: 'उपयोगकर्ता नाम फ़ील्ड खाली नहीं हो सकता और उसमें केवल अक्षर होने चाहिए।',
    PhoneNumberValidation: 'फ़ोन नंबर फ़ील्ड खाली नहीं हो सकता और इसमें 10 अंकीय अंक होने चाहिए।',
    SignUpButtonTitle: 'साइन अप करें',

    SignUpUserCheck: 'उपयोगकर्ता पहले से मौजूद है।',
    Quantity: 'मात्रा',
    LoginH1: 'डिमांड पर किताबें।',
    LoginH2: 'कभी भी कहीं भी।',
    LoginPageDescription: 'काम या खेल, रेप्रो बुक्स को दुनिया भर के प्रकाशकों और ग्राहकों से प्यार है।',


    // for Home Screen
    Home: "पुस्तकें",// 'नया क्या है',
    BooksSearch: 'पुस्तकें खोजें...',
    // for Stories Screen
    Stories: 'कहानियाँ ',
    StoriesList: 'कोई कहानी नहीं मिली।',
    StoriesSearch: 'कहानियां खोजें...',
    //for Library Screen
    Library: 'पुस्तकालय',
    LibraryList: 'कोई किताब नहीं मिली।',
    // for Search Screen
    Search: 'खोजना',
    SearchInput: 'किताबें खोजें...',
    SearchEbookPlaceHolder: 'खोज ',
    SearchEbook: 'ईबुक...',
    SearchButton: 'खोज',
    FilterText: 'फिल्टर',
    SearchBookList: 'कोई पुस्तक उपलब्ध नहीं है।',
    BuyButton: 'अभी खरीदें',
    SaveDiscount: 'सहेजें',
    AuthorBy: 'द्वारा',
    PublishedBy: 'द्वारा प्रकाशित :',
    FilterApplyButton: 'लागू',
    FilterCloseButton: 'बंद करे',
    PurchaseBook: 'पुस्तक खरीद',
    PuchaseBookPrice: 'पुस्तक का मूल्य',
    PuchaseBookQuantity: 'पुस्तक की मात्रा',
    ExistingAddress: 'मौजूदा पते',
    NewAddress: 'नया पता जोड़ें',
    PaymentButton: 'भुगतान करे',
    SaveButton: 'पता सहेजें',
    AddressLine1Label: 'पता पंक्ति 1',
    AddressLine1PlaceHolder: 'पता पंक्ति 1 दर्ज करें *',
    AddressLine1Validation: 'पता पंक्ति 1 फ़ील्ड आवश्यक है।',
    AddressLine2Label: 'पता पंक्ति 2',
    AddressLine2PlaceHolder: 'पता पंक्ति 2 दर्ज करें *',
    AddressLine2Validation: 'पता पंक्ति 2 फ़ील्ड आवश्यक है।',
    ZipCode: 'ज़िप कोड',
    ZipCodePlaceHolder: 'ज़िप कोड डालें *',
    ZipCodeValidation: 'ज़िप कोड फ़ील्ड आवश्यक है।',
    City: 'शहर',

    CityPlaceHolder: 'शहर दर्ज करें *',
    CityValidation: 'शहर फील्ड आवश्यक है।',
    State: 'राज्य',
    StatePlaceHolder: 'राज्य दर्ज करें *',
    BackHeaderButton: 'वापस जाए ',
    StateValidation: 'राज्य फ़ील्ड आवश्यक है।',

   


    // for Orders Screen
    Orders: 'ऑर्डर',
    OrdersSearchInput: 'ऑर्डर खोजें ...',
    OrdersList: "रेप्रो बुक्स में कोई ऑर्डर उपलब्ध नहीं है।",
    OrderNumber: 'ऑर्डर संख्या',
    OrderAmmout: 'ऑर्डर करने की राशि',
    OrderChannel: 'चैनल :',
    OrderPending: 'चैनल की स्थिति :',
    OrderBusinessUnit: 'व्यापार की इकाई :',
    OrderStatus: 'स्थिति',
    OrderDate: 'ऑर्डर की तारीख',
    OrderShipDate: 'शिप बाय डेट',
    OrderItemType: "Item Type",

    // Order Details Screens
    OrderItemList: "सामान की सूची",
    OrderISBN: "आईएसबीएन",
    OrderQuantity: "ऑर्डर की मात्रा",
    OrderUnitPrice: "यूनिट मूल्य",

    OrderCouponDetails: "कूपन विवरण",
    OrderCouponCode: "कूपन कोड",
    OrderDiscount: "छूट",


    OrderPaymentDetails: "भुगतान विवरण ",
    OrderPaymentAmmount: "भुगतान राशि",
    OrderPaymentID: "भुगतान आईडी",
    OrderPaymentDate: "भुगतान तिथि",

    OrderCustomerDetails: "उपभोक्ता विवरण",
    OrderCustomerName: "नाम",
    OrderCustomerAddress: "पता",
    OrderCustomerPincode: "पिन कोड",
    OrderCustomerCity: "शहर",
    OrderCustomerState: "राज्य",
    OrderCustomerCountry: "देश",
    OrderCustomerContactNumber: "संपर्क संख्या",
    OrderCustomerEmailID: "ईमेल आईडी",

    OrderAwbNo: "एडब्ल्यूबीनंबर",



    OrderInvoice: "ऑर्डर चालान",
    OrderSendMail: "ईमेल भेजें",
    OrderDetail: "ऑर्डर विवरण",
    OrderDetails: "ऑर्डर का विवरण",
    OrderCurrency: "ऑर्डर मुद्रा",
    OrderBookRemarks: "ऑर्डर टिप्पणियाँ",
    OrderChannelLocation: "ऑर्डर चैनल / ऑर्डर स्थान",

    OrderRemarks: "टिप्पणियाँ",

    //Settings Screen
     // for Setting Screen
     Setting: 'सेटिंग',
     LogoutButton: 'लॉग आउट',
    language: "भाषा",
    Privacy: "गोपनीयता नीतियां और उपयोग की शर्तें",
    ReturnCancellation: 'रिटर्न और रद्दीकरण',
    ContactUs: 'संपर्क करें',
    ContactWriteUs:'हमें लिखना',
    ContactWriteUsDes:"बस हमें एक नोट लिखें और हम जितनी जल्दी हो सके आपसे संपर्क करेंगे।",
    SubmitButton:'सबमिट',
    ResetButton:"रीसेट",
    CorpoRateName:"कॉर्पोरेट और पंजीकृत कार्यालय",
    Text1:'रेप्रो इंडिया लिमिटेड',
    Text2:'11वीं मंजिल',
    Text3:'सन पैराडाइज बिजनेस प्लाजा,',
    Text4:'बी विंग, सेनापति बापट मार्ग,',
    Text5:'लोअर परेल, मुंबई 400013,',
    Text6:'भारत।',
    Text7:'टेलीफोन : +91 22 7191 4000',
    Text8:'कारपोरेट वेबसाइट : https://www.reproindialtd.com/ ',
    b2bBusiness:'b2bsupport@reproindialtd.com',
    BuildNumber: "निर्माण संख्या : 4.0.00",

    // Cancelation Button

    CancellationText: "हमारे सभी उत्पाद गैर-वापसी योग्य होंगे।",//All our products shall be non - returnable.",

    // SellBox Item Name  

    HeaderName: 'विक्रेता जानकारी',

    // Chat Screen ------------

    chat: "चैट",

    // Product Review Screen 
    ProductReview: "उत्पाद समीक्षा जोड़ें",
    AllProductReview: "सभी समीक्षा",




  },
};

export default new LocalizedStrings(translations);
