import React, { createContext, useState, useEffect } from 'react';
import translations, { DEFAULT_LANGUAGE } from './translations';
import AsyncStorage from '@react-native-community/async-storage';
import * as RNLocalize from 'react-native-localize';
import { ApiCall } from "../../component/comman/ApiCall";

export const APP_LANGUAGE = 'appLanguage';
export const LocalizationContext = createContext({
  translations,
  setAppLanguage: () => { },
  appLanguage: DEFAULT_LANGUAGE,
  initializeAppLanguage: () => { },
});

export const LocalizationProvider = ({ children }) => {
  const [appLanguage, setAppLanguage] = useState(null);
  useEffect(() => {
    // AsyncStorage.getItem(APP_LANGUAGE).then((res) => {
    //   if (res != null) {
    //     translations.setLanguage(res)
    //   }
    //   setAppLanguage(res);
    //   console.log("hello", res)
    // });
    AsyncStorage.getItem('userLogin').then((res) => {
      if (res != null) {
        getSelectedLanguage(JSON.parse(res));
      } else {
        AsyncStorage.getItem(APP_LANGUAGE).then((res) => {
          if (res != null) {
            translations.setLanguage(res)
          }
          setAppLanguage(res);
          console.log("hello", res)
        });
      }
    });
  })


  const getSelectedLanguage = (user) => {
    var requestData = {};
    requestData.userId = user.user.userId
    ApiCall.Api_Call('/language/getUserlanguage', requestData, 'POST', true).then((result) => {
      console.log(result);
      if (result.status == 1) {
        setLanguage(result.result.languageCode)
      } else if (result.status == 0) {
        
      } else {
       
      }
    }).catch((error) => {
      console.log(error);
    });
  }


  const setLanguage = language => {
    // console.log(language);
    translations.setLanguage(language);
    setAppLanguage(language);
    AsyncStorage.setItem(APP_LANGUAGE, language);
  };

  const initializeAppLanguage = async () => {

    const currentLanguage = await AsyncStorage.getItem(APP_LANGUAGE);
    if (!currentLanguage) {
      let localeCode = DEFAULT_LANGUAGE;
      const supportedLocaleCodes = translations.getAvailableLanguages();
      const phoneLocaleCodes = RNLocalize.getLocales().map(
        locale => locale.languageCode,
      );
      phoneLocaleCodes.some(code => {
        if (supportedLocaleCodes.includes(code)) {
          localeCode = code;
          return true;
        }
      });
      setLanguage(localeCode);
    } else {
      setLanguage(currentLanguage);
    }
  };

  return (
    <LocalizationContext.Provider
      value={{
        translations,
        setAppLanguage: setLanguage,
        appLanguage,
        initializeAppLanguage,
      }}>
      {children}
    </LocalizationContext.Provider>
  );
};
