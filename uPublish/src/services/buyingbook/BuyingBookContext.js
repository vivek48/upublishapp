import React, { createContext, useState, useEffect } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from "../../component/comman/Constant";

export const BuyingBookContext = createContext({
    setUserBuyingBook: () => { },
    userBuyingBook: [],
});

export const BuyingBookContextProvider = ({ children }) => {
    const [userBuyingBook, setUserBuyingBook] = useState('');  //Constants.buyingUserData
    const setUserBuyingBookData = userBuyBook => {
        setUserBuyingBook(userBuyBook);
        // console.log("After Setting Users BuyingBookContext Details", userBuyingBook);
    };
    return (
        <BuyingBookContext.Provider
            value={{
                userBuyingBook: userBuyingBook,
                setUserBuyingBook: setUserBuyingBookData,
            }}>
            {children}
        </BuyingBookContext.Provider>
    );
};