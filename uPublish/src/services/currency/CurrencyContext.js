import React, { createContext, useState, useEffect } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from "../../component/comman/Constant";

export const CurrencyContext = createContext({
    setUserCurreny: () => { },
    userCurrency: null,
    setUserCurrenyValue: () => { },
    userCurrencyValue: null,
});

export const CurrencyContextProvider = ({ children }) => {
    const [userCurrency, setUserCurreny] = useState('INR');
    const [userCurrencyValue, setUserCurrenyValue] = useState(1);
    const setUserCurrencyData = currency => {
        setUserCurreny(currency);
    };
    const setUserCurrencyValueData = currencyValue => {
        setUserCurrenyValue(currencyValue);
    };
    return (
        <CurrencyContext.Provider
            value={{
                userCurrency: userCurrency,
                setUserCurreny: setUserCurrencyData,
                userCurrencyValue: userCurrencyValue,
                setUserCurrenyValue: setUserCurrencyValueData,
            }}>
            {children}
        </CurrencyContext.Provider>
    );
};