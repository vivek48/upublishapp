import React, { createContext, useState, useEffect } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from "../../component/comman/Constant";

export const UserAddressContext = createContext({
    setUserAddress: () => { },
    userAddress: [],
});

export const UserAddressContextProvider = ({ children }) => {
    const [userAddress, setUserAddress] = useState([]);
    const setUserAddressData = (type, userAddr) => {
        // console.log(userAddr);

        if (type == 'API') {
            setUserAddress(userAddr)
        } else {
            userAddress.push(userAddr);
        }
        // console.log("After Setting Users Address Details", userAddress);
    };
    return (
        <UserAddressContext.Provider
            value={{
                userAddress: userAddress,
                setUserAddress: setUserAddressData,
            }}>
            {children}
        </UserAddressContext.Provider>
    );
};