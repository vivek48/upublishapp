import React, { createContext, useState, useEffect } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from "../../component/comman/Constant";

export const UserContext = createContext({
    setUserLogin: () => { },
    userLogin: null,
});

export const UserProvider = ({ children }) => {
    const [userLogin, setUserLogin] = useState(null);
    useEffect(() => {
        AsyncStorage.getItem('userLogin').then((res) => {
            if (res != null) {
                setUserLogin(res)
                Constants.LogIn_User = res;
            }
        });
    })
    const setUserLoginData = userLogin => {
       
        if (userLogin !== null) {
            
            Constants.LogIn_User = JSON.stringify(userLogin)
            AsyncStorage.setItem('userLogin', JSON.stringify(userLogin));
            setUserLogin(userLogin);
        } else {
            Constants.LogIn_User = {};
            setUserLogin(userLogin);
            AsyncStorage.setItem('userLogin', '');
        }
        // console.log("After Setting Users Details", Constants.LogIn_User);
    };
    return (
        <UserContext.Provider
            value={{
                userLogin: userLogin,
                setUserLogin: setUserLoginData,
            }}>
            {children}
        </UserContext.Provider>
    );
};