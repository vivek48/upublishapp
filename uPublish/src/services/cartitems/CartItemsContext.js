import React, { createContext, useState, useEffect } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from "../../component/comman/Constant";

export const CartItemsContext = createContext({
    setUserCartItems: () => { },
    userCartgBook: [],
});

export const CartItemsContextProvider = ({ children }) => {
    const [userCartgBook, setUserCartItems] = useState('');
    const setUserCartItemBookData = cartItemBooks => {
        setUserCartItems(cartItemBooks);

    };
    return (
        <CartItemsContext.Provider
            value={{
                userCartgBook: userCartgBook,
                setUserCartItems: setUserCartItemBookData,
            }}>
            {children}
        </CartItemsContext.Provider>
    );
};