import React, { createContext, useState, useEffect } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from "../../component/comman/Constant";

export const BuyEpubBookContext = createContext({
    setBuyEpubBookList: () => { },
    buyEpubBookList: [],
});

export const BuyEpubBookContextProvider = ({ children }) => {
    const [buyEpubBookList, setBuyEpubBookList] = useState([]);

    useEffect(() => {
        AsyncStorage.getItem(Constants.BuyEPubBook).then((res) => {
            if (res != null) {
                // console.log("Buy EPUB BOOK URL ------", JSON.parse(res))
                setBuyEpubBookList(JSON.parse(res));
                Constants.BuyEPubBookLIST = JSON.parse(res);
            }

        }).catch((err) => { console.log(err); });
    })
    const setBuyEpubBookListData = (buyEPubBookURL) => {
        buyEpubBookList.push(buyEPubBookURL);
        console.log("AFter setting Epub URL ------", buyEPubBookURL)
    };
    return (
        <BuyEpubBookContext.Provider
            value={{
                buyEpubBookList: buyEpubBookList,
                setBuyEpubBookList: setBuyEpubBookListData,
            }}>
            {children}
        </BuyEpubBookContext.Provider>
    );
};