/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React, { useEffect } from 'react';
import { Provider } from "react-redux";
import { store } from '../uPublish/src/store/store'
import Splash from 'react-native-splash-screen'
import { LocalizationProvider } from './src/services/localization/LocalizationContext'
import Routes from '../uPublish/src/navigations/Routes'
import { UserProvider } from './src/services/loginuser/UserContext'
import { BuyingBookContextProvider } from "./src/services/buyingbook/BuyingBookContext";
import { UserAddressContextProvider } from "./src/services/address/UserAddressContext";
import { BuyEpubBookContextProvider } from "./src/services/buyepubbook/BuyEpubBookContext";
import { CartItemsContextProvider } from './src/services/cartitems/CartItemsContext'
import { CurrencyContextProvider } from "./src/services/currency/CurrencyContext";

// import { decode, encode } from 'base-64'
// if (!global.btoa) { global.btoa = encode }

// if (!global.atob) { global.atob = decode }
const App = () => {
  useEffect(() => {
    Splash.hide();
  })
  return (
    <LocalizationProvider>
      <UserProvider>
        <BuyingBookContextProvider>
          <UserAddressContextProvider>
            <BuyEpubBookContextProvider>
              <CartItemsContextProvider>
                <CurrencyContextProvider>
                  <Provider store={store}>
                    <Routes></Routes>
                  </Provider>
                </CurrencyContextProvider>
              </CartItemsContextProvider>
            </BuyEpubBookContextProvider>
          </UserAddressContextProvider>
        </BuyingBookContextProvider>
      </UserProvider>
    </LocalizationProvider>
  );
};
export default App;
